#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Helper functions for evaluation.

@author: kartmann
"""

import os
import numpy as np
import pandas as pd

import evaluation_config as cfg

#%% input file paths

def labelFilePath(scene_name):
    return os.path.join(cfg.pointcloud_dir, scene_name + "-labels.txt")


def pointcloudFilePath(scene_name):
    return os.path.join(cfg.pointcloud_dir, scene_name + ".pcd")


def groundTruthFilePath(scene_name):
    return os.path.join(cfg.ground_truth_dir, scene_name + ".txt")


#%% Result file paths

def sceneResultsDir(scene_name):
    return os.path.join(cfg.experiments_dir, scene_name)


def graphFilename(scene_name, strategy_name, relabeled=True):
    filename = cfg.output_graph_name + "_" + strategy_name
    if relabeled:
        filename += cfg.relabel_suffix
    return filename + ".txt"


def graphFilePath(scene_name, strategy_name, relabeled=True):
    return os.path.join(sceneResultsDir(scene_name), 
                   graphFilename(scene_name, strategy_name, relabeled))



#%%

def resetDirectory(dirname, basedir = "."):
    
    dir_path = os.path.join(basedir, dirname)
    
    if dirname not in os.listdir(basedir):
        print("Making directory '{}' in '{}'".format(dirname, basedir))
        os.mkdir(dir_path)
        
    else:
        print("Clearing directory '{}' in '{}'".format(dirname, basedir))
        for file in os.listdir(dir_path):
            try:
                os.remove(os.path.join(dir_path, file))
            except IsADirectoryError:
                if file not in cfg.scene_names:
                    print("Skipping directory '{}'".format(file))
            except Exception as e:
                print("Could not delete file '{}' in {}:\n{}".format(
                        file, cfg.experiments_dir, e))
                

#%% 

def ignoredLine(line):
    return line.startswith("#") or line.strip() == ""


#%%

class LoadLabelFileError(Exception):
    pass   


def loadLabelFile(scene_name):
    
    relabelMap = dict()
    with open(labelFilePath(scene_name)) as file:
        for line in file:
            
            if ignoredLine(line):
                continue
            
            split = line.strip().split(" ")
            
            if len(split) != 2:
                raise LoadLabelFileError("Cannot parse line (expected exactly one ' ' (space)):\n\t'{}'".format(line))
                
            relabelMap[split[0].strip()] = split[1].strip()
        
    return relabelMap


#%% 

def relabelResultGraph(scene_name, strategy_name):
    
    relabelMap = loadLabelFile(scene_name)
    
    out_lines = []
    with open(graphFilePath(scene_name, strategy_name, relabeled=False)) as file:
        for line in file:
            
            if ignoredLine(line):
                out_lines.append(line)
                continue
            
            split = line.strip().split(" ")
            
            out_split = []
            for s in split:
                if s in relabelMap:
                    out_split.append(relabelMap[s])
                else:
                    out_split.append(s)
                
            out_line = " ".join(out_split) + "\n"
            out_lines.append(out_line)
            
    with open(graphFilePath(scene_name, strategy_name, relabeled=True), "w") as file:
        file.writelines(out_lines)
    

#%% 

class LoadGraphError(Exception):
    pass


def loadGraph(filename):
    
    graph = dict()
    
    with open(filename) as file:
        for line in file:
            
            if ignoredLine(line):
                continue
            
            adj_line = line.strip().split("->")
            
            if len(adj_line) != 2:
                raise LoadGraphError("Cannot parse line (expected exactly one '->'):\n\t'{}'".format(line))
            
            vertex = adj_line[0].strip()
            edges = adj_line[1].strip()
            
            edge_list = edges.split(" ")
            
            graph[vertex] = []
            
            for e in edge_list:
                e = e.strip()
                if e is not "":
                    graph[vertex].append(e)
            
    return graph


#%%        

def loadGraphHypo(scene_name, strategy_name, relabeled=True):
    return loadGraph(graphFilePath(scene_name, strategy_name, relabeled))
    

def loadGraphGT(scene_name):
    return loadGraph(groundTruthFilePath(scene_name))

    
#%%
    
def repeat(lst, times):
    res = []
    for item in lst:
        res += [item] * times
    return res


#%%
    
def createEvalDataFrame():
    
    num_rows = len(cfg.scene_names) * len(cfg.strategy_names)
    zeros = [0] * num_rows
    
    data = pd.DataFrame({ 
            "scene": repeat(cfg.scene_names, len(cfg.strategy_names)),
            "strategy": (cfg.strategy_names * len(cfg.scene_names)),
            "tp": zeros, "fp": zeros, "fn": zeros, 
            "tn": zeros, "prec": zeros, "rec": zeros},
            dtype = np.float32
        )
    data = data.set_index(["scene", "strategy"])
    
    return data


#%%

def toPaperFormat(eval_data):
    
    scenes = eval_data.index.get_level_values("scene").unique() 
    strategies = eval_data.index.get_level_values("strategy").unique()
    
    # create data frame
    columns = ["strategy", "metric"] + list(scenes) + ["Mean"]
    
    paper_data = pd.DataFrame(columns=columns)
    paper_data.strategy = repeat(strategies, 2)
    paper_data.metric = ["Prec", "Rec"] * len(strategies)
    
    paper_data = paper_data.set_index(["strategy", "metric"])
    
    # fill in eval data
    for index, row in eval_data.iterrows():
        (scene, strategy) = index
        paper_data.loc[(strategy, "Prec")][scene] = row.prec
        paper_data.loc[(strategy, "Rec")][scene] = row.rec
    
    # add means
    means = eval_data.groupby("strategy").mean()
    for s in strategies:
        paper_data.loc[(s, "Prec")]["Mean"] = means.loc[s].prec
        paper_data.loc[(s, "Rec")]["Mean"] = means.loc[s].rec
    
    return paper_data
    
    
#%%



