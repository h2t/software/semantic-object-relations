#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Confusion matrix (wrapper for pd.crosstab)

@author: kartmann
"""

#%%
import pandas as pd

import evaluation_utils as utils


#%%
    
class ConfusionMatrix:
    
    def __init__(self, scene_name, strategy_name):
        
        self.scene_name = scene_name
        self.strategy_name = strategy_name
        
        self.graph_hypo = utils.loadGraphHypo(scene_name, strategy_name)
        self.graph_gt = utils.loadGraphGT(scene_name)
        
        if not self.graph_hypo.keys() == self.graph_gt.keys():
            print("[WARNING] Vertex sets differ:\n\tGround Truth ({}): {}\n\tStrategy ({}): {}".format(
                    scene_name, self.graph_gt.keys(), strategy_name, self.graph_hypo.keys()))
        
        vertices = list(self.graph_gt.keys())
    
        hypoList = []
        gtList = []
        
        for u in vertices:
            for v in vertices:
                
                if u == v: 
                    continue 
            
                hypoList.append(v in self.graph_hypo[u])
                gtList.append(v in self.graph_gt[u])
    
        hypoSeries = pd.Series(hypoList, name = scene_name + "_" + strategy_name)
        gtSeries = pd.Series(gtList, name = scene_name + "_GT")
        
        self.cm = pd.crosstab(hypoSeries, gtSeries, margins=True)
        
        
    def tp(self):
        return self.cm[True][True]
    def tn(self):
        return self.cm[False][False]
    def fp(self):
        return self.cm[False][True]
    def fn(self):
        return self.cm[True][False]
        
    def pos(self):
        """Number of real support edges."""
        return self.cm[True]["All"]
    def neg(self):
        """Number of real non-support edges."""
        return self.cm[False]["All"]
    
    def posHyp(self):
        """Number of predicted support edges."""
        return self.cm["All"][True]
    def negHyp(self):
        """Number of predicted non-support edges."""
        return self.cm["All"][False]
    
    
    def precision(self):
        return self.tp() / (self.tp() + self.fp())
    
    def recall(self):
        return self.tp() / self.pos()
        
    
    def __repr__(self):
        return repr(self.cm)
    
    def __str__(self):
        return str(self.cm)
        