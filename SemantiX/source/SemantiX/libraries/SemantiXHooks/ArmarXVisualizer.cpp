#include "ArmarXVisualizer.h"

#include <RobotAPI/libraries/core/Pose.h>

#include <SemantiX/libraries/SemantiXVariants/ShapeVariants.h>


using namespace armarx;

static Vector3Ptr toArmarx(const Eigen::Vector3f& v)
{
    return Vector3Ptr(new armarx::Vector3(v));
}

static QuaternionPtr toArmarx(const Eigen::Quaternionf& q)
{
    return QuaternionPtr(new armarx::Quaternion(q));
}

static PosePtr toArmarx(const Eigen::Vector3f& pos, const Eigen::Quaternionf& ori)
{
    return PosePtr(new armarx::Pose(toArmarx(pos), toArmarx(ori)));
}

static armarx::DrawColor toArmarx(const semrel::DrawColor& color)
{
    return DrawColor { color.r, color.g, color.b, color.a };
}


void ArmarXVisualizer::setAsImplementation(DebugDrawerInterfacePrx debugDrawer)
{
    semrel::VisualizerInterface::setImplementation(std::make_shared<ArmarXVisualizer>(debugDrawer));
}


ArmarXVisualizer::ArmarXVisualizer(DebugDrawerInterfacePrx debugDrawer) :
    drawer(debugDrawer)
{
}

void ArmarXVisualizer::clearAll()
{
    drawer->clearAll();
}

void ArmarXVisualizer::clearLayer(const std::string& layer)
{
    drawer->clearLayer(layer);
}

void ArmarXVisualizer::drawLine(semrel::VisuMetaInfo id, const Eigen::Vector3f& start, const Eigen::Vector3f& end, float lineWidth, semrel::DrawColor color)
{
    drawer->setLineVisu(id.layer, id.name, toArmarx(start), toArmarx(end), lineWidth, toArmarx(color));
}

void ArmarXVisualizer::drawArrow(semrel::VisuMetaInfo id, const Eigen::Vector3f& origin, const Eigen::Vector3f& direction, float length, float width, semrel::DrawColor color)
{
    drawer->setArrowVisu(id.layer, id.name, toArmarx(origin), toArmarx(direction), toArmarx(color), length, width);
}

void ArmarXVisualizer::drawBox(semrel::VisuMetaInfo id, const semrel::Box& box, semrel::DrawColor color)
{
    drawer->setBoxVisu(id.layer, id.name, toArmarx(box.getPosition(), box.getOrientation()),
                       toArmarx(box.getExtents()), toArmarx(color));
}

void ArmarXVisualizer::drawCylinder(semrel::VisuMetaInfo id, const semrel::Cylinder& cylinder, semrel::DrawColor color)
{
    drawer->setCylinderVisu(id.layer, id.name, toArmarx(cylinder.getPosition()), toArmarx(cylinder.getAxisDirection()),
                            cylinder.getHeight(), cylinder.getRadius(), toArmarx(color));
}

void ArmarXVisualizer::drawSphere(semrel::VisuMetaInfo id, const semrel::Sphere& sphere, semrel::DrawColor color)
{
    drawer->setSphereVisu(id.layer, id.name, toArmarx(sphere.getPosition()), toArmarx(color), sphere.getRadius());
}

void ArmarXVisualizer::drawPolygon(semrel::VisuMetaInfo id, const std::vector<Eigen::Vector3f>& polygonPoints, float lineWidth, semrel::DrawColor colorInner, semrel::DrawColor colorBorder)
{
    PolygonPointList armarxPoints;
    for (const Eigen::Vector3f& v : polygonPoints)
    {
        armarxPoints.push_back(toArmarx(v));
    }

    drawer->setPolygonVisu(id.layer, id.name, armarxPoints, toArmarx(colorInner), toArmarx(colorBorder), lineWidth);
}

void ArmarXVisualizer::drawTriMesh(semrel::VisuMetaInfo id, const semrel::TriMesh& mesh, semrel::DrawColor color)
{
    /*
    // use variant to create armarx tri mesh (DebugDrawerTriMesh)
    MeshShapeVariant variant(semrel::MeshShape(Eigen::Vector3f::Zero(), Eigen::Quaternionf::Identity(), mesh));
    DebugDrawerTriMesh ddMesh = variant.getMesh();

    // replace default color
    ddMesh.colors.front() = toArmarx(color);

    drawer->setTriMeshVisu(id.layer, id.name, ddMesh);
    */

    int i = 0;
    for (semrel::TriMesh::Triangle triangle : mesh.getTriangles())
    {
        semrel::VisuMetaInfo triID = id;
        std::stringstream ss;
        ss << triID.name << "_" << i;
        triID.name = ss.str();

        std::vector<Eigen::Vector3f> points;
        points.push_back(mesh.getVertex(triangle.v0));
        points.push_back(mesh.getVertex(triangle.v1));
        points.push_back(mesh.getVertex(triangle.v2));

        semrel::DrawColor inner = color;
        color.a = 0.1f;

        drawPolygon(triID, points, 1, inner, color);
        i++;
    }
}

void ArmarXVisualizer::drawText(semrel::VisuMetaInfo id, const std::string& text, const Eigen::Vector3f& position, float size, semrel::DrawColor color)
{
    drawer->setTextVisu(id.layer, id.name, text, toArmarx(position), toArmarx(color), int(roundf(size)));
}

void ArmarXVisualizer::drawPointCloud(semrel::VisuMetaInfo id, const std::vector<Eigen::Vector3f>& cloud, float pointSize, semrel::DrawColor color)
{
    DrawColor armarxColor = toArmarx(color);

    DebugDrawerColoredPointCloud ddPointcloud;
    ddPointcloud.pointSize = pointSize;

    for (const Eigen::Vector3f& v : cloud)
    {
        DebugDrawerColoredPointCloudElement el { v(0), v(1), v(2), armarxColor };
        ddPointcloud.points.push_back(el);
    }

    drawer->setColoredPointCloudVisu(id.layer, id.name, ddPointcloud);
}


void ArVizVisualizer::setAsImplementation(const viz::Client& arviz)
{
    semrel::VisualizerInterface::setImplementation(std::make_shared<ArVizVisualizer>(arviz));
}

ArVizVisualizer::ArVizVisualizer(const viz::Client& arviz)
    : arviz(arviz)
{

}

void ArVizVisualizer::clearAll()
{

}

void ArVizVisualizer::clearLayer(const std::string& name)
{
    auto& layer = getLayer(name);
    layer.clear();
    arviz.commit(layer);
}

static viz::Color toViz(semrel::DrawColor color)
{
    viz::Color result;
    result.r = 255.0f * color.r;
    result.g = 255.0f * color.g;
    result.b = 255.0f * color.b;
    result.a = 255.0f * color.a;
    return result;
}

void ArVizVisualizer::drawLine(semrel::VisuMetaInfo id, const Eigen::Vector3f& start, const Eigen::Vector3f& end, float lineWidth, semrel::DrawColor color)
{
    armarx::viz::Layer& layer = getLayer(id.layer);
    viz::Cylinder line = viz::Cylinder(id.name)
                         .fromTo(start, end)
                         .radius(0.5f * lineWidth)
                         .color(toViz(color));
    layer.add(line);
    arviz.commit(layer);
}

void ArVizVisualizer::drawArrow(semrel::VisuMetaInfo id, const Eigen::Vector3f& origin, const Eigen::Vector3f& direction, float length, float width, semrel::DrawColor color)
{
    armarx::viz::Layer& layer = getLayer(id.layer);
    viz::Arrow arrow = viz::Arrow(id.name)
                       .position(origin)
                       .direction(direction)
                       .length(length)
                       .width(width)
                       .color(toViz(color));
    layer.add(arrow);
    arviz.commit(layer);
}

void ArVizVisualizer::drawBox(semrel::VisuMetaInfo id, const semrel::Box& box, semrel::DrawColor color)
{
    armarx::viz::Layer& layer = getLayer(id.layer);
    auto element = viz::Box(id.name)
                   .position(box.getPosition())
                   .orientation(box.getOrientation())
                   .size(box.getExtents())
                   .color(toViz(color));
    layer.add(element);
    arviz.commit(layer);
}

void ArVizVisualizer::drawCylinder(semrel::VisuMetaInfo id, const semrel::Cylinder& cylinder, semrel::DrawColor color)
{
    armarx::viz::Layer& layer = getLayer(id.layer);
    auto element = viz::Cylinder(id.name)
                   .position(cylinder.getPosition())
                   .orientation(cylinder.getOrientation())
                   .radius(cylinder.getRadius())
                   .height(cylinder.getHeight())
                   .color(toViz(color));
    layer.add(element);
    arviz.commit(layer);
}

void ArVizVisualizer::drawSphere(semrel::VisuMetaInfo id, const semrel::Sphere& sphere, semrel::DrawColor color)
{
    armarx::viz::Layer& layer = getLayer(id.layer);
    auto element = viz::Sphere(id.name)
                   .position(sphere.getPosition())
                   .orientation(sphere.getOrientation())
                   .radius(sphere.getRadius())
                   .color(toViz(color));
    layer.add(element);
    arviz.commit(layer);
}

void ArVizVisualizer::drawPolygon(semrel::VisuMetaInfo id, const std::vector<Eigen::Vector3f>& polygonPoints, float lineWidth, semrel::DrawColor colorInner, semrel::DrawColor colorBorder)
{
    armarx::viz::Layer& layer = getLayer(id.layer);
    auto element = viz::Polygon(id.name)
                   .points(polygonPoints)
                   .color(toViz(colorInner))
                   .lineWidth(1.0f)
                   .lineColor(toViz(colorBorder));
    layer.add(element);
    arviz.commit(layer);
}

void ArVizVisualizer::drawTriMesh(semrel::VisuMetaInfo id, const semrel::TriMesh& mesh, semrel::DrawColor color)
{

}

void ArVizVisualizer::drawText(semrel::VisuMetaInfo id, const std::string& text, const Eigen::Vector3f& position, float size, semrel::DrawColor color)
{

}

void ArVizVisualizer::drawPointCloud(semrel::VisuMetaInfo id, const std::vector<Eigen::Vector3f>& cloud, float pointSize, semrel::DrawColor color)
{

}

viz::Layer& ArVizVisualizer::getLayer(const std::string& name)
{
    auto it = layers.find(name);
    if (it == layers.end())
    {
        it = layers.emplace(name, arviz.layer(name)).first;
    }
    return it->second;
}
