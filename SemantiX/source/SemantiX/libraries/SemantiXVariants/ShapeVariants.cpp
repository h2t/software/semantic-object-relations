/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SemantiX::ArmarXObjects::ShapeVariants
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ShapeVariants.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <ArmarXCore/observers/AbstractObjectSerializer.h>
#include <RobotAPI/libraries/core/Pose.h>

#include "VariantListSerialization.h"


using namespace armarx;


std::string str(const Vector3BasePtr& v)
{
    std::stringstream ss;
    ss << "(" << v->x << " " << v->y << " " << v->z << ")";
    return ss.str();
}
std::string str(const QuaternionBasePtr& q)
{
    std::stringstream ss;
    ss << q->qw << " | " << q->qx << " "  << q->qy << " " << q->qz << ")";
    return ss.str();
}

static Eigen::Vector3f toEigen(const Vector3BasePtr& v)
{
    return Eigen::Vector3f(v->x, v->y, v->z);
}
static Eigen::Quaternionf toEigen(const QuaternionBasePtr& q)
{
    return Eigen::Quaternionf(q->qw, q->qx, q->qy, q->qz);
}

enum class ShapeType
{
    BOX, CYLINDER, SPHERE, MESH_SHAPE
};


ShapeVariant::ShapeVariant() : ShapeVariant(0)
{
}

ShapeVariant::ShapeVariant(long id) :
    // v-this-v base constructor call is omitted due to virtual inheritance
    ShapeBase(id)
{
    this->id = id;
}
/*
ShapeVariant::ShapeVariant(const semrel::Shape& shape) : ShapeBase(shape.getID())
{
}
*/
std::string ShapeVariant::output(const Ice::Current&) const
{
    return toShapePtr()->str();
}

Ice::Int ShapeVariant::getType(const Ice::Current&) const
{
    return armarx::VariantType::ShapeVariant;
}

bool ShapeVariant::validate(const Ice::Current&)
{
    return true;
}

void ShapeVariant::serialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current&) const
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
    obj->setInt("id", id);
}

void ShapeVariant::deserialize(const armarx::ObjectSerializerBasePtr& serializer, const Ice::Current&)
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
    id = obj->getInt("id");
}

void ShapeVariant::initShape(semrel::Shape& shape) const
{
    shape.setID(semrel::ShapeID {id});
}

BoxVariant::BoxVariant() : BoxVariant(0, new Vector3(), new Quaternion(), new Vector3())
{
}

BoxVariant::BoxVariant(long id, const Vector3BasePtr& position,
                       const QuaternionBasePtr& orientation, const Vector3BasePtr& extents) :
    ShapeBase(id), BoxBase(id, position, orientation, extents), ShapeVariant(id)
{
}

BoxVariant::BoxVariant(const semrel::Box& box) :
    BoxVariant(box.getID(),
               new Vector3(box.getPosition()),
               new Quaternion(box.getOrientation()),
               new Vector3(box.getExtents()))
{
}

Vector3BasePtr BoxVariant::getPosition() const
{
    return position;
}

QuaternionBasePtr BoxVariant::getOrientation() const
{
    return orientation;
}

Vector3BasePtr BoxVariant::getExtents() const
{
    return extents;
}

semrel::Box BoxVariant::toBox() const
{
    semrel::Box box(toEigen(position), toEigen(orientation), toEigen(extents));
    initShape(box);
    return box;
}

std::unique_ptr<semrel::Shape> BoxVariant::toShapePtr() const
{
    return std::unique_ptr<semrel::Box>(new semrel::Box(toBox()));
}

Ice::ObjectPtr BoxVariant::ice_clone() const
{
    return this->clone();
}

VariantDataClassPtr BoxVariant::clone(const Ice::Current&) const
{
    return new BoxVariant(*this);
}

std::string BoxVariant::output(const Ice::Current&) const
{
    return toBox().str();
}

Ice::Int BoxVariant::getType(const Ice::Current&) const
{
    return VariantType::BoxVariant;
}

void BoxVariant::serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& c) const
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

    obj->setInt("type", int(ShapeType::BOX)); // type indicator for ShapeVariantList

    ShapeVariant::serialize(serializer, c);

    AbstractObjectSerializerPtr pos = obj->createElement();
    position->serialize(pos, c);
    obj->setElement("position", pos);

    AbstractObjectSerializerPtr ori = obj->createElement();
    orientation->serialize(ori, c);
    obj->setElement("orientation", ori);

    AbstractObjectSerializerPtr exts = obj->createElement();
    extents->serialize(exts, c);
    obj->setElement("extents", exts);
}

void BoxVariant::deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& c)
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

    ShapeVariant::deserialize(serializer, c);

    position = new Vector3();
    orientation = new Quaternion();
    extents = new Vector3();

    position->deserialize(obj->getElement("position"));
    orientation->deserialize(obj->getElement("orientation"));
    extents->deserialize(obj->getElement("extents"));
}

CylinderVariant::CylinderVariant() : CylinderVariant(0, new Vector3(), new Vector3(), 0, 0)
{
}

CylinderVariant::CylinderVariant(long id, const Vector3BasePtr& position,
                                 const Vector3BasePtr& direction,
                                 float radius, float height) :
    ShapeBase(id), CylinderBase(id, position, direction, radius, height), ShapeVariant(id)
{
}

CylinderVariant::CylinderVariant(const semrel::Cylinder& cyl) :
    CylinderVariant(cyl.getID(),
                    new Vector3(cyl.getPosition()),
                    new Vector3(cyl.getAxisDirection()),
                    cyl.getRadius(), cyl.getHeight())
{
}

Vector3BasePtr CylinderVariant::getPosition() const
{
    return position;
}

Vector3BasePtr CylinderVariant::getDirection() const
{
    return direction;
}

float CylinderVariant::getRadius() const
{
    return radius;
}

float CylinderVariant::getHeight() const
{
    return height;
}

semrel::Cylinder CylinderVariant::toCylinder() const
{
    semrel::Cylinder cyl(toEigen(position), toEigen(direction), radius, height);
    initShape(cyl);
    return cyl;
}

std::unique_ptr<semrel::Shape> CylinderVariant::toShapePtr() const
{
    return std::unique_ptr<semrel::Cylinder>(new semrel::Cylinder(toCylinder()));
}

Ice::ObjectPtr CylinderVariant::ice_clone() const
{
    return this->clone();
}

VariantDataClassPtr CylinderVariant::clone(const Ice::Current&) const
{
    return new CylinderVariant(*this);
}

std::string CylinderVariant::output(const Ice::Current&) const
{
    return toCylinder().str();
}

Ice::Int CylinderVariant::getType(const Ice::Current&) const
{
    return VariantType::CylinderVariant;
}

void CylinderVariant::serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& c) const
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

    obj->setInt("type", int(ShapeType::CYLINDER)); // type indicator for ShapeVariantList

    ShapeVariant::serialize(serializer, c);

    AbstractObjectSerializerPtr pos = obj->createElement();
    position->serialize(pos, c);
    obj->setElement("position", pos);

    AbstractObjectSerializerPtr dir = obj->createElement();
    direction->serialize(dir, c);
    obj->setElement("direction", dir);

    obj->setFloat("radius", radius);
    obj->setFloat("height", height);
}

void CylinderVariant::deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& c)
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

    position = new Vector3();
    direction = new Vector3();

    ShapeVariant::deserialize(serializer, c);
    position->deserialize(obj->getElement("position"));
    direction->deserialize(obj->getElement("direction"));
    radius = obj->getFloat("radius");
    height = obj->getFloat("height");
}

SphereVariant::SphereVariant() :
    SphereVariant(0, new Vector3(), 0)
{
}

SphereVariant::SphereVariant(long id, const Vector3BasePtr& position, float radius) :
    ShapeBase(id), SphereBase(id, position, radius), ShapeVariant(id)
{
}

SphereVariant::SphereVariant(const semrel::Sphere& sphere) :
    SphereVariant(sphere.getID(), new Vector3(sphere.getPosition()), sphere.getRadius())
{
}

Vector3BasePtr SphereVariant::getPosition() const
{
    return position;
}

float SphereVariant::getRadius() const
{
    return radius;
}

semrel::Sphere SphereVariant::toSphere() const
{
    semrel::Sphere sph(toEigen(position), radius);
    initShape(sph);
    return sph;
}

std::unique_ptr<semrel::Shape> SphereVariant::toShapePtr() const
{
    return std::unique_ptr<semrel::Sphere>(new semrel::Sphere(toSphere()));
}

Ice::ObjectPtr SphereVariant::ice_clone() const
{
    return this->clone();
}

VariantDataClassPtr SphereVariant::clone(const Ice::Current&) const
{
    return new SphereVariant(*this);
}

std::string SphereVariant::output(const Ice::Current&) const
{
    return toSphere().str();
}

Ice::Int SphereVariant::getType(const Ice::Current&) const
{
    return VariantType::SphereVariant;
}

void SphereVariant::serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& c) const
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

    obj->setInt("type", int(ShapeType::SPHERE)); // type indicator for ShapeVariantList

    ShapeVariant::serialize(serializer, c);

    AbstractObjectSerializerPtr pos = obj->createElement();
    position->serialize(pos, c);
    obj->setElement("position", pos);
    obj->setFloat("radius", radius);
}

void SphereVariant::deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& c)
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

    ShapeVariant::deserialize(serializer, c);

    position = new Vector3();

    position->deserialize(obj->getElement("position"));
    radius = obj->getFloat("radius");
}


MeshShapeVariant::MeshShapeVariant()
{
}

MeshShapeVariant::MeshShapeVariant(
    long id, const Vector3BasePtr& position,
    const QuaternionBasePtr& orientation, const DebugDrawerTriMesh& mesh) :
    ShapeBase(id), MeshShapeBase(id, position, orientation, mesh), ShapeVariant(id)
{
}

MeshShapeVariant::MeshShapeVariant(const semrel::MeshShape& meshShape) :
    MeshShapeVariant(meshShape.getID(),
                     new Vector3(meshShape.getPosition()),
                     new Quaternion(meshShape.getOrientation()),
                     DebugDrawerTriMesh())
{
    // fill internal debug drawer tri mesh

    const semrel::TriMesh& triMesh = meshShape.mesh();

    // add vertices
    for (const Eigen::Vector3f& v : triMesh.getVertices())
    {
        mesh.vertices.push_back(DebugDrawerVertex {v(0), v(1), v(2)});
    }

    // add normals
    for (auto tri : triMesh.getTriangles())
    {
        // debug drawer face allows only one normal per face, not per vertex
        // => compute face normal

        Eigen::Vector3f n = (triMesh.getNormal(tri.n0)
                             + triMesh.getNormal(tri.n1)
                             + triMesh.getNormal(tri.n2)).normalized();

        mesh.faces.push_back(DebugDrawerFace
        {
            DebugDrawerVertexID { tri.v0, 0, 0},
            DebugDrawerVertexID { tri.v1, 0, 0},
            DebugDrawerVertexID { tri.v2, 0, 0},
            DebugDrawerNormal { n(0), n(1), n(2) }
        });
    }

    // add one generic color
    mesh.colors.push_back(DrawColor { 0.5, 0.5, 0.5, 1.0 });
}

Vector3BasePtr MeshShapeVariant::getPosition() const
{
    return position;
}

QuaternionBasePtr MeshShapeVariant::getOrientation() const
{
    return orientation;
}

DebugDrawerTriMesh MeshShapeVariant::getMesh() const
{
    return mesh;
}

Eigen::Vector3f toEigen(DebugDrawerVertex v)
{
    return Eigen::Vector3f(v.x, v.y, v.z);
}

static void insertAt(std::vector<Eigen::Vector3f>& container, int index, Eigen::Vector3f const& value)
{
    if (index >= 0 && container.size() <= std::size_t(index))
    {
        container.resize(index + 1);
    }
    container[index] = value;
    // vertices may be written more than once, but I guess it is still
    // faster than tracking which vertices have been written so far
}

semrel::MeshShape MeshShapeVariant::toMeshShape() const
{
    // build TriMesh
    semrel::TriMesh triMesh;

    std::vector<Eigen::Vector3f>& vertices = triMesh.getVertices();
    std::vector<Eigen::Vector3f>& normals = triMesh.getNormals();

    auto insert = [this](std::vector<Eigen::Vector3f>& container, int vertexID, int offset)
    {
        int newID = vertexID - offset;
        if (newID < 0)
        {
            std::stringstream ss;
            ss << "normalID " << vertexID << " pointing to out of normal range (min normal index: " << offset << ")";
            throw std::logic_error(ss.str());
        }

        insertAt(container, newID, toEigen(mesh.vertices.at(vertexID)));
    };

    std::size_t normalID = 0;
    for (const DebugDrawerFace& face : mesh.faces)
    {
        insert(vertices, face.vertex1.vertexID, 0);
        insert(vertices, face.vertex2.vertexID, 0);
        insert(vertices, face.vertex3.vertexID, 0);

        Eigen::Vector3f normal(face.normal.x, face.normal.y, face.normal.z);
        // Insert the normal three times (once per vertex)
        normals.push_back(normal);

        triMesh.addTriangle(semrel::TriMesh::Triangle(
                                face.vertex1.vertexID,
                                face.vertex2.vertexID,
                                face.vertex3.vertexID,
                                normalID,
                                normalID,
                                normalID));
        normalID += 1;
    }

    semrel::MeshShape meshShape(toEigen(position), toEigen(orientation), triMesh);
    initShape(meshShape);
    return meshShape;
}

std::unique_ptr<semrel::Shape> MeshShapeVariant::toShapePtr() const
{
    return std::unique_ptr<semrel::MeshShape>(new semrel::MeshShape(toMeshShape()));
}

Ice::ObjectPtr MeshShapeVariant::ice_clone() const
{
    return this->clone();
}

VariantDataClassPtr MeshShapeVariant::clone(const Ice::Current&) const
{
    return new MeshShapeVariant(*this);
}

std::string MeshShapeVariant::output(const Ice::Current&) const
{
    return toMeshShape().str();
}

Ice::Int MeshShapeVariant::getType(const Ice::Current&) const
{
    return VariantType::MeshShapeVariant;
}


void MeshShapeVariant::serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& c) const
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

    obj->setInt("type", int(ShapeType::MESH_SHAPE)); // type indicator for ShapeVariantList

    ShapeVariant::serialize(serializer, c);

    AbstractObjectSerializerPtr pos = obj->createElement();
    position->serialize(pos, c);
    obj->setElement("position", pos);

    AbstractObjectSerializerPtr ori = obj->createElement();
    orientation->serialize(ori, c);
    obj->setElement("orientation", ori);


    auto serializeDrawColor = [](AbstractObjectSerializerPtr obj, const DrawColor & col)
    {
        obj->setFloat("r", col.r);
        obj->setFloat("g", col.g);
        obj->setFloat("b", col.b);
        obj->setFloat("a", col.a);
    };

    auto serializeVertex = [](AbstractObjectSerializerPtr obj, const DebugDrawerVertex & v)
    {
        obj->setFloat("x", v.x);
        obj->setFloat("y", v.y);
        obj->setFloat("z", v.z);
    };

    auto serializeFace = [](AbstractObjectSerializerPtr obj, const DebugDrawerFace & f)
    {
        obj->setInt("v1", f.vertex1.vertexID);
        obj->setInt("v2", f.vertex2.vertexID);
        obj->setInt("v3", f.vertex3.vertexID);

        obj->setInt("n1", f.vertex1.normalID);
        obj->setInt("n2", f.vertex2.normalID);
        obj->setInt("n3", f.vertex3.normalID);

        obj->setInt("c1", f.vertex1.colorID);
        obj->setInt("c2", f.vertex2.colorID);
        obj->setInt("c3", f.vertex3.colorID);

        obj->setFloat("nx", f.normal.x);
        obj->setFloat("ny", f.normal.y);
        obj->setFloat("nz", f.normal.z);
    };

    serializeVariantList<DrawColor>(mesh.colors, "mesh.colors", obj, serializeDrawColor);
    serializeVariantList<DebugDrawerVertex>(mesh.vertices, "mesh.vertices", obj, serializeVertex);
    serializeVariantList<DebugDrawerFace>(mesh.faces, "mesh.faces", obj, serializeFace);
}

void MeshShapeVariant::deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& c)
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

    ShapeVariant::deserialize(serializer, c);

    position = new Vector3();
    orientation = new Quaternion();

    position->deserialize(obj->getElement("position"));
    orientation->deserialize(obj->getElement("orientation"));

    auto deserializeDrawColor = [](AbstractObjectSerializerPtr obj)
    {
        DrawColor col;
        col.r = obj->getFloat("r");
        col.g = obj->getFloat("g");
        col.b = obj->getFloat("b");
        col.a = obj->getFloat("a");
        return col;
    };

    auto deserializeVertex = [](AbstractObjectSerializerPtr obj)
    {
        DebugDrawerVertex v;
        v.x = obj->getFloat("x");
        v.y = obj->getFloat("y");
        v.z = obj->getFloat("z");
        return v;
    };

    auto deserializeFace = [](AbstractObjectSerializerPtr obj)
    {
        DebugDrawerFace f;
        f.vertex1.vertexID = obj->getInt("v1");
        f.vertex2.vertexID = obj->getInt("v2");
        f.vertex3.vertexID = obj->getInt("v3");

        f.vertex1.normalID = obj->getInt("n1");
        f.vertex2.normalID = obj->getInt("n2");
        f.vertex3.normalID = obj->getInt("n3");

        f.vertex1.colorID = obj->getInt("c1");
        f.vertex2.colorID = obj->getInt("c2");
        f.vertex3.colorID = obj->getInt("c3");

        f.normal.x = obj->getFloat("nx");
        f.normal.y = obj->getFloat("ny");
        f.normal.z = obj->getFloat("nz");
        return f;
    };

    deserializeVariantList<DrawColor>(mesh.colors, "mesh.colors", obj, deserializeDrawColor);
    deserializeVariantList<DebugDrawerVertex>(mesh.vertices, "mesh.vertices", obj, deserializeVertex);
    deserializeVariantList<DebugDrawerFace>(mesh.faces, "mesh.faces", obj, deserializeFace);
}

ShapeVariantList::ShapeVariantList() = default;

ShapeVariantList::ShapeVariantList(const ShapeSequence& list) :
    ShapeBaseList(list)
{}


ShapeVariantList::ShapeVariantList(const semrel::ShapeList& shapeList)
{
    list.reserve(shapeList.size());
    for (const auto& shape : shapeList)
    {
        addShape(*shape);
    }
}

ShapeVariantList::ShapeVariantList(const semrel::ShapeMap& shapeMap)
{
    list.reserve(shapeMap.size());
    for (const auto& [id, shape] : shapeMap)
    {
        addShape(*shape);
    }
}


void ShapeVariantList::addShape(const semrel::Shape& shape)
{
    list.push_back(frost(shape));
}


semrel::ShapeList ShapeVariantList::toShapeList() const
{
    semrel::ShapeList shapeList;

    for (ShapeBasePtr shapeBase : list)
    {
        ShapeVariantPtr shapeVariant = dyncast(shapeBase);
        shapeList.push_back(shapeVariant->toShapePtr());
    }

    return shapeList;
}

ShapeSequence& ShapeVariantList::getList()
{
    return list;
}

const ShapeSequence& ShapeVariantList::getList() const
{
    return list;
}

std::size_t ShapeVariantList::size() const
{
    return list.size();
}

void ShapeVariantList::push_back(const ShapeBasePtr& item)
{
    list.push_back(item);
}

ShapeBasePtr& ShapeVariantList::at(std::size_t i)
{
    return list.at(i);
}

const ShapeBasePtr& ShapeVariantList::at(std::size_t i) const
{
    return list.at(i);
}

ShapeBasePtr& ShapeVariantList::operator[](std::size_t i)
{
    return list[i];
}

const ShapeBasePtr& ShapeVariantList::operator[](std::size_t i) const
{
    return list[i];
}

void ShapeVariantList::clear()
{
    list.clear();
}

Ice::ObjectPtr ShapeVariantList::ice_clone() const
{
    return this->clone();
}

VariantDataClassPtr ShapeVariantList::clone(const Ice::Current&) const
{
    return new ShapeVariantList(*this);
}

std::string ShapeVariantList::output(const Ice::Current&) const
{
    std::stringstream ss;
    ss << "+-- ShapeVariantList --\n";
    for (const ShapeBasePtr& shape : list)
    {
        ShapeVariantPtr variant = dyncast(shape);
        ss << "| " << variant->toShapePtr()->str() << "\n";
    }
    return ss.str();
}

Ice::Int ShapeVariantList::getType(const Ice::Current&) const
{
    return VariantType::ShapeVariantList;
}

bool ShapeVariantList::validate(const Ice::Current&)
{
    return true;
}

void ShapeVariantList::serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&) const
{
    serializeVariantList<ShapeBasePtr>(list, "list", serializer, VariantPtrSerializer<ShapeBasePtr>);
}

void ShapeVariantList::deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&)
{
    auto deserialize = [](AbstractObjectSerializerPtr obj) -> ShapeBasePtr
    {
        ShapeType type = ShapeType(obj->getInt("type"));

        // switch needs declaration out of switch scope
        BoxVariantPtr box;
        CylinderVariantPtr cyl;
        SphereVariantPtr sph;
        MeshShapeVariantPtr msh;

        ShapeVariantPtr shapeVar;

        switch (type)
        {
            case ShapeType::BOX:
                box = new BoxVariant();
                box->deserialize(obj);
                shapeVar = box;
                break;

            case ShapeType::CYLINDER:
                cyl = new CylinderVariant();
                cyl->deserialize(obj);
                shapeVar = cyl;
                break;
            case ShapeType::SPHERE:
                sph = new SphereVariant();
                sph->deserialize(obj);
                shapeVar = sph;
                break;

            case ShapeType::MESH_SHAPE:
                msh = new MeshShapeVariant();
                msh->deserialize(obj);
                shapeVar = msh;
                break;

            default:
                throw std::logic_error("Unknown/unsupported shape type in deserialization");
                break;
        }
        return shapeVar;
    };

    deserializeVariantList<ShapeBasePtr>(list, "list", serializer, deserialize);
}

ShapeVariantPtr ShapeVariantList::dyncast(const ShapeBasePtr& shapeBasePtr)
{
    ShapeVariantPtr variant = ShapeVariantPtr::dynamicCast(shapeBasePtr);
    if (variant)
    {
        return variant;
    }
    else
    {
        throw std::logic_error("Could not cast ShapeBasePtr to ShapeVariantPtr (item of ShapeVariantList)");
    }
}


semrel::ShapeList armarx::defrost(const ShapeBaseListPtr& shapeBase)
{
    return ShapeVariantListPtr::dynamicCast(shapeBase)->toShapeList();
}

ShapeVariantListPtr armarx::frost(const semrel::ShapeList& shapes)
{
    return new ShapeVariantList(shapes);
}

ShapeVariantListPtr armarx::frost(const semrel::ShapeMap& shapes)
{
    return new ShapeVariantList(shapes);
}

BoxVariantPtr armarx::frost(const semrel::Box& box)
{
    return new BoxVariant(box);
}

CylinderVariantPtr armarx::frost(const semrel::Cylinder& cylinder)
{
    return new CylinderVariant(cylinder);
}

SphereVariantPtr armarx::frost(const semrel::Sphere& sphere)
{
    return new SphereVariant(sphere);
}

MeshShapeVariantPtr armarx::frost(const semrel::MeshShape& mesh)
{
    return new MeshShapeVariant(mesh);
}

ShapeVariantPtr armarx::frost(const semrel::Shape& shape)
{
    if (auto box = dynamic_cast<const semrel::Box*>(&shape))
    {
        return new BoxVariant(*box);
    }
    else if (auto cyl = dynamic_cast<const semrel::Cylinder*>(&shape))
    {
        return new CylinderVariant(*cyl);
    }
    else if (auto sph = dynamic_cast<const semrel::Sphere*>(&shape))
    {
        return new SphereVariant(*sph);
    }
    else if (auto msh = dynamic_cast<const semrel::MeshShape*>(&shape))
    {
        return new MeshShapeVariant(*msh);
    }
    else
    {
        throw std::logic_error("Unknown/unsupported shape type (all attempted casts failed).");
    }
}

