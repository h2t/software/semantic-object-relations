/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SemantiX::ArmarXObjects::SemantiXVariants
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE SemantiX::ArmarXLibraries::SemantiXVariants

#define ARMARX_BOOST_TEST

#include <SemantiX/Test.h>
#include "../ShapeVariants.h"

#include <iostream>

#include <Eigen/Eigen>
#include <SemanticObjectRelations/Shapes.h>
#include <ArmarXCore/util/json/JSONObject.h>


using namespace armarx;
using namespace semrel;
using namespace Eigen;


namespace Eigen
{
    bool operator==(const Vector3f& lhs, const Vector3f& rhs)
    {
        return lhs.isApprox(rhs);
    }
    bool operator==(const Quaternionf& lhs, const Quaternionf& rhs)
    {
        return lhs.isApprox(rhs);
    }
    bool operator==(const Vector3f& lhs, const Vector3BasePtr& rhs)
    {
        return lhs(0) == rhs->x && lhs(1) == rhs->y && lhs(2) == rhs->z;
    }
    bool operator==(const Quaternionf& lhs, const QuaternionBasePtr& rhs)
    {
        return lhs.x() == rhs->qx && lhs.y() == rhs->qy && lhs.z() == rhs->qz
               && lhs.w() == rhs->qw;
    }

    std::ostream& operator<<(std::ostream& os, const Vector3f& v)
    {
        IOFormat iof(7, 0, " ", "", "", "", "[ ", " ]");
        return os << v.format(iof);
    }
    std::ostream& operator<<(std::ostream& os, const Quaternionf& q)
    {
        return os << "[ " << q.w() << " | " << q.x() << " " << q.y() << " " << q.z() << " ]";
    }
}


struct BoxFixture
{
    BoxFixture() :
        box(Vector3f(1.0, 2.0, 3.0), Quaternionf(AngleAxisf(30, Vector3f(1.0, 1.0, 1.0))),
            Vector3f(3.5, 2.5, 1.5), ShapeID {42}),
        bv(box)
    {}

    Box box;
    BoxVariant bv;
};

struct CylinderFixture
{
    CylinderFixture() :
        cyl(Vector3f(1.2f, 2.3f, 3.1f), Vector3f(-1.2f, 2.3f, 3.1f), 2.5, 3.5, ShapeID { -45}),
        cv(cyl)
    {}

    Cylinder cyl;
    CylinderVariant cv;
};

struct SphereFixture
{
    SphereFixture() :
        sph(Vector3f(2.0, 2.5, 3.7f), 4.5, ShapeID {41}), sv(sph)
    {}

    Sphere sph;
    SphereVariant sv;
};

struct MeshShapeFixture
{
    MeshShapeFixture() :
        msh(Vector3f(1.2f, -2.0, -13.0), Quaternionf(AngleAxisf(30, Vector3f(1.0, 1.0, 1.0))),
            CylinderFixture().cyl.getTriMeshLocal(),
            ShapeID { -42 }),
        mv(msh)
    {
    }

    MeshShape msh;
    MeshShapeVariant mv;
};



#define CHECK_EQUAL_BOX(b1, b2) do { \
        BOOST_CHECK_EQUAL(b1.getID(), b2.getID()); \
        BOOST_CHECK_EQUAL(b1.getPosition(), b2.getPosition()); \
        BOOST_CHECK_EQUAL(b1.getOrientation(), b2.getOrientation()); \
        BOOST_CHECK_EQUAL(b1.getExtents(), b2.getExtents()); \
    } while (false)

#define CHECK_EQUAL_BOXVARIANT(b, bv) do { \
        BOOST_CHECK_EQUAL(b.getID(), bv.id); \
        BOOST_CHECK_EQUAL(b.getPosition(), bv.position); \
        BOOST_CHECK_EQUAL(b.getOrientation(), bv.orientation); \
        BOOST_CHECK_EQUAL(b.getExtents(), bv.extents); \
    } while (false)


BOOST_FIXTURE_TEST_CASE(testBoxVariantConversion, BoxFixture)
{
    BoxVariant bv(box);
    CHECK_EQUAL_BOXVARIANT(box, bv);

    Box box2 = bv.toBox();
    CHECK_EQUAL_BOX(box, box2);
}

BOOST_FIXTURE_TEST_CASE(testBoxVariantSerialization, BoxFixture)
{
    AbstractObjectSerializerPtr serializer = new JSONObject();
    bv.serialize(serializer);

    BoxVariant bv2;
    bv2.deserialize(serializer);
    CHECK_EQUAL_BOXVARIANT(box, bv2);

    Box box2 = bv2.toBox();
    CHECK_EQUAL_BOX(box, box2);
}


#define CHECK_EQUAL_CYLINDER(c1, c2) do { \
        BOOST_CHECK_EQUAL(c1.getID(), c2.getID()); \
        BOOST_CHECK(c1.getPosition().isApprox(c2.getPosition())); \
        BOOST_CHECK(c1.getAxisDirection().isApprox(c2.getAxisDirection())); \
        BOOST_CHECK_EQUAL(c1.getRadius(), c2.getRadius()); \
        BOOST_CHECK_EQUAL(c1.getHeight(), c2.getHeight()); \
    } while (false)

#define CHECK_EQUAL_CYLINDERVARIANT(c, cv) do { \
        BOOST_CHECK_EQUAL(c.getID(), cv.id); \
        BOOST_CHECK_EQUAL(c.getPosition(), cv.position); \
        BOOST_CHECK_EQUAL(c.getAxisDirection(), cv.direction); \
        BOOST_CHECK_EQUAL(c.getRadius(), cv.radius); \
        BOOST_CHECK_EQUAL(c.getHeight(), cv.height); \
    } while (false)

BOOST_FIXTURE_TEST_CASE(testCylinderVariantConversion, CylinderFixture)
{
    CylinderVariant cv(cyl);
    CHECK_EQUAL_CYLINDERVARIANT(cyl, cv);

    Cylinder cyl2 = cv.toCylinder();
    CHECK_EQUAL_CYLINDER(cyl, cyl2);
}

BOOST_FIXTURE_TEST_CASE(testCylinderVariantSerialization, CylinderFixture)
{
    AbstractObjectSerializerPtr serializer = new JSONObject();
    cv.serialize(serializer);

    CylinderVariant cv2;
    cv2.deserialize(serializer);
    CHECK_EQUAL_CYLINDERVARIANT(cyl, cv2);

    Cylinder cyl2 = cv2.toCylinder();
    CHECK_EQUAL_CYLINDER(cyl, cyl2);
}


#define CHECK_EQUAL_SPHERE(s1, s2) do { \
        BOOST_CHECK_EQUAL(s1.getID(), s2.getID()); \
        BOOST_CHECK(s1.getPosition().isApprox(s2.getPosition())); \
        BOOST_CHECK_EQUAL(s1.getRadius(), s2.getRadius()); \
    } while (false)

#define CHECK_EQUAL_SPHEREVARIANT(s, sv) do { \
        BOOST_CHECK_EQUAL(s.getID(), sv.id); \
        BOOST_CHECK_EQUAL(s.getPosition(), sv.position); \
        BOOST_CHECK_EQUAL(s.getRadius(), sv.radius); \
    } while (false)

BOOST_FIXTURE_TEST_CASE(testSphereVariantConversion, SphereFixture)
{
    CHECK_EQUAL_SPHEREVARIANT(sph, sv);

    Sphere sph2 = sv.toSphere();
    CHECK_EQUAL_SPHERE(sph, sph2);
}

BOOST_FIXTURE_TEST_CASE(testSphereVariantSerialization, SphereFixture)
{
    AbstractObjectSerializerPtr serializer = new JSONObject();
    sv.serialize(serializer);

    SphereVariant sv2;
    sv2.deserialize(serializer);
    CHECK_EQUAL_SPHEREVARIANT(sph, sv2);

    Sphere sph2 = sv2.toSphere();
    CHECK_EQUAL_SPHERE(sph, sph2);
}


// Cannot check normals, since the MeshShapeVariant only stores one normal per face
// Therefore the triangles can only be checked on the vertex level
#define CHECK_EQUAL_MESHSHAPE(m1, m2) do { \
        BOOST_CHECK_EQUAL(m1.getID(), m2.getID()); \
        BOOST_CHECK_EQUAL(m1.getPosition(), m2.getPosition()); \
        BOOST_CHECK_EQUAL(m1.getOrientation(), m2.getOrientation()); \
        BOOST_CHECK_EQUAL_COLLECTIONS(m1.mesh().getVertices().begin(), m1.mesh().getVertices().end(), \
                                      m2.mesh().getVertices().begin(), m2.mesh().getVertices().end()); \
        BOOST_CHECK_EQUAL(m1.mesh().getTriangles().size(), m2.mesh().getTriangles().size()); \
    } while (false)


#define CHECK_EQUAL_MESHSHAPEVARIANT(m, mv) do { \
        BOOST_CHECK_EQUAL(m.mesh().numVertices(), mv.mesh.vertices.size()); \
        BOOST_CHECK_EQUAL(m.mesh().numTriangles(), mv.mesh.faces.size()); \
        BOOST_CHECK_GE(mv.mesh.colors.size(), 1); \
    } while (false)

BOOST_FIXTURE_TEST_CASE(testMeshShapeVariantConversion, MeshShapeFixture)
{
    BOOST_CHECK_EQUAL(msh.getID(), mv.id);
    BOOST_CHECK_EQUAL(msh.getPosition(), mv.position);
    BOOST_CHECK_EQUAL(msh.getOrientation(), mv.orientation);

    // rough checking against internal mesh structure in MeshShapeVariant
    CHECK_EQUAL_MESHSHAPEVARIANT(msh, mv);


    MeshShape msh2 = mv.toMeshShape();
    CHECK_EQUAL_MESHSHAPE(msh, msh2);
}

BOOST_FIXTURE_TEST_CASE(testMeshShapeVariantSerialization, MeshShapeFixture)
{
    AbstractObjectSerializerPtr serializer = new JSONObject();
    mv.serialize(serializer);

    MeshShapeVariant mv2;
    mv2.deserialize(serializer);
    CHECK_EQUAL_MESHSHAPEVARIANT(msh, mv2);

    MeshShape msh2 = mv2.toMeshShape();
    CHECK_EQUAL_MESHSHAPE(msh, msh2);
}


struct ShapeVariantListFixture :
    public BoxFixture, public CylinderFixture,
    public SphereFixture, public MeshShapeFixture
{
    ShapeVariantListFixture() :
        list(), varList()
    {
        list.push_back(std::unique_ptr<Box>(new Box(box)));
        list.push_back(std::unique_ptr<Cylinder>(new Cylinder(cyl)));
        list.push_back(std::unique_ptr<Sphere>(new Sphere(sph)));
        list.push_back(std::unique_ptr<MeshShape>(new MeshShape(msh)));

        varList = ShapeVariantList(list);
    }

    ShapeList list;
    ShapeVariantList varList;

    static Box& getBox(ShapeList& list)
    {
        return *dynamic_cast<Box*>(list[0].get());
    }
    static Cylinder& getCyl(ShapeList& list)
    {
        return *dynamic_cast<Cylinder*>(list[1].get());
    }
    static Sphere& getSph(ShapeList& list)
    {
        return *dynamic_cast<Sphere*>(list[2].get());
    }
    static MeshShape& getMsh(ShapeList& list)
    {
        return *dynamic_cast<MeshShape*>(list[3].get());
    }

    static BoxVariant& getBoxVar(ShapeVariantList& varList)
    {
        return *BoxVariantPtr::dynamicCast(varList[0]);
    }
    static CylinderVariant& getCylVar(ShapeVariantList& varList)
    {
        return *CylinderVariantPtr::dynamicCast(varList[1]);
    }
    static SphereVariant& getSphVar(ShapeVariantList& varList)
    {
        return *SphereVariantPtr::dynamicCast(varList[2]);
    }
    static MeshShapeVariant& getMshVar(ShapeVariantList& varList)
    {
        return *MeshShapeVariantPtr::dynamicCast(varList[3]);
    }

};


#define CHECK_EQUAL_SHAPELIST(list1, list2) do { \
        BOOST_CHECK_EQUAL(list1.size(), list2.size()); \
        CHECK_EQUAL_BOX(getBox(list1), getBox(list2)); \
        CHECK_EQUAL_CYLINDER(getCyl(list1), getCyl(list2)); \
        CHECK_EQUAL_SPHERE(getSph(list1), getSph(list2)); \
        CHECK_EQUAL_MESHSHAPE(getMsh(list1), getMsh(list2)); \
    } while (false);

#define CHECK_EQUAL_SHAPELISTVARIANT(list, varList) do { \
        BOOST_CHECK_EQUAL(list.size(), varList.size()); \
        CHECK_EQUAL_BOXVARIANT(getBox(list), getBoxVar(varList)); \
        CHECK_EQUAL_CYLINDERVARIANT(getCyl(list), getCylVar(varList)); \
        CHECK_EQUAL_SPHEREVARIANT(getSph(list), getSphVar(varList)); \
        CHECK_EQUAL_MESHSHAPEVARIANT(getMsh(list), getMshVar(varList)); \
    } while (false);


BOOST_FIXTURE_TEST_CASE(testShapeVariantListConversion, ShapeVariantListFixture)
{
    CHECK_EQUAL_SHAPELISTVARIANT(list, varList);

    ShapeList list2 = varList.toShapeList();
    CHECK_EQUAL_SHAPELIST(list, list2);
}


BOOST_FIXTURE_TEST_CASE(testShapeVariantListSerialization, ShapeVariantListFixture)
{
    AbstractObjectSerializerPtr serializer = new JSONObject();
    varList.serialize(serializer);

    ShapeVariantList varList2;
    varList2.deserialize(serializer);
    CHECK_EQUAL_SHAPELISTVARIANT(list, varList2);

    ShapeList list2 = varList2.toShapeList();
    CHECK_EQUAL_SHAPELIST(list, list2);
}





