/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SemantiX::ArmarXObjects::SemantiXVariants
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE SemantiX::ArmarXLibraries::SemantiXVariants

#define ARMARX_BOOST_TEST

#include <SemantiX/Test.h>
#include "../SupportGraphVariant.h"

#include <iostream>

#include <Eigen/Eigen>
#include <SemanticObjectRelations/Shapes.h>
#include <ArmarXCore/util/json/JSONObject.h>


using namespace armarx;
using namespace semrel;
using namespace Eigen;


using Vertex = RelationGraph<SupportVertex, SupportEdge>::Vertex;
using Edge = RelationGraph<SupportVertex, SupportEdge>::Edge;

template <class ShapeT>
static auto makeShape(int id) -> std::unique_ptr<ShapeT>
{
    return std::unique_ptr<ShapeT>(new ShapeT(ShapeID{id}));
}

struct SupportGraphFixture
{
    SupportGraphFixture()
    {
        objects.push_back(makeShape<Sphere>(-5));
        objects.push_back(makeShape<Box>(2));
        objects.push_back(makeShape<MeshShape>(-5));
        objects.push_back(makeShape<Cylinder>(10));

        SupportGraph::Vertex v = graph.addVertex(*objects[0]);
        v.attrib().ud.enabled = false;
        v.attrib().ud.supportAreaRatio = 0;
        v.attrib().ud.safe = false;

        v = graph.addVertex(*objects[1]);
        v.attrib().ud.enabled = true;
        v.attrib().ud.supportAreaRatio = 1.0;
        v.attrib().ud.safe = true;

        v = graph.addVertex(*objects[2]);
        v.attrib().ud.enabled = true;
        v.attrib().ud.supportAreaRatio = 0.5;
        v.attrib().ud.safe = false;

        v = graph.addVertex(*objects[3]);
        v.attrib().ud.enabled = true;
        v.attrib().ud.supportAreaRatio = 0.6;
        v.attrib().ud.safe = true;

        graphVariant = SupportGraphVariant(graph);
    }

    ShapeList objects;
    SupportGraph graph;
    SupportGraphVariant graphVariant;
};


namespace boost
{
    bool operator!=(const Vertex& lhs, const SupportVertexBasePtr& rhs)
    {
        BOOST_CHECK_EQUAL(lhs.objectID(), rhs->shapeID);
        BOOST_CHECK_EQUAL(lhs.attrib().ud.enabled, rhs->udEnabled);
        BOOST_CHECK_EQUAL(lhs.attrib().ud.supportAreaRatio, rhs->udSupportAreaRatio);
        BOOST_CHECK_EQUAL(lhs.attrib().ud.safe, rhs->udSafe);
        return false;
    }

    bool operator!=(const Edge& lhs, const SupportEdgeBasePtr& rhs)
    {
        BOOST_CHECK_EQUAL(lhs.sourceObjectID(), rhs->sourceID);
        BOOST_CHECK_EQUAL(lhs.targetObjectID(), rhs->targetID);
        BOOST_CHECK_EQUAL(lhs.attrib().verticalSeparatingPlane, rhs->verticalSeparatingPlane);
        BOOST_CHECK_EQUAL(lhs.attrib().fromUncertaintyDetection, rhs->fromUncertaintyDetection);
        return false;
    }

    bool operator!=(const Vertex& lhs, const Vertex& rhs)
    {
        BOOST_CHECK_EQUAL(lhs.objectID(), rhs.objectID());
        BOOST_CHECK_EQUAL(lhs.attrib().ud.enabled, rhs.attrib().ud.enabled);
        BOOST_CHECK_EQUAL(lhs.attrib().ud.supportAreaRatio, rhs.attrib().ud.supportAreaRatio);
        BOOST_CHECK_EQUAL(lhs.attrib().ud.safe, rhs.attrib().ud.safe);
        return false;
    }

    bool operator!=(const Edge& lhs, const Edge& rhs)
    {
        BOOST_CHECK_EQUAL(lhs.sourceObjectID(), rhs.sourceObjectID());
        BOOST_CHECK_EQUAL(lhs.targetObjectID(), rhs.targetObjectID());
        BOOST_CHECK_EQUAL(lhs.attrib().verticalSeparatingPlane, rhs.attrib().verticalSeparatingPlane);
        BOOST_CHECK_EQUAL(lhs.attrib().fromUncertaintyDetection, rhs.attrib().fromUncertaintyDetection);
        return false;
    }
}


BOOST_FIXTURE_TEST_CASE(testSupportGraphVariantConversion, SupportGraphFixture)
{
    BOOST_CHECK_EQUAL_COLLECTIONS(graph.vertices().begin(), graph.vertices().end(),
                                  graphVariant.vertices.begin(), graphVariant.vertices.end());
    BOOST_CHECK_EQUAL_COLLECTIONS(graph.edges().begin(), graph.edges().end(),
                                  graphVariant.edges.begin(), graphVariant.edges.end());


    SupportGraph graph2 = graphVariant.toSupportGraph();

    BOOST_CHECK_EQUAL_COLLECTIONS(graph.vertices().begin(), graph.vertices().end(),
                                  graph2.vertices().begin(), graph2.vertices().end());
    BOOST_CHECK_EQUAL_COLLECTIONS(graph.edges().begin(), graph.edges().end(),
                                  graph2.edges().begin(), graph2.edges().end());
}

BOOST_FIXTURE_TEST_CASE(testSupportGraphVariantSerialization, SupportGraphFixture)
{
    AbstractObjectSerializerPtr serializer = new JSONObject();
    graphVariant.serialize(serializer);

    SupportGraphVariant var2;
    var2.deserialize(serializer);

    BOOST_CHECK_EQUAL_COLLECTIONS(graph.vertices().begin(), graph.vertices().end(),
                                  var2.vertices.begin(), var2.vertices.end());

    SupportGraph graph2 = var2.toSupportGraph();

    BOOST_CHECK_EQUAL_COLLECTIONS(graph.edges().begin(), graph.edges().end(),
                                  graph2.edges().begin(), graph2.edges().end());
}

