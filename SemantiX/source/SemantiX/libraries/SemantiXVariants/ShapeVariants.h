/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SemantiX::ArmarXObjects::ShapeVariants
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/observers/variant/Variant.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

#include <SemanticObjectRelations/Shapes.h>
#include <SemantiX/interface/Shapes.h>


namespace armarx
{
    namespace VariantType
    {
        const VariantTypeId ShapeVariant = Variant::addTypeName("::armarx::ShapeBase");
        const VariantTypeId BoxVariant = Variant::addTypeName("::armarx::BoxBase");
        const VariantTypeId CylinderVariant = Variant::addTypeName("::armarx::CylinderBase");
        const VariantTypeId SphereVariant = Variant::addTypeName("::armarx::SphereBase");
        const VariantTypeId MeshShapeVariant = Variant::addTypeName("::armarx::MeshShapeBase");
        const VariantTypeId ShapeVariantList = Variant::addTypeName("::armarx::ShapeBaseList");
    }


    class ShapeVariant : virtual public ShapeBase
    {
        template <class BaseClass, class VariantClass> friend class GenericFactory;

    public:

        ShapeVariant();
        explicit ShapeVariant(long id);
        //explicit ShapeVariant(const semrel::Shape& shape);

        long getID() const;

        virtual std::string output(const Ice::Current& = Ice::emptyCurrent) const override;
        virtual Ice::Int getType(const Ice::Current& = Ice::emptyCurrent) const override;
        virtual bool validate(const Ice::Current& = Ice::emptyCurrent) override;

        // Serializable interface
        virtual void serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& = Ice::emptyCurrent) const override;
        virtual void deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& = Ice::emptyCurrent) override;

        // ShapeVariant interface
        virtual std::unique_ptr<semrel::Shape> toShapePtr() const = 0;


    protected:

        /// Init the attributes of the given shape with the attributes belonging to Shape.
        void initShape(semrel::Shape& shape) const;

    };

    using ShapeVariantPtr = IceInternal::Handle<ShapeVariant>;


    class BoxVariant : virtual public BoxBase, virtual public ShapeVariant
    {
        template <class BaseClass, class VariantClass> friend class GenericFactory;

    public:

        BoxVariant();
        BoxVariant(long id,
                   const Vector3BasePtr& position,
                   const QuaternionBasePtr& orientation,
                   const Vector3BasePtr& extents);
        explicit BoxVariant(const semrel::Box& box);

        Vector3BasePtr getPosition() const;
        QuaternionBasePtr getOrientation() const;
        Vector3BasePtr getExtents() const;

        semrel::Box toBox() const;
        virtual std::unique_ptr<semrel::Shape> toShapePtr() const override;

        virtual Ice::ObjectPtr ice_clone() const override;
        virtual VariantDataClassPtr clone(const Ice::Current& = Ice::emptyCurrent) const override;
        virtual std::string output(const Ice::Current& = Ice::emptyCurrent) const override;
        virtual Ice::Int getType(const Ice::Current& = Ice::emptyCurrent) const override;

        // Serializable interface
        virtual void serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& = Ice::emptyCurrent) const override;
        virtual void deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& = Ice::emptyCurrent) override;

    };

    using BoxVariantPtr = IceInternal::Handle<BoxVariant>;


    class CylinderVariant : virtual public CylinderBase, virtual public ShapeVariant
    {
        template <class BaseClass, class VariantClass> friend class GenericFactory;

    public:

        CylinderVariant();
        CylinderVariant(long id,
                        const Vector3BasePtr& position,
                        const Vector3BasePtr& direction,
                        float radius, float height);
        explicit CylinderVariant(const semrel::Cylinder& cyl);

        Vector3BasePtr getPosition() const;
        Vector3BasePtr getDirection() const;
        float getRadius() const;
        float getHeight() const;

        semrel::Cylinder toCylinder() const;
        virtual std::unique_ptr<semrel::Shape> toShapePtr() const override;


        virtual Ice::ObjectPtr ice_clone() const override;
        virtual VariantDataClassPtr clone(const Ice::Current& = Ice::emptyCurrent) const override;
        virtual std::string output(const Ice::Current& = Ice::emptyCurrent) const override;
        virtual Ice::Int getType(const Ice::Current& = Ice::emptyCurrent) const override;

        // Serializable interface
        virtual void serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& = Ice::emptyCurrent) const override;
        virtual void deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& = Ice::emptyCurrent) override;

    };

    using CylinderVariantPtr = IceInternal::Handle<CylinderVariant>;


    class SphereVariant : virtual public SphereBase, virtual public ShapeVariant
    {
        template <class BaseClass, class VariantClass> friend class GenericFactory;

    public:

        SphereVariant();
        SphereVariant(long id, const Vector3BasePtr& position, float radius);
        explicit SphereVariant(const semrel::Sphere& sphere);

        Vector3BasePtr getPosition() const;
        float getRadius() const;

        semrel::Sphere toSphere() const;
        virtual std::unique_ptr<semrel::Shape> toShapePtr() const override;


        virtual Ice::ObjectPtr ice_clone() const override;
        virtual VariantDataClassPtr clone(const Ice::Current& = Ice::emptyCurrent) const override;
        virtual std::string output(const Ice::Current& = Ice::emptyCurrent) const override;
        virtual Ice::Int getType(const Ice::Current& = Ice::emptyCurrent) const override;

        // Serializable interface
        virtual void serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& = Ice::emptyCurrent) const override;
        virtual void deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& = Ice::emptyCurrent) override;

    };

    using SphereVariantPtr = IceInternal::Handle<SphereVariant>;


    class MeshShapeVariant : virtual public MeshShapeBase, virtual public ShapeVariant
    {
        template <class BaseClass, class VariantClass> friend class GenericFactory;

    public:

        MeshShapeVariant();
        MeshShapeVariant(long id, const Vector3BasePtr& position, const QuaternionBasePtr& orientation,
                         const DebugDrawerTriMesh& mesh);
        explicit MeshShapeVariant(const semrel::MeshShape& meshShape);

        Vector3BasePtr getPosition() const;
        QuaternionBasePtr getOrientation() const;
        DebugDrawerTriMesh getMesh() const;

        semrel::MeshShape toMeshShape() const;
        virtual std::unique_ptr<semrel::Shape> toShapePtr() const override;


        virtual Ice::ObjectPtr ice_clone() const override;
        virtual VariantDataClassPtr clone(const Ice::Current& = Ice::emptyCurrent) const override;
        virtual std::string output(const Ice::Current& = Ice::emptyCurrent) const override;
        virtual Ice::Int getType(const Ice::Current& = Ice::emptyCurrent) const override;

        // Serializable interface
        virtual void serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& = Ice::emptyCurrent) const override;
        virtual void deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& = Ice::emptyCurrent) override;

    };

    using MeshShapeVariantPtr = IceInternal::Handle<MeshShapeVariant>;


    BoxVariantPtr frost(const semrel::Box& box);
    CylinderVariantPtr frost(const semrel::Cylinder& cylinder);
    SphereVariantPtr frost(const semrel::Sphere& sphere);
    MeshShapeVariantPtr frost(const semrel::MeshShape& mesh);
    ShapeVariantPtr frost(const semrel::Shape& shape);


    class ShapeVariantList : public ShapeBaseList
    {

    public:

        ShapeVariantList();

        explicit ShapeVariantList(const ShapeSequence& list);
        explicit ShapeVariantList(const semrel::ShapeList& shapeList);
        explicit ShapeVariantList(const semrel::ShapeMap& shapeList);

        semrel::ShapeList toShapeList() const;

        ShapeSequence& getList();
        const ShapeSequence& getList() const;

        // forward some operations on internal list
        std::size_t size() const;

        ShapeBasePtr& at(std::size_t i);
        const ShapeBasePtr& at(std::size_t i) const;

        ShapeBasePtr& operator[](std::size_t i);
        const ShapeBasePtr& operator[](std::size_t i) const;

        void clear();
        void push_back(const ShapeBasePtr& item);


        // VariantDataClass interface
        virtual Ice::ObjectPtr ice_clone() const override;
        virtual VariantDataClassPtr clone(const Ice::Current& = Ice::emptyCurrent) const override;
        virtual std::string output(const Ice::Current& = Ice::emptyCurrent) const override;
        virtual Ice::Int getType(const Ice::Current& = Ice::emptyCurrent) const override;
        virtual bool validate(const Ice::Current& = Ice::emptyCurrent) override;

        // Serializable interface
        virtual void serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& = Ice::emptyCurrent) const override;
        virtual void deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& = Ice::emptyCurrent) override;


    private:

        static ShapeVariantPtr dyncast(const ShapeBasePtr& shapeBasePtr);

        void addShape(const semrel::Shape& shape);

    };

    using ShapeVariantListPtr = IceInternal::Handle<ShapeVariantList>;


    semrel::ShapeList defrost(const ShapeBaseListPtr& shapeBase);
    ShapeVariantListPtr frost(const semrel::ShapeList& shapes);
    ShapeVariantListPtr frost(const semrel::ShapeMap& shapes);

}
