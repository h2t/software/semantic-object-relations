#pragma once

#include <ArmarXCore/observers/variant/Variant.h>

#include <SemanticObjectRelations/SupportAnalysis/SupportGraph.h>

#include <SemantiX/interface/SupportGraph.h>



namespace armarx
{

    namespace VariantType
    {
        const VariantTypeId SupportVertexVariant = Variant::addTypeName("::armarx::SupportVertexBase");
        const VariantTypeId SupportEdgeVariant = Variant::addTypeName("::armarx::SupportEdgeBase");
        const VariantTypeId SupportGraphVariant = Variant::addTypeName("::armarx::SupportGraphBase");
    }


    class SupportVertexVariant : virtual public SupportVertexBase
    {
        template <class BaseClass, class VariantClass> friend class GenericFactory;

    public:

        SupportVertexVariant();
        SupportVertexVariant(long shapeID, bool udEnabled, float udSupportAreaRatio, bool udSafe);
        explicit SupportVertexVariant(const semrel::SupportGraph::ConstVertex& vertex);

        /**
         * @brief Converts this to a `semrel::SupportVertex`.
         * The object pointer is set to nullptr.
         */
        semrel::SupportVertex toVertexAttrib() const;

        // VariantDataClass interface
        virtual Ice::ObjectPtr ice_clone() const override;
        virtual VariantDataClassPtr clone(const Ice::Current& = Ice::emptyCurrent) const override;
        virtual std::string output(const Ice::Current& = Ice::emptyCurrent) const override;
        virtual Ice::Int getType(const Ice::Current& = Ice::emptyCurrent) const override;
        virtual bool validate(const Ice::Current& = Ice::emptyCurrent) override;

        // Serializable interface
        virtual void serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& = Ice::emptyCurrent) const override;
        virtual void deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& = Ice::emptyCurrent) override;
    };

    using SupportVertexVariantPtr = IceInternal::Handle<SupportVertexVariant>;


    class SupportEdgeVariant : virtual public SupportEdgeBase
    {
        template <class BaseClass, class VariantClass> friend class GenericFactory;

    public:

        SupportEdgeVariant();
        SupportEdgeVariant(long sourceID, long targetID, bool verticalSeparatingPlane, bool fromUncertaintyDetection);
        explicit SupportEdgeVariant(const semrel::SupportGraph::ConstEdge& edge);

        semrel::SupportEdge toEdgeAttrib() const;

        // VariantDataClass interface
        virtual Ice::ObjectPtr ice_clone() const override;
        virtual VariantDataClassPtr clone(const Ice::Current& = Ice::emptyCurrent) const override;
        virtual std::string output(const Ice::Current& = Ice::emptyCurrent) const override;
        virtual Ice::Int getType(const Ice::Current& = Ice::emptyCurrent) const override;
        virtual bool validate(const Ice::Current& = Ice::emptyCurrent) override;

        // Serializable interface
        virtual void serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& = Ice::emptyCurrent) const override;
        virtual void deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& = Ice::emptyCurrent) override;
    };

    using SupportEdgeVariantPtr = IceInternal::Handle<SupportEdgeVariant>;


    class SupportGraphVariant : virtual public SupportGraphBase
    {
        template <class BaseClass, class VariantClass> friend class GenericFactory;

    public:

        SupportGraphVariant();
        SupportGraphVariant(const SupportVertexBaseList& vertices, const SupportEdgeBaseList& edges);
        explicit SupportGraphVariant(const semrel::SupportGraph& supportGraph);

        /// Get `this` as `semrel::SupportGraph` with set object IDs. Object pointers will be nul.
        semrel::SupportGraph toSupportGraph() const;

        // VariantDataClass interface
        virtual Ice::ObjectPtr ice_clone() const override;
        virtual VariantDataClassPtr clone(const Ice::Current& = Ice::emptyCurrent) const override;
        virtual std::string output(const Ice::Current& = Ice::emptyCurrent) const override;
        virtual Ice::Int getType(const Ice::Current& = Ice::emptyCurrent) const override;
        virtual bool validate(const Ice::Current& = Ice::emptyCurrent) override;


        // Serializable interface
        virtual void serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& = Ice::emptyCurrent) const override;
        virtual void deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current& = Ice::emptyCurrent) override;

    private:

        /**
         * @brief Add a vertex to `graph` from `vertex` and the given object.
         * @throw std::invalid_argument If `vertex` cannot be cast to `SupportVertexVariant`.
         */
        static void addVertexFromVariant(semrel::SupportGraph& graph,
                                         const SupportVertexBasePtr& vertex,
                                         const semrel::Shape* object = nullptr);
        /**
         * @brief Add an edge to `graph` from `edge`.
         * @throw std::invalid_argument If `edge` cannot be cast to `SupportEdgeVariant`.
         */
        static void addEdgeFromVariant(semrel::SupportGraph& graph, const SupportEdgeBasePtr& edge);

    };

    using SupportGraphVariantPtr = IceInternal::Handle<SupportGraphVariant>;


    SupportGraphVariantPtr frost(const semrel::SupportGraph& supportGraph);
    semrel::SupportGraph defrost(const SupportGraphBasePtr& supportGraph);

}
