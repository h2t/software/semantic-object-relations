#pragma once

#include <ArmarXCore/core/system/FactoryCollectionBase.h>

#include "ShapeVariants.h"
#include "SupportGraphVariant.h"


namespace armarx
{
    namespace ObjectFactories
    {
        class ShapePrimitivesFactories : public FactoryCollectionBase
        {
        public:
            ObjectFactoryMap getFactories() override
            {
                ObjectFactoryMap map;

                add<armarx::BoxBase, armarx::BoxVariant>(map);
                add<armarx::CylinderBase, armarx::CylinderVariant>(map);
                add<armarx::SphereBase, armarx::SphereVariant>(map);
                add<armarx::MeshShapeBase, armarx::MeshShapeVariant>(map);
                add<armarx::ShapeBaseList, armarx::ShapeVariantList>(map);

                add<armarx::SupportVertexBase, armarx::SupportVertexVariant>(map);
                add<armarx::SupportEdgeBase, armarx::SupportEdgeVariant>(map);
                add<armarx::SupportGraphBase, armarx::SupportGraphVariant>(map);

                return map;
            }
            static const FactoryCollectionBaseCleanUp ShapePrimitivesFactoriesVar;
        };
    }
}

