#pragma once


#include <SemantiX/interface/Shapes.ice>


module semantix
{

    interface ShapeListener
    {
        void reportShapes(TimedShapeBaseList timedShapeList);
    };

};

