#pragma once

#include <ArmarXCore/interface/observers/VariantBase.ice>

module armarx
{

    class SupportVertexBase extends armarx::VariantDataClass
    {
        long shapeID; // the ID of the shape corresponding to this vertex

        bool udEnabled;
        float udSupportAreaRatio;
        bool udSafe = true;
    };

    class SupportEdgeBase extends armarx::VariantDataClass
    {
        long sourceID; // source object ID
        long targetID; // target object ID

        bool verticalSeparatingPlane;
        bool fromUncertaintyDetection;
    };

    sequence<SupportVertexBase> SupportVertexBaseList;
    sequence<SupportEdgeBase> SupportEdgeBaseList;

    class SupportGraphBase extends armarx::VariantDataClass
    {
        SupportVertexBaseList vertices;
        SupportEdgeBaseList edges;
    };

};


module semantix
{
    struct TimedSupportGraphBase
    {
        long timestampMicroseconds;
        armarx::SupportGraphBase supportGraph;
    };
};

