#pragma once

#include <VisionX/interface/core/PointCloudProcessorInterface.ice>
#include <VisionX/interface/core/DataTypes.ice>

module armarx
{

    interface SegmentFilterInterface extends visionx::PointCloudProcessorInterface
    {
        void setNumSegments(int numSegments);
    };

    interface SegmentFilterListener
    {
        void reportFilteredSegments(visionx::ColoredLabeledPointCloud pointCloud);
    };

};
