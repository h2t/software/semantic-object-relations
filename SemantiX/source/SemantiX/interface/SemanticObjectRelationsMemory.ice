#pragma once


#include <SemantiX/interface/ShapeListener.ice>
#include <SemantiX/interface/SupportGraphListener.ice>


module semantix
{

    interface SemanticObjectRelationsMemoryInterface extends ShapeListener, SupportGraphListener
    {
        TimedShapeBaseList getLatestShapes();
        TimedSupportGraphBase getLatestSupportGraph();
    };

};

