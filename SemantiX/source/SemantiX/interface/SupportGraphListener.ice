#pragma once


#include <SemantiX/interface/SupportGraph.ice>


module semantix
{

    interface SupportGraphListener
    {
        void reportSupportGraph(TimedSupportGraphBase timedSupportGraph);
    };

};

