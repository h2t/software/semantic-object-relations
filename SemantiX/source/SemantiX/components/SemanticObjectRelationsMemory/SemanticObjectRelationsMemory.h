/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SemantiX::ArmarXObjects::SemanticObjectRelationsMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <RobotAPI/libraries/core/visualization/DebugDrawerTopic.h>

#include <SemantiX/interface/SemanticObjectRelationsMemory.h>
#include <SemanticObjectRelations/Shapes/shape_containers.h>
#include <SemanticObjectRelations/SupportAnalysis/SupportGraph.h>


#include "History.h"


namespace semantix
{
    /// @class SemanticObjectRelationsMemoryPropertyDefinitions
    class SemanticObjectRelationsMemoryPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        SemanticObjectRelationsMemoryPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-SemanticObjectRelationsMemory SemanticObjectRelationsMemory
     * @ingroup SemantiX-Components
     * A description of the component SemanticObjectRelationsMemory.
     *
     * @class SemanticObjectRelationsMemory
     * @ingroup Component-SemanticObjectRelationsMemory
     * @brief Brief description of class SemanticObjectRelationsMemory.
     *
     * Detailed description of class SemanticObjectRelationsMemory.
     */
    class SemanticObjectRelationsMemory :
        virtual public armarx::Component,
        virtual public SemanticObjectRelationsMemoryInterface
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        virtual std::string getDefaultName() const override;

        // ShapeListener interface
        void reportShapes(const TimedShapeBaseList& shapes, const Ice::Current& = Ice::emptyCurrent) override;

        // SupportGraphListener interface
        void reportSupportGraph(const TimedSupportGraphBase& supportGraph, const Ice::Current& = Ice::emptyCurrent) override;


        // SemanticObjectRelationsMemoryInterface interface
        TimedShapeBaseList getLatestShapes(const Ice::Current& = Ice::emptyCurrent) override;
        TimedSupportGraphBase getLatestSupportGraph(const Ice::Current& = Ice::emptyCurrent) override;


    protected:

        virtual void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        virtual void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        virtual void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        virtual void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        void reportLatest();


    private:

        // Private methods and member variables go here.

        /// Debug observer. Used to visualize e.g. time series.
        armarx::DebugObserverInterfacePrx debugObserver;
        /// Debug drawer. Used for 3D visualization.
        armarx::DebugDrawerTopic debugDrawer;


        std::mutex shapesHistoryMutex;
        armarx::History<semrel::ShapeList> shapesHistory { 1000 };

        std::mutex supportGraphHistoryMutex;
        armarx::History<semrel::SupportGraph> supportGraphHistory { 1000 };

        armarx::SimplePeriodicTask<>::pointer_type reportTask;


    };
}
