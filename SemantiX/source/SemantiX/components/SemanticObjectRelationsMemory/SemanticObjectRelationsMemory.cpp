/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SemantiX::ArmarXObjects::SemanticObjectRelationsMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SemanticObjectRelationsMemory.h"

#include <SemanticObjectRelations/Visualization/Visualizer.h>
#include <SemanticObjectRelations/Visualization/SupportAnalysisVisualizer.h>

#include <SemantiX/libraries/SemantiXHooks/SemantiXHooks.h>
#include <SemantiX/libraries/SemantiXVariants/ShapeVariants.h>
#include <SemantiX/libraries/SemantiXVariants/SupportGraphVariant.h>


namespace semantix
{
    SemanticObjectRelationsMemoryPropertyDefinitions::SemanticObjectRelationsMemoryPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
        defineOptionalProperty<std::string>("ice.DebugObserverName", "DebugObserver",
                                            "Name of the topic the DebugObserver listens on");

        defineOptionalProperty<std::string>("ice.ShapeTopicName", "ShapeUpdates",
                                            "Topic where shapes are published.");
        defineOptionalProperty<std::string>("ice.SupportGraphTopicName", "SupportGraphUpdates",
                                            "Topic where support graphs are be published.");


        defineOptionalProperty<bool>("report.VisualizeShapes", false,
                                     "Enable/disable visualization.");
        defineOptionalProperty<bool>("report.VisualizeSupportGraph", false,
                                     "Enable/disable visualization.");
        defineOptionalProperty<int>("report.PeriodicTime", 250,
                                    "Periodic time of visualization in ms.");
    }


    std::string SemanticObjectRelationsMemory::getDefaultName() const
    {
        return "SemanticObjectRelationsMemory";
    }


    void SemanticObjectRelationsMemory::onInitComponent()
    {
        offeringTopicFromProperty("ice.DebugObserverName");
        debugDrawer.offeringTopic(*this);  // Calls this->offeringTopic().
        usingTopicFromProperty("ice.ShapeTopicName");
        usingTopicFromProperty("ice.SupportGraphTopicName");
    }


    void SemanticObjectRelationsMemory::onConnectComponent()
    {
        getTopicFromProperty(debugObserver, "ice.DebugObserverName");
        debugDrawer.getTopic(*this);  // Calls this->getTopic().

        armarx::ArmarXVisualizer::setAsImplementation(debugDrawer);
        armarx::ArmarXVisualizer::setMinimumVisuLevel(semrel::VisuLevel::USER);

        const int reportPeriodicTime = getProperty<int>("report.PeriodicTime");
        reportTask = new armarx::SimplePeriodicTask<>([this]()
        {
            reportLatest();
        }, reportPeriodicTime);
        reportTask->start();
    }


    void SemanticObjectRelationsMemory::onDisconnectComponent()
    {
    }


    void SemanticObjectRelationsMemory::onExitComponent()
    {
    }



    void SemanticObjectRelationsMemory::reportShapes(const TimedShapeBaseList& shapes, const Ice::Current&)
    {
        std::scoped_lock lock(shapesHistoryMutex);
        shapesHistory.insert(IceUtil::Time::microSeconds(shapes.timestampMicroseconds),
                             defrost(shapes.shapes));
    }

    void SemanticObjectRelationsMemory::reportSupportGraph(const TimedSupportGraphBase& supportGraph, const Ice::Current&)
    {
        std::scoped_lock lock(supportGraphHistoryMutex);

        const IceUtil::Time time = IceUtil::Time::microSeconds(supportGraph.timestampMicroseconds);
        supportGraphHistory.insert(time, armarx::defrost(supportGraph.supportGraph));
    }

    TimedShapeBaseList SemanticObjectRelationsMemory::getLatestShapes(const Ice::Current&)
    {
        std::scoped_lock lock(shapesHistoryMutex);
        const auto& latest = shapesHistory.getLatest();
        return { latest.time.toMicroSeconds(), armarx::frost(latest.value) };
    }

    TimedSupportGraphBase SemanticObjectRelationsMemory::getLatestSupportGraph(const Ice::Current&)
    {
        std::scoped_lock lock(supportGraphHistoryMutex);
        const auto& latest = supportGraphHistory.getLatest();
        return { latest.time.toMicroSeconds(), armarx::frost(latest.value) };
    }


    void SemanticObjectRelationsMemory::reportLatest()
    {
        armarx::StringVariantBaseMap debugChannel;

        {
            std::scoped_lock lock(shapesHistoryMutex);
            debugChannel["Shape History Size"] = new armarx::Variant(static_cast<int>(shapesHistory.size()));
            if (!shapesHistory.empty())
            {
                const auto& [time, shapes] = shapesHistory.getLatest();
                debugChannel["Number of Shapes"] = new armarx::Variant(static_cast<int>(shapes.size()));
                debugChannel["Latest Shape Time [us]"] = new armarx::Variant(static_cast<int>(time.toMicroSeconds()));
                debugChannel["Latest Shape Date Time"] = new armarx::Variant(time.toDateTime());

                if (getProperty<bool>("report.VisualizeShapes"))
                {
                    semrel::Visualizer visualizer("Shapes");
                    visualizer.drawShapeList(semrel::VisuLevel::USER, shapes, .5);
                }
            }
        }
        {
            std::scoped_lock lock(supportGraphHistoryMutex);
            debugChannel["Support Graph History Size"] = new armarx::Variant(static_cast<int>(supportGraphHistory.size()));
            if (!supportGraphHistory.empty())
            {
                auto latest = supportGraphHistory.getLatest();
                auto& supportGraph = latest.value;

                debugChannel["Number of Nodes"] = new armarx::Variant(static_cast<int>(supportGraph.numVertices()));
                debugChannel["Number of Edges"] = new armarx::Variant(static_cast<int>(supportGraph.numEdges()));
                debugChannel["Latest Support Graph Time [us]"] = new armarx::Variant(static_cast<int>(latest.time.toMicroSeconds()));
                debugChannel["Latest Support Graph Date Time"] = new armarx::Variant(latest.time.toDateTime());

                if (getProperty<bool>("report.VisualizeSupportGraph"))
                {
                    std::scoped_lock lock(shapesHistoryMutex);
                    if (!shapesHistory.empty())
                    {
                        semrel::SupportAnalysisVisualizer visualizer;
                        visualizer.setLayer("Support Graph");

                        auto& shapes = shapesHistory.getLatestValue();
                        visualizer.drawSupportGraph(
                            semrel::VisuLevel::USER, "support_graph",
                            supportGraph, toShapeMap(shapes), {0, 0, 0, 1}, {0, 0, 1, 1}, {.5, 0, 0, 1});
                    }
                }
            }
        }

        debugObserver->setDebugChannel(getName(), debugChannel);
    }


    armarx::PropertyDefinitionsPtr SemanticObjectRelationsMemory::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new SemanticObjectRelationsMemoryPropertyDefinitions(
                getConfigIdentifier()));
    }

}
