/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SemantiX::ArmarXObjects::SupportGraphExtractor
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SupportGraphExtractor.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <SemantiX/libraries/SemantiXHooks/SemantiXHooks.h>
#include <SemantiX/libraries/SemantiXVariants/ShapeVariants.h>
#include <SemantiX/libraries/SemantiXVariants/SupportGraphVariant.h>

#include <SemanticObjectRelations/SupportAnalysis/SupportAnalysisParameters.h>


namespace semantix
{
    SupportGraphExtractorPropertyDefinitions::SupportGraphExtractorPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
        defineOptionalProperty<std::string>("ice.DebugObserverName", "DebugObserver", "Name of the topic the DebugObserver listens on");
        defineOptionalProperty<std::string>("ice.ShapesTopicName", "Shapes", "Topic where shapes are published.");
        defineOptionalProperty<std::string>("ice.SupportGraphTopicName", "SupportGraphUpdates", "Topic where support graphs shall be published.");


        defineOptionalProperty<float>(
            "SA.GR.ContactMargin", 10.,
            "Distance by which objects are increased for contact computation [mm].")
        .setMin(0.);

        defineOptionalProperty<float>(
            "SA.GR.VerticalSepPlaneAngleMax", 10.,
            "Maximal angle [degree] between gravity and separating plane for \n"
            "separating plane to be considered vertical.")
        .setMin(0.);

        defineOptionalProperty<bool>(
            "SA.GR.VerticalSepPlaneAssumeSupport", false,
            "If true, edges are added if the separating plane is vertical.");

        defineOptionalProperty<bool>(
            "SA.UD.Enabled", true,
            "Enable or disble uncertainty detection (UD).");

        defineOptionalProperty<float>(
            "SA.UD.SupportAreaRatioMin", 0.7f,
            "Minimal support area ratio of an object to consider it safe.")
        .setMin(0.).setMax(1.);

    }


    std::string SupportGraphExtractor::getDefaultName() const
    {
        return "SupportGraphExtractor";
    }


    void SupportGraphExtractor::onInitComponent()
    {
        // Register offered topices and used proxies here.
        offeringTopicFromProperty("ice.DebugObserverName");
        debugDrawer.offeringTopic(*this);

        usingTopicFromProperty("ice.ShapesTopicName");
        offeringTopicFromProperty("ice.SupportGraphTopicName");

        semrel::SupportAnalysisParameters params(supportAnalysis);

        getProperty(params.contactMargin, "SA.GR.ContactMargin");
        getProperty(params.vertSepPlaneAngleMax, "SA.GR.VerticalSepPlaneAngleMax");
        getProperty(params.vertSepPlaneAssumeSupport, "SA.GR.VerticalSepPlaneAssumeSupport");
        getProperty(params.uncertaintyDetectionEnabled, "SA.UD.Enabled");
        getProperty(params.supportAreaRatioMin, "SA.UD.SupportAreaRatioMin");

        params.writeTo(supportAnalysis);
    }


    void SupportGraphExtractor::onConnectComponent()
    {
        // Get topics and proxies here. Pass the *InterfacePrx type as template argument.
        getTopicFromProperty(debugObserver, "ice.DebugObserverName");
        debugDrawer.getTopic(*this);

        getTopicFromProperty(supportGraphListener, "ice.SupportGraphTopicName");
    }



    void SupportGraphExtractor::onDisconnectComponent()
    {
    }


    void SupportGraphExtractor::onExitComponent()
    {
    }


    void SupportGraphExtractor::reportShapes(const TimedShapeBaseList& shapes, const Ice::Current&)
    {
        const IceUtil::Time time = IceUtil::Time::microSeconds(shapes.timestampMicroseconds);
        const semrel::ShapeList shapeList = armarx::defrost(shapes.shapes);
        const semrel::ShapeMap shapeMap = semrel::toShapeMap(shapeList);

        // Find table.
        const semrel::ShapeID tableID = findTable(shapeMap);


        supportAnalysis.setGravityVector(gravity);
        const semrel::SupportGraph supportGraph = supportAnalysis.performSupportAnalysis(
                    shapeMap, {tableID});

        supportGraphListener->reportSupportGraph({ time.toMicroSeconds(), armarx::frost(supportGraph) });
    }


    semrel::ShapeID SupportGraphExtractor::findTable(const semrel::ShapeMap& shapes)
    {
        float minHeight = std::numeric_limits<float>::max();
        semrel::ShapeID minID(-1);

        if (shapes.empty())
        {
            return minID;
        }

        for (const auto& [id, shape] : shapes)
        {
            float height = (-gravity).dot(shape->getPosition());
            if (height < minHeight)
            {
                minHeight = height;
                minID = shape->getID();
            }
        }

        return minID;
    }


    armarx::PropertyDefinitionsPtr SupportGraphExtractor::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new SupportGraphExtractorPropertyDefinitions(
                getConfigIdentifier()));
    }
}
