/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SemantiX::ArmarXObjects::ShapeExtractor
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <SemanticObjectRelations/ShapeExtraction/ShapeExtraction.h>

#include <ArmarXCore/core/Component.h>
#include <RobotAPI/libraries/core/visualization/DebugDrawerTopic.h>
#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>

#include <SemantiX/interface/ShapeExtractor.h>
#include <SemantiX/libraries/SemantiXVariants/ShapeVariants.h>



namespace armarx
{
    /// @class ShapeExtractorPropertyDefinitions
    class ShapeExtractorPropertyDefinitions :
        public visionx::PointCloudProcessorPropertyDefinitions
    {
    public:
        ShapeExtractorPropertyDefinitions(std::string prefix);
    };


    /**
     * @defgroup Component-ShapeExtractor ShapeExtractor
     * @ingroup SemantiX-Components
     * A description of the component ShapeExtractor.
     *
     * @class ShapeExtractor
     * @ingroup Component-ShapeExtractor
     * @brief Brief description of class ShapeExtractor.
     *
     * Detailed description of class ShapeExtractor.
     */
    class ShapeExtractor :
        virtual public ShapeExtractorInterface,
        virtual public visionx::PointCloudProcessor
    {

    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


        // ShapeExtractorInterface interface
        ShapeBaseListPtr extractShapes(const Ice::Current&) override;
        ShapeBaseListPtr getExtractedShapes(const Ice::Current&) override;


    protected:

        void onInitPointCloudProcessor() override;
        void onConnectPointCloudProcessor() override;
        void onExitPointCloudProcessor() override;

        void process() override;


        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:


        // INPUT

        /// The latest received point cloud.
        pcl::PointCloud<pcl::PointXYZRGBL>::Ptr inputPointCloud;
        std::mutex inputPointCloudMutex;


        // PROCESSING

        /// Locked if process is running.
        std::mutex extractionMutex;

        /// The shape extraction.
        semrel::ShapeExtraction shapeExtraction;


        // RESULT

        /**
         * @brief The result of the latest processed point cloud.
         *
         * Protected by latestObjectHypothesesMutex.
         * Use getObjectHypotheses() and setObjectHyspotheses() for access.
         */
        semrel::ShapeList extractedShapes;
        /// Serialized version of latest object hypotheses. Is kept up to date.
        ShapeVariantListPtr extractedShapesVariant = { new ShapeVariantList() };

        /// Protects extractedShapes and extractedShapesVariant.
        std::mutex extractedShapesMutex;



        // ICE

        // INPUT / OUTPUT

        /// The debug drawer proxy.
        DebugDrawerTopic debugDrawer;

        /// The shape listener proxy.
        ShapeExtractorListenerPrx shapeListener;


        // OPTIONS

        /// If true, enable life visualization.
        bool enableLiveVisualization = false;

    };
}
