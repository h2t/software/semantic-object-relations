/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SemantiX::ArmarXObjects::ShapeExtractor
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ShapeExtractor.h"

#include <SemanticObjectRelations/Hooks/Log.h>
#include <SemanticObjectRelations/Hooks/VisualizerInterface.h>

#include <SemantiX/libraries/SemanticObjectRelationsHooks/ArmarXLog.h>
#include <SemantiX/libraries/SemanticObjectRelationsHooks/ArmarXVisualizer.h>


using namespace armarx;


ShapeExtractorPropertyDefinitions::ShapeExtractorPropertyDefinitions(std::string prefix) :
    visionx::PointCloudProcessorPropertyDefinitions(prefix)
{
    // TOPICS

    defineOptionalProperty<std::string>(
        "ExtractedShapesTopicName", "ExtractedShapesUpdates",
        "Name of the topic where extracted shapes are published.");


    // EXTRACTION

    defineOptionalProperty<std::string>(
        "TargetShapes", "box, cylinder, sphere",
        "Shapes to be extracted (any combination of: box, cylinder, sphere).");

    defineOptionalProperty<float>(
        "ransac.maxModelError", 1000.,
        "Discard a model if its error is above this threshold.")
    .setMin(0.);

    defineOptionalProperty<int>(
        "ransac.maxIterations", 100,
        "Maximal number of iterations of each RANSAC run.")
    .setMin(0);

    defineOptionalProperty<float>(
        "ransac.distanceThresholdBox", 5.,
        "A point with a distance below this threshold is considererd an inlier. (Box)")
    .setMin(0.);
    defineOptionalProperty<float>(
        "ransac.distanceThresholdPCL", 0.5,
        "A point with a distance below this threshold is considererd an inlier. "
        "(PCL, i.e. cylinder and sphere)")
    .setMin(0.);
    defineOptionalProperty<float>(
        "ransac.outlierRate", 0.0125f,
        "The percentage of points that may be considered as outliers and may be ignored for fitting. "
        "This affects box extents, cylinder height, and error computation.")
    .setMin(0).setMax(1.);
    defineOptionalProperty<float>(
        "ransac.minInlierRate", 0.75,
        "A model is considered a good fit, if this fraction of all points are "
        "inliers for that model. (0.0 ... 1.0)")
    .setMin(0.).setMax(1.0);
    defineOptionalProperty<float>(
        "ransac.sizePenaltyFactor", 0.000125f,
        "Weight of the size penalty (relative to distance error). "
        "Size penalty inhibits shapes with huge radius (cyl, sph) or extents (box)."
        "Set to 0 to disable size penalty.")
    .setMin(0);
    defineOptionalProperty<float>(
        "ransac.sizePenaltyExponent", 2.0,
        "Exponent of the of the size penalty (as in s^x, with size measure s and exponent x). "
        "Size penalty inhibits shapes with huge radius (cyl, sph) or extents (box).")
    .setMin(0);


    // VISUALIZATION

    defineOptionalProperty<bool>(
        "EnableLiveVisualization", false,
        "True to enable visualization of the extraction process, false to disable.");
}



void ShapeExtractor::onInitPointCloudProcessor()
{
    getProperty(enableLiveVisualization, "EnableLiveVisualization");

    // published topics
    debugDrawer.offeringTopic(*this);
    offeringTopicFromProperty("ExtractedShapesTopicName");


    semrel::LogInterface::setImplementation(std::make_shared<ArmarXLog>(getName()));
    // set log level to debug, ArmarXLog will filter to component's log level
    semrel::LogInterface::setMinimumLogLevel(semrel::LogLevel::DEBUG);

    // initialize shapeExtraction with parameters
    shapeExtraction.setMaxModelError(getProperty<float>("ransac.maxModelError"));
    shapeExtraction.setMaxIterations(getProperty<int>("ransac.maxIterations"));

    shapeExtraction.setDistanceThresholdBox(getProperty<float>("ransac.distanceThresholdBox"));
    shapeExtraction.setDistanceThresholdPCL(getProperty<float>("ransac.distanceThresholdPCL"));

    shapeExtraction.setOutlierRate(getProperty<float>("ransac.outlierRate"));

    shapeExtraction.setMinInlierRate(getProperty<float>("ransac.minInlierRate"));

    shapeExtraction.setSizePenaltyFactor(getProperty<float>("ransac.sizePenaltyFactor"));
    shapeExtraction.setSizePenaltyExponent(getProperty<float>("ransac.sizePenaltyExponent"));

    // target shapes

    std::string targetShapes = getProperty<std::string>("TargetShapes");

    // make targetShapes all lowercase
    std::transform(targetShapes.begin(), targetShapes.end(), targetShapes.begin(), ::tolower);

    // find != npos <=> string contained
    shapeExtraction.setBoxExtractionEnabled(targetShapes.find("box") != std::string::npos);
    shapeExtraction.setCylinderExtractionEnabled(targetShapes.find("cylinder") != std::string::npos);
    shapeExtraction.setSphereExtractionEnabled(targetShapes.find("sphere") != std::string::npos);

}

void ShapeExtractor::onConnectPointCloudProcessor()
{
    // Topic proxies
    debugDrawer.getTopic(*this);
    getTopicFromProperty(shapeListener, "ExtractedShapesTopicName");

    // set visualizer hook
    semrel::VisualizerInterface::setImplementation(std::make_shared<ArmarXVisualizer>(debugDrawer));

    semrel::VisuLevel visuLevel = enableLiveVisualization
                                  ? semrel::VisuLevel::LIVE_VISU
                                  : semrel::VisuLevel::RESULT;

    semrel::VisualizerInterface::setMinimumVisuLevel(visuLevel);
}


void ShapeExtractor::onExitPointCloudProcessor()
{

}

void ShapeExtractor::process()
{
    if (!waitForPointClouds(10000))
    {
        ARMARX_WARNING << "Timeout or error in wait for pointclouds";
        return;
    }

    // Retrieve pointcloud

    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr receivedPointCloud(new pcl::PointCloud<pcl::PointXYZRGBL>());
    if (!getPointClouds(receivedPointCloud))
    {
        ARMARX_WARNING << "Unable to get point cloud data.";
        return;
    }

    // Store new point cloud
    {
        std::lock_guard<std::mutex> lock(inputPointCloudMutex);
        inputPointCloud = receivedPointCloud;
        ARMARX_INFO << deactivateSpam(10)
                    << "Received point cloud (" << receivedPointCloud->size() << " points).";
    }
}

std::string ShapeExtractor::getDefaultName() const
{
    return "ShapeExtractor";
}

ShapeBaseListPtr ShapeExtractor::extractShapes(const Ice::Current&)
{
    if (!inputPointCloud)
    {
        ARMARX_WARNING << "No point cloud received. Aborting shape extraction.";
        return new ShapeVariantList();
    }


    if (!extractionMutex.try_lock())
    {
        ARMARX_WARNING << "A shape extraction is already running. Aborting.";
        return new ShapeVariantList();
    }

    std::lock_guard<std::mutex> lockInputPointCloud(inputPointCloudMutex);

    ARMARX_INFO << "Running shape extraction (" << inputPointCloud->size() << " points) ...";

    semrel::ShapeExtractionResult extractionResult = shapeExtraction.extract(*inputPointCloud);

    // increase table
    //increaseTableSize(shapes);

    // store the result
    {
        std::lock_guard<std::mutex> lock(extractedShapesMutex);

        extractedShapes = std::move(extractionResult.objects);
        extractedShapesVariant = new ShapeVariantList(extractedShapes);

        ARMARX_INFO << "Extracted " << extractedShapes.size() << " shapes. \n"
                    << extractedShapes;

        ARMARX_VERBOSE << "Publishing shape collection ... ";
        shapeListener->reportExtractedShapes(extractedShapesVariant);

        // unlock extraction mutex
        extractionMutex.unlock();

        return extractedShapesVariant;
    }
}

ShapeBaseListPtr ShapeExtractor::getExtractedShapes(const Ice::Current&)
{
    std::lock_guard<std::mutex> lock(extractedShapesMutex);
    return extractedShapesVariant;
}


armarx::PropertyDefinitionsPtr ShapeExtractor::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new ShapeExtractorPropertyDefinitions(getConfigIdentifier()));
}
