#pragma once

#include <map>

#include <Eigen/Core>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/PointIndices.h>

#include <SemanticObjectRelations/Shapes/shape_containers.h>


namespace semrel
{
    /// Return the AABBs of each point cloud segment in a ShapeList.
    ShapeList getShapesFromAABBs(const pcl::PointCloud<pcl::PointXYZL>& pointCloud);
    /// Return the AABBs of each point cloud segment in a ShapeList.
    ShapeList getShapesFromAABBs(const pcl::PointCloud<pcl::PointXYZRGBL>& pointCloud);
    /// Return the given AABBs as a ShapeList.
    ShapeList getShapesFromAABBs(const std::map<uint32_t, Eigen::Matrix32f>& segmentAABBs);


    Eigen::Matrix<float, 3, 2>
    getSoftAABB(const pcl::PointCloud<pcl::PointXYZRGBL>& pointCloud,
                const pcl::PointIndices& segmentIndices,
                float outlierRatio);

    std::map<uint32_t, Eigen::Matrix<float, 3, 2>>
            getSoftAABBs(const pcl::PointCloud<pcl::PointXYZRGBL>& pointCloud,
                         const std::map<uint32_t, pcl::PointIndices>& segmentIndices,
                         float outlierRatio);


    /// Return the AABBs of each point cloud segment in a ShapeList.
    ShapeList getShapesFromSoftAABBs(
        const pcl::PointCloud<pcl::PointXYZRGBL>& pointCloud, float outlierRatio);

}
