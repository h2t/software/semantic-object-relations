
#include <SemanticObjectRelations/Shapes/json.h>

#include <iostream>

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

#include "../../ShapeListFixture.h"
#include "tools.h"


using namespace semrel;


BOOST_AUTO_TEST_SUITE(Shape_Json_Basic)


BOOST_AUTO_TEST_CASE(test_ShapeSerializers_getAcceptedTypes)
{
    BOOST_CHECK(!json::ShapeSerializers::getRegisteredTypes().empty());
}

BOOST_AUTO_TEST_CASE(test_to_json_from_json_Box)
{
    test::test_to_json_from_json<Box>();
}
BOOST_AUTO_TEST_CASE(test_to_json_from_json_Cylinder)
{
    test::test_to_json_from_json<Cylinder>();
}
BOOST_AUTO_TEST_CASE(test_to_json_from_json_Sphere)
{
    test::test_to_json_from_json<Sphere>();
}


BOOST_AUTO_TEST_CASE(test_to_json_from_json_ShapePtr_Box)
{
    test::test_to_json_from_json_pointer<Box>();
}
BOOST_AUTO_TEST_CASE(test_to_json_from_json_ShapePtr_Cylinder)
{
    test::test_to_json_from_json_pointer<Cylinder>();
}
BOOST_AUTO_TEST_CASE(test_to_json_from_json_ShapePtr_Sphere)
{
    test::test_to_json_from_json_pointer<Sphere>();
}


void check_throws_NoTypeNameEntryInJsonObject(const nlohmann::json& j)
{
    BOOST_TEST_CONTEXT(j.dump(2))
    {
        Box box;
        Cylinder cyl;
        Sphere sph;
        BOOST_CHECK_THROW(from_json(j, static_cast<Shape&>(box)), error::NoTypeNameEntryInJsonObject);
        BOOST_CHECK_THROW(from_json(j, static_cast<Shape&>(cyl)), error::NoTypeNameEntryInJsonObject);
        BOOST_CHECK_THROW(from_json(j, static_cast<Shape&>(sph)), error::NoTypeNameEntryInJsonObject);
    }
}
BOOST_AUTO_TEST_CASE(test_throws_NoTypeNameEntryInJsonObject_on_json_null)
{
    check_throws_NoTypeNameEntryInJsonObject(nlohmann::json());
}
BOOST_AUTO_TEST_CASE(test_throws_NoTypeNameEntryInJsonObject_on_json_array)
{
    check_throws_NoTypeNameEntryInJsonObject(nlohmann::json::array());
}
BOOST_AUTO_TEST_CASE(test_throws_NoTypeNameEntryInJsonObject_on_json_empty_object)
{
    check_throws_NoTypeNameEntryInJsonObject(nlohmann::json::object());
}


BOOST_AUTO_TEST_CASE(test_type_name_mismatch)
{
    const nlohmann::json j = static_cast<const Shape&>(test::makeShape<Box>(55));
    BOOST_TEST_CONTEXT(j.dump(2))
    {
        Box box;
        Cylinder cyl;
        Sphere sph;

        BOOST_CHECK_NO_THROW(from_json(j, static_cast<Shape&>(box)));
        BOOST_CHECK_THROW(from_json(j, static_cast<Shape&>(cyl)), error::TypeNameMismatch);
        BOOST_CHECK_THROW(from_json(j, static_cast<Shape&>(sph)), error::TypeNameMismatch);
    }
}


BOOST_AUTO_TEST_CASE(test_serializer_already_registered)
{
    BOOST_CHECK_THROW(json::ShapeSerializers::registerSerializer<Box>(semrel::to_json, semrel::from_json),
                      error::SerializerAlreadyRegisteredForType);
    BOOST_CHECK_THROW(json::ShapeSerializers::registerSerializer<Cylinder>(semrel::to_json, semrel::from_json),
                      error::SerializerAlreadyRegisteredForType);
    BOOST_CHECK_THROW(json::ShapeSerializers::registerSerializer<Sphere>(semrel::to_json, semrel::from_json),
                      error::SerializerAlreadyRegisteredForType);
}


BOOST_AUTO_TEST_SUITE_END()
