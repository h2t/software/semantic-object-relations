#include "RegisteredShape.h"

namespace semrel
{


    template <int Ov>
    void to_json_tmpl(nlohmann::json& j, const test::CustomShape<Ov>& rhs)
    {
        json::to_json_base(j, rhs);
        j["customData"] = rhs.customData;
    }

    template <int Ov>
    void from_json_tmpl(const nlohmann::json& j, test::CustomShape<Ov>& rhs)
    {
        json::from_json_base(j, rhs);
        rhs.customData = j.at("customData");
    }

    template <int Ov>
    void check_equal_derived_tmpl(const test::CustomShape<Ov>& lhs, const test::CustomShape<Ov>& rhs)
    {
        BOOST_CHECK_EQUAL(lhs.customData, rhs.customData);
    }

    void test::to_json(nlohmann::json& j, const UnregisteredShape& rhs)
    {
        to_json_tmpl(j, rhs);
    }

    void test::from_json(const nlohmann::json& j, UnregisteredShape& rhs)
    {
        from_json_tmpl(j, rhs);
    }

    void test::to_json(nlohmann::json& j, const MemberRegisteredShape& rhs)
    {
        to_json_tmpl(j, rhs);
    }

    void test::from_json(const nlohmann::json& j, test::MemberRegisteredShape& rhs)
    {
        from_json_tmpl(j, rhs);
    }

    template <> const int test::MemberRegisteredShape::_JSON_REGISTRATION = []()
    {
        json::ShapeSerializers::registerSerializer<MemberRegisteredShape>(to_json, from_json);
        return 0;
    }();


    void test::to_json(nlohmann::json& j, const test::FreeRegisteredShape& rhs)
    {
        to_json_tmpl(j, rhs);
    }

    void test::from_json(const nlohmann::json& j, test::FreeRegisteredShape& rhs)
    {
        from_json_tmpl(j, rhs);
    }


    void test::check_equal_derived(const test::UnregisteredShape& lhs, const test::UnregisteredShape& rhs)
    {
        check_equal_derived_tmpl(lhs, rhs);
    }

    void test::check_equal_derived(const test::MemberRegisteredShape& lhs, const test::MemberRegisteredShape& rhs)
    {
        check_equal_derived_tmpl(lhs, rhs);
    }

    void test::check_equal_derived(const test::FreeRegisteredShape& lhs, const test::FreeRegisteredShape& rhs)
    {
        check_equal_derived_tmpl(lhs, rhs);
    }

    const int test::_FREE_REGISTERED_SHAPE_REGISTRATION = []()
    {
        static int call_count = 0;
        call_count++;
        // throw std::runtime_error("Call count: " + std::to_string(call_count));
        json::ShapeSerializers::registerSerializer<FreeRegisteredShape>(to_json, from_json);
        return 0;
    }();

}
