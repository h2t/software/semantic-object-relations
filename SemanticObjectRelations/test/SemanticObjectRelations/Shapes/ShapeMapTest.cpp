
#include <SemanticObjectRelations/Shapes.h>

#include <iostream>

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

#include "../ShapeListFixture.h"

using namespace semrel;


BOOST_AUTO_TEST_SUITE(ShapeMapTest)


template <class PointerT>
void checkIntegrity(const std::map<semrel::ShapeID, PointerT>& shapeMap)
{
    // Check shapeMap's integrity.
    for (const auto& [id, shape] : shapeMap)
    {
        BOOST_CHECK(shape);
        BOOST_CHECK_EQUAL(id, shape->getID());
    }
}


BOOST_AUTO_TEST_CASE(test_toShapeMap)
{
    const ShapeList shapeList = test::makeShapeList(5);
    const ShapeMap shapeMap = semrel::toShapeMap(shapeList);

    BOOST_CHECK_EQUAL(shapeMap.size(), shapeList.size());
    checkIntegrity(shapeMap);

    // Check consistency with shapeList.
    for (const auto& shape : shapeList)
    {
        BOOST_CHECK_EQUAL(shapeMap.count(shape->getID()), 1);
        BOOST_CHECK_EQUAL(shapeMap.at(shape->getID()), shape.get());
    }
}


BOOST_AUTO_TEST_CASE(test_toShapeMap_with_ShapeList_containing_nullptr)
{
    ShapeList shapeList = test::makeShapeList(5);
    shapeList.at(2).reset(nullptr);

    BOOST_CHECK_THROW(semrel::toShapeMap(shapeList), semrel::error::ShapePointerIsNull);
}


BOOST_AUTO_TEST_SUITE_END()
