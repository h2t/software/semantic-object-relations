
#include <SemanticObjectRelations/Shapes.h>

#include <iostream>

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

#include "../ShapeListFixture.h"

using namespace semrel;


BOOST_AUTO_TEST_SUITE(ShapeListTest)


// After refactoring, there is not much to test for ShapeList.
// We keep this test file in case tests come up in the future.

BOOST_AUTO_TEST_CASE(test_emplace_back)
{
    ShapeList shapes;

    long size = 0;
    BOOST_CHECK_EQUAL(shapes.size(), size);

    shapes.emplace_back(new Box(ShapeID(++size)));
    BOOST_CHECK_EQUAL(shapes.size(), size);
    Box* box = dynamic_cast<Box*>(shapes.back().get());
    BOOST_CHECK(box);

    shapes.emplace_back(new Cylinder(ShapeID(++size)));
    BOOST_CHECK_EQUAL(shapes.size(), size);
    Cylinder* cylinder = dynamic_cast<Cylinder*>(shapes.back().get());
    BOOST_CHECK(cylinder);

    shapes.emplace_back(new Sphere(ShapeID(++size)));
    BOOST_CHECK_EQUAL(shapes.size(), size);
    Sphere* sphere = dynamic_cast<Sphere*>(shapes.back().get());
    BOOST_CHECK(sphere);

    shapes.emplace_back(new MeshShape(ShapeID(++size)));
    BOOST_CHECK_EQUAL(shapes.size(), size);
    MeshShape* meshShape = dynamic_cast<MeshShape*>(shapes.back().get());
    BOOST_CHECK(meshShape);
}


BOOST_AUTO_TEST_CASE(test_test_makeShapeList)
{
    size_t size = 5;
    long maxID = 10;
    const ShapeList shapes = test::makeShapeList(size, maxID);

    BOOST_CHECK_EQUAL(shapes.size(), size);
    for (const auto& shape : shapes)
    {
        BOOST_CHECK(shape);
        BOOST_CHECK_LE(ShapeID(0), shape->getID());
        BOOST_CHECK_LE(shape->getID(), ShapeID(maxID));
    }
}

BOOST_AUTO_TEST_SUITE_END()


using test::ShapeListFixture;

BOOST_FIXTURE_TEST_SUITE(ShapeListFixtureTest, ShapeListFixture)


BOOST_AUTO_TEST_CASE(test_ShapeListFixture)
{
    for (const auto& shape : shapeList)
    {
        BOOST_CHECK(shape);
        BOOST_CHECK_LE(ShapeID(0), shape->getID());
    }
}


BOOST_AUTO_TEST_SUITE_END()
