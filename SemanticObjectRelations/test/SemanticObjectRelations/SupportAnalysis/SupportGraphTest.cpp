
#include <SemanticObjectRelations/Shapes.h>
#include <SemanticObjectRelations/SupportAnalysis/SupportGraph.h>

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

#include "../ShapeListFixture.h"

using namespace semrel;


using test::ShapeListFixture;

BOOST_FIXTURE_TEST_SUITE(SupportGraphTest, ShapeListFixture)


BOOST_AUTO_TEST_CASE(test_SupportGraph_construction_from_ShapeList)
{
    SupportGraph graph(shapeList);
}

BOOST_AUTO_TEST_CASE(test_SupportGraph_graphviz_writer)
{
    SupportGraph graph(shapeList);

    SupportEdge se;
    graph.addEdge(graph.vertex(0), graph.vertex(1), se);

    se.fromUncertaintyDetection = true;
    graph.addEdge(graph.vertex(0), graph.vertex(2), se);

    se.verticalSeparatingPlane = true;
    graph.addEdge(graph.vertex(0), graph.vertex(3), se);

    se.fromUncertaintyDetection = false;
    graph.addEdge(graph.vertex(0), graph.vertex(4), se);

    BOOST_TEST_MESSAGE("Visualizing graph: " << graph.str());

    SupportGraph::getGraphvizWriter().write(graph, "support_graph");
    SupportGraph::getGraphvizWriter().write(graph, toShapeMap(shapeList), "support_graph");
}


BOOST_AUTO_TEST_CASE(test_addVertex)
{
    BOOST_REQUIRE_GE(shapeList.size(), 4);

    SupportGraph graph;
    BOOST_CHECK_EQUAL(graph.numVertices(), 0);

    graph.addVertex(*shapeList[1]);
    BOOST_CHECK_EQUAL(graph.numVertices(), 1);

    graph.addVertex(*shapeList[2].get());
    BOOST_CHECK_EQUAL(graph.numVertices(), 2);

    Shape& sh3 = *shapeList[3];
    graph.addVertex(sh3);
    BOOST_CHECK_EQUAL(graph.numVertices(), 3);
}

BOOST_AUTO_TEST_SUITE_END()


struct StringOutputFixture : public ShapeListFixture
{
    SupportGraph graph { shapeList };

    StringOutputFixture()
    {
        graph.addEdge(graph.vertex(0), graph.vertex(1));
        graph.addEdge(graph.vertex(0), graph.vertex(2));
        graph.addEdge(graph.vertex(3), graph.vertex(0));

        graph.edge(graph.vertex(0), graph.vertex(2)).attrib().verticalSeparatingPlane = true;
        graph.edge(graph.vertex(0), graph.vertex(2)).attrib().fromUncertaintyDetection = false;
    }

};


BOOST_FIXTURE_TEST_SUITE(SupportGraph_StringOutputTests, StringOutputFixture)


BOOST_AUTO_TEST_CASE(test_strVertex)
{
    for (auto v : graph.vertices())
    {
        const std::string str = graph.strVertex(v);
        BOOST_CHECK(!str.empty());
    }
}

BOOST_AUTO_TEST_CASE(test_strEdge)
{
    for (auto e : graph.edges())
    {
        const std::string str = graph.strEdge(e);
        BOOST_CHECK(!str.empty());
    }
}

BOOST_AUTO_TEST_CASE(test_str)
{
    const std::string str = graph.str();
    BOOST_CHECK(!str.empty());

    BOOST_TEST_MESSAGE(str);
}

BOOST_AUTO_TEST_CASE(test_stream_operator)
{
    std::stringstream ss;
    ss << graph;
    BOOST_CHECK_EQUAL(ss.str(), graph.str());
}


BOOST_AUTO_TEST_SUITE_END()
