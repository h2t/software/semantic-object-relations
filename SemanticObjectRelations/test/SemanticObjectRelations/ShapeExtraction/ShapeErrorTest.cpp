
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>

#include <SemanticObjectRelations/ShapeExtraction/ransac/ShapeErrorFunction.h>
#include <SemanticObjectRelations/Shapes.h>

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>


#define ENABLE_ERROR_VISUALIZATION


using namespace semrel;
using namespace Eigen;

using Point = pcl::PointXYZRGB;


// change to true to generate high resolution point clouds
// (may be inappropriate if tests are run regularly)
static bool highRes = false;

struct Color {
    Color(float r = 0, float g = 0, float b = 0) : r(r), g(g), b(b) {}
    float r, g, b;
};


static std::vector<float> errorKeys = { 0.0, 0.01f, 0.2f, 1.0 };
static std::vector<Color> errorColors = {
    Color(0, 0, 1), Color(0, 1, 0), Color(1, 1, 0), Color(1, 0, 0)
};

void colorCodeError(float error, Point& point, float maxError)
{

    // minError = 0
    float errorRel = std::min(0.99f, error / maxError);

    std::size_t colorIndex = 0;
    while (errorRel > errorKeys[colorIndex + 1])
    {
        colorIndex++;
    }
    BOOST_CHECK_LT(colorIndex, errorKeys.size());
    BOOST_CHECK_LT(colorIndex, errorColors.size());
    BOOST_CHECK_LE(errorRel, errorKeys[colorIndex + 1]);

    float t = (errorRel - errorKeys[colorIndex]) / (errorKeys[colorIndex + 1] - errorKeys[colorIndex]);

    BOOST_CHECK_LE(0, t);
    BOOST_CHECK_LE(t, 1.0);

    // interpolate between colors[colorIndex] and colors[colorIndex+1]
    Color& left = errorColors[colorIndex];
    Color& right = errorColors[colorIndex + 1];

    auto interpol = [&](float a, float b, float t)
    {
        return a + t * (b - a);
    };

    point.r = uint8_t(255 * interpol(left.r, right.r, t));
    point.g = uint8_t(255 * interpol(left.g, right.g, t));
    point.b = uint8_t(255 * interpol(left.b, right.b, t));
}


pcl::PointCloud<Point> createErrorRasterPointCloud(
        const Shape& shape, ShapeErrorFunction shapeError, float maxError,
        Vector3f bbMin, Vector3f bbMax, float resolution)
{
    std::vector<Vector3f> points;

    for (float x = bbMin(0); x < bbMax(0); x += resolution)
    {
        for (float y = bbMin(1); y < bbMax(1); y += resolution)
        {
            for (float z = bbMin(2); z < bbMax(2); z += resolution)
            {
                points.push_back(Vector3f(x, y, z));
            }
        }
    }

    std::vector<float> errors = shapeError(points, shape);

    BOOST_CHECK_EQUAL(points.size(), errors.size());

    pcl::PointCloud<Point> pointCloud;
    for (std::size_t i = 0; i < points.size(); ++i)
    {
        Eigen::Vector3f& point = points[i];
        float error = errors[i];

        if (error > maxError)
            continue;

        Point p;
        p.x = point(0);
        p.y = point(1);
        p.z = point(2);
        colorCodeError(error, p, maxError);
        pointCloud.push_back(p);
    }

    return pointCloud;
}

BOOST_AUTO_TEST_SUITE(ShapeErrorTest)

BOOST_AUTO_TEST_CASE(TestBoxError)
{
    Vector3f position(0, 0, 0);
    Matrix3f axes;
    axes.col(0) = Vector3f(1, 1, 1).normalized();
    axes.col(1) = Vector3f(-1, 1, 0).normalized();
    axes.col(2) = axes.col(0).cross(axes.col(1));
    Vector3f extents(2, 3, 4);

    Box shape(position, axes, extents);

    Vector3f bbMin = -4 * Vector3f(1, 1, 1);
    Vector3f bbMax =  4 * Vector3f(1, 1, 1);
    float resolution = highRes ? 0.25 : 1.0;

    float maxError = 15;

    pcl::PointCloud<Point> pointCloud = createErrorRasterPointCloud(
                shape, &BoxErrorFunction, maxError, bbMin, bbMax, resolution);

    pcl::io::savePCDFileASCII("box_error.pcd", pointCloud);
}



BOOST_AUTO_TEST_CASE(TestCylinderError)
{
    Vector3f position(0, 0, 0);
    Vector3f direction = Vector3f(1, 1, 0).normalized();
    float radius = 3;
    float height = 5;
    Cylinder shape(position, direction, radius, height);

    Vector3f bbMin = -4 * Vector3f(1, 1, 1);
    Vector3f bbMax =  4 * Vector3f(1, 1, 1);
    float resolution = highRes ? 0.25 : 1.0;

    float maxError = 15;

    pcl::PointCloud<Point> pointCloud = createErrorRasterPointCloud(
                shape, &CylinderErrorFunction, maxError, bbMin, bbMax, resolution);

    pcl::io::savePCDFileASCII("cylinder_error.pcd", pointCloud);
}



BOOST_AUTO_TEST_CASE(TestSphereError)
{
    Vector3f position(0, 0, 5);
    float radius = 5;
    Sphere sphere(position, radius);

    Vector3f bbMin = -5 * Vector3f(1, 1, 1);
    Vector3f bbMax =  5 * Vector3f(1, 1, 1);
    float resolution = highRes ? 0.25 : 1.0;

    float maxError = 5;

    pcl::PointCloud<Point> pointCloud = createErrorRasterPointCloud(
                sphere, &SphereErrorFunction, maxError, bbMin, bbMax, resolution);

    // raster sphere
    for (float x = -radius; x <= radius; x+=resolution/2)
    {
        for (float y = -radius; y <= +radius; y+=resolution/2)
        {
            float z = std::sqrt(radius*radius - x*x - y*y);
            Point p;
            p.x = x + position(0);
            p.y = y + position(1);
            p.z = z + position(2);
            p.r = uint8_t(0);
            p.g = uint8_t(0);
            p.b = uint8_t(255);
            pointCloud.push_back(p);

            p.z = -z + position(2);
            pointCloud.push_back(p);
        }
    }

    pcl::io::savePCDFileASCII("sphere_error.pcd", pointCloud);

}

BOOST_AUTO_TEST_SUITE_END()
