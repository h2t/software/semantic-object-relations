#pragma once

#include "ParameterSet.h"

#include <Eigen/Eigen>


class CommonParameterSet : public ParameterSet
{

public:
    
    static CommonParameterSet loadParameterSet(const std::string& filename);
    
    
    CommonParameterSet();
    
    virtual void setParameter(const std::string& name, const std::string& value) override;
    
    
    int getMaxIterations() const;
    void setMaxIterations(int value);
    
    float getDistanceThresholdBox() const;
    void setDistanceThresholdBox(float value);
    
    float getDistanceThresholdPCL() const;
    void setDistanceThresholdPCL(float value);
    
    float getMinInlierRate() const;
    void setMinInlierRate(float value);
    
    float getOutlierRate() const;
    void setOutlierRate(float value);
    
    float getSizePenaltyFactor() const;
    void setSizePenaltyFactor(float value);
    
    float getSizePenaltyExponent() const;
    void setSizePenaltyExponent(float value);
    
    bool getConcurrencyEnabled() const;
    void setConcurrencyEnabled(bool value);
    
    Eigen::Vector3f getGravity() const;
    void setGravity(const Eigen::Vector3f& value);
    void setGravityCoordinate(int dim, float value);
    
    float getContactMargin() const;
    void setContactMargin(float value);
    
    float getSupportAreaRatioMin() const;
    void setSupportAreaRatioMin(float value);
    
    
    friend std::ostream& operator<<(std::ostream& os, const CommonParameterSet& ps);
    
private:
    
    // Shape Extraction
    
    int maxIterations = 100;
    float distanceThresholdBox = 5.0;
    float distanceThresholdPCL = 5.0;
    
    float minInlierRate = 0.7f;
    float outlierRate = 0.00125f;
    float sizePenaltyFactor = 0.0;
    float sizePenaltyExponent = 2.0;
    bool concurrencyEnabled = true;
    
    
    // Support Analysis
    
    Eigen::Vector3f gravity { 0, 0, -1 };
    
    float contactMargin = 10.0;
    float supportAreaRatioMin = 0.7f;
    
};
