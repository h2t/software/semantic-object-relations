#include "CommonParameterSet.h"

CommonParameterSet CommonParameterSet::loadParameterSet(const std::string& filename)
{
    std::ifstream file(filename);
    checkFileOpened(file, filename);

    CommonParameterSet params;
    
    int lineNr = 0;
    std::string line;
    
    while (std::getline(file, line))
    {
        if (isIgnoredLine(line))
        {
            continue;
        }
        else
        {
            // parse assignment and set parameter
            std::string name;
            std::string value;
            try 
            {
                std::tie(name, value) = parseAssignmentLine(line);
                params.setParameter(name, value);
            } 
            catch (ParameterParsingError& e) 
            {
                e.setLineNr(lineNr);
                e.setLine(line);
                throw e;
            }
        }
        
        ++lineNr;
    }
    
    return params;
}

CommonParameterSet::CommonParameterSet() : ParameterSet ("CommonParams")
{
}

void CommonParameterSet::setParameter(const std::string& name, const std::string& value)
{
    if (name == "maxIterations")
    {
        maxIterations = parseInt(value);
    }
    else if(name == "distanceThresholdBox")
    {
        distanceThresholdBox = parseFloat(value);
    }
    else if (name == "distanceThresholdPCL") 
    {
        distanceThresholdPCL = parseFloat(value);
    }
    else if(name == "minInlierRate")
    {
        minInlierRate = parseFloat(value);
    }
    else if (name == "outlierRate") 
    {
        outlierRate = parseFloat(value);
    }
    else if (name == "sizePenaltyFactor") 
    {
        sizePenaltyFactor = parseFloat(value);
    }
    else if(name == "sizePenaltyExponent")
    {
        sizePenaltyExponent = parseFloat(value);
    }
    else if (name == "concurrencyEnabled") 
    {
        concurrencyEnabled = parseBool(value);
    }
    else if(name == "gravityX")
    {
        setGravityCoordinate(0, parseFloat(value));
    }
    else if (name == "gravityY") 
    {
        setGravityCoordinate(1, parseFloat(value));
    }
    else if(name == "gravityZ")
    {
        setGravityCoordinate(2, parseFloat(value));
    }
    else if (name == "contactMargin") 
    {
        contactMargin = parseFloat(value);
    }
    else if(name == "supportAreaRatioMin")
    {
        supportAreaRatioMin = parseFloat(value);
    }
    else
    {
        throw unknownParameterError(name);
    }
}

int CommonParameterSet::getMaxIterations() const
{
    return maxIterations;
}

void CommonParameterSet::setMaxIterations(int value)
{
    maxIterations = value;
}

float CommonParameterSet::getDistanceThresholdBox() const
{
    return distanceThresholdBox;
}

void CommonParameterSet::setDistanceThresholdBox(float value)
{
    distanceThresholdBox = value;
}

float CommonParameterSet::getDistanceThresholdPCL() const
{
    return distanceThresholdPCL;
}

void CommonParameterSet::setDistanceThresholdPCL(float value)
{
    distanceThresholdPCL = value;
}

float CommonParameterSet::getMinInlierRate() const
{
    return minInlierRate;
}

void CommonParameterSet::setMinInlierRate(float value)
{
    minInlierRate = value;
}

float CommonParameterSet::getOutlierRate() const
{
    return outlierRate;
}

void CommonParameterSet::setOutlierRate(float value)
{
    outlierRate = value;
}

float CommonParameterSet::getSizePenaltyFactor() const
{
    return sizePenaltyFactor;
}

void CommonParameterSet::setSizePenaltyFactor(float value)
{
    sizePenaltyFactor = value;
}

float CommonParameterSet::getSizePenaltyExponent() const
{
    return sizePenaltyExponent;
}

void CommonParameterSet::setSizePenaltyExponent(float value)
{
    sizePenaltyExponent = value;
}

bool CommonParameterSet::getConcurrencyEnabled() const
{
    return concurrencyEnabled;
}

void CommonParameterSet::setConcurrencyEnabled(bool value)
{
    concurrencyEnabled = value;
}

Eigen::Vector3f CommonParameterSet::getGravity() const
{
    return gravity;
}

void CommonParameterSet::setGravity(const Eigen::Vector3f& value)
{
    gravity = value;
}

void CommonParameterSet::setGravityCoordinate(int dim, float value)
{
    if (dim < 0 || dim >= 3)
    {
        std::stringstream ss;
        ss << "dim must be 0, 1 or 2, but was " << dim;
        throw std::invalid_argument(ss.str());
    }
    gravity(dim) = value;
}

float CommonParameterSet::getContactMargin() const
{
    return contactMargin;
}

void CommonParameterSet::setContactMargin(float value)
{
    contactMargin = value;
}

float CommonParameterSet::getSupportAreaRatioMin() const
{
    return supportAreaRatioMin;
}

void CommonParameterSet::setSupportAreaRatioMin(float value)
{
    supportAreaRatioMin = value;
}

std::ostream& operator<<(std::ostream& os, const CommonParameterSet& ps)
{
    Eigen::IOFormat iof(4, 0, " ", "", "", "", "[ ", " ]");
    
    std::string boolStr[2] = {"false", "true"};
    return os <<   "+-- Common parameters --"
              << "\n| maxIterations = " << ps.maxIterations
              << "\n| distanceThresholdBox = " << ps.distanceThresholdBox
              << "\n| distanceThresholdPCL = " << ps.distanceThresholdPCL

              << "\n| minInlierRate = " << ps.minInlierRate
              << "\n| outlierRate = " << ps.outlierRate
              << "\n| sizePenaltyFactor = " << ps.sizePenaltyFactor
              << "\n| sizePenaltyExponent = " << ps.sizePenaltyExponent
              << "\n| concurrencyEnabled = " << boolStr[ps.concurrencyEnabled]
    
              << "\n| gravity = " << ps.gravity.format(iof)
    
              << "\n| contactMargin = " << ps.contactMargin
              << "\n| supportAreaRatioMin = " << ps.supportAreaRatioMin
              << std::endl;
}
