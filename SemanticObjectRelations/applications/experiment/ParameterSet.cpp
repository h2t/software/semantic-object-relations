#include "ParameterSet.h"

#include <sstream>
#include <algorithm>




ParameterSet::ParameterSet(const std::string& name) : name(name)
{
}

ParameterSet::~ParameterSet()
{
    
}

bool ParameterSet::isIgnoredLine(const std::string& line)
{
    return line.empty() || line.front() == '#';
}

std::pair<std::string, std::string> ParameterSet::parseAssignmentLine(const std::string& line)
{
    std::size_t indexOfAssignemnt = line.find('=');
    if (indexOfAssignemnt == std::string::npos)
    {
        std::stringstream msg;
        msg << "Cannot parse parameter assignment ('=' not found): '" << line << "'";
        throw ParameterParsingError(msg.str());
    }
    
    std::string name = line.substr(0, indexOfAssignemnt);
    std::string value = line.substr(indexOfAssignemnt+1, line.size());
    
    trim(name);
    trim(value);
    
    return std::make_pair(name, value);
}

bool ParameterSet::parseBool(const std::string& trueOrFalse)
{
    if (trueOrFalse == "true" || trueOrFalse == "1")
    {
        return true;
    }
    else if (trueOrFalse == "false" || trueOrFalse == "0")
    {
        return false;
    }
    else
    {
        std::stringstream ss;
        ss << "Cannot parse '" << trueOrFalse << "' to bool";
        throw ParameterParsingError(ss.str());
    }
}

int ParameterSet::parseInt(const std::string& intString)
{
    try 
    {
        return std::stoi(intString);
    } 
    catch (std::invalid_argument& e)
    {
        std::stringstream msg;
        msg << "Cannot parse int '" << intString << "': " << e.what();
        throw ParameterParsingError(msg.str());
    }
}

float ParameterSet::parseFloat(const std::string& floatString)
{
    try 
    {
        return std::stof(floatString);
    } 
    catch (std::invalid_argument& e)
    {
        std::stringstream msg;
        msg << "Cannot parse float '" << floatString << "': " << e.what();
        throw ParameterParsingError(msg.str());
    }
}


void ParameterSet::ltrim(std::string& s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

void ParameterSet::rtrim(std::string& s)
{
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

void ParameterSet::trim(std::string& s)
{
    ltrim(s);
    rtrim(s);
}

ParameterParsingError ParameterSet::unknownParameterError(const std::string& paramName)
{
    std::stringstream ss;
    ss << "Unknown parameter: '" << paramName << "'";
    return ParameterParsingError(ss.str());
}

std::string ParameterSet::getName() const
{
    return name;
}

void ParameterSet::setName(const std::string& value)
{
    name = value;
}

void ParameterSet::checkFileOpened(const std::ifstream& ifs, const std::string& filename)
{
    if (!ifs.is_open())
    {
        std::stringstream msg;
        msg << "Cannot open file '" << filename << "'";
        throw ParameterParsingError(msg.str());
    }
}




// PARAMETER PARSING ERROR

ParameterParsingError::ParameterParsingError(const std::string& msg) :
    std::runtime_error(msg), msg(msg)
{
    updateMessage();
}

ParameterParsingError::ParameterParsingError(
        const std::string& msg, int lineNr, const std::string& line) :
    std::runtime_error(msg), msg(msg), lineNr(lineNr), line(line)
{
    updateMessage();
}

const char* ParameterParsingError::what() const noexcept
{
    return fullMsg.c_str();
}

int ParameterParsingError::getLineNr() const
{
    return lineNr;
}

void ParameterParsingError::setLineNr(int value)
{
    lineNr = value;
    updateMessage();
}

std::string ParameterParsingError::getMsg() const
{
    return msg;
}

void ParameterParsingError::setMsg(const std::string& value)
{
    msg = value;
    updateMessage();
}

std::string ParameterParsingError::getLine() const
{
    return line;
}

void ParameterParsingError::setLine(const std::string& value)
{
    line = value;
    updateMessage();
}

void ParameterParsingError::updateMessage()
{
    std::stringstream ss;
    ss << msg;
    
    if (lineNr >= 0 || !line.empty())
    {
        ss << "\n in line";
        if (lineNr >= 0)
        {
            ss << " " << lineNr;
        }
        if (!line.empty())
        {
            ss << ": '" << line << "'";
        }
    }
    
    fullMsg = ss.str();
}

