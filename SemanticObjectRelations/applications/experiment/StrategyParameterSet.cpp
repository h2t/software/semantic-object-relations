#include "StrategyParameterSet.h"


#include <sstream>
#include <memory>
#include <algorithm>

#include "ParameterSet.h"

std::vector<StrategyParameterSet> StrategyParameterSet::loadParameterSets(
        const std::string& filename)
{
    std::ifstream file(filename);
    checkFileOpened(file, filename);

    std::vector<StrategyParameterSet> parameterSets;    
    StrategyParameterSet currentParams("");
    
    int lineNr = 0;
    std::string line;
    
    while (std::getline(file, line))
    {
        if (isIgnoredLine(line))
        {
            continue;
        }
        
        if (line.substr(0, 3) == "---")
        {
            // new set
            if (!currentParams.getName().empty())
            {
                // store current params
                parameterSets.push_back(currentParams);
            }
            std::string name = line.substr(3, line.length());
            trim(name);
            currentParams.setName(name);
        }
        else
        {
            // parse assignment and set parameter
            std::string name;
            std::string value;
            try 
            {
                std::tie(name, value) = parseAssignmentLine(line);
                currentParams.setParameter(name, value);
            } 
            catch (ParameterParsingError& e) 
            {
                e.setLineNr(lineNr);
                e.setLine(line);
                throw e;
            }
        }
        
        ++lineNr;
    }
    
    if (!currentParams.getName().empty())
    {
        // store last parameter set
        parameterSets.push_back(currentParams);
    }
    
    return parameterSets;
}

StrategyParameterSet::StrategyParameterSet()
{
}

StrategyParameterSet::StrategyParameterSet(
        const std::string& name, 
        float angleMax, bool verticalSepplaneSupport, bool uncertaintyDetection) :
    ParameterSet(name),
    angleMax(angleMax), 
    verticalSepPlaneSupport(verticalSepplaneSupport), 
    uncertaintyDetection(uncertaintyDetection)
{
    
}

void StrategyParameterSet::setParameter(const std::string& name, const std::string& value)
{
    if (name == "angleMax")
    {
        angleMax = parseFloat(value);
    }
    else if(name == "verticalSepPlaneSupport")
    {
        verticalSepPlaneSupport = parseBool(value);
    }
    else if (name == "uncertaintyDetection") 
    {
        uncertaintyDetection = parseBool(value);
    }
    else
    {
        throw unknownParameterError(name);
    }
}

float StrategyParameterSet::getAngleMax() const
{
    return angleMax;
}

bool StrategyParameterSet::getVerticalSepplaneSupport() const
{
    return verticalSepPlaneSupport;
}

bool StrategyParameterSet::getUncertaintyDetection() const
{
    return uncertaintyDetection;
}

void StrategyParameterSet::setAngleMax(float value)
{
    angleMax = value;
}

void StrategyParameterSet::setVerticalSepplaneSupport(bool value)
{
    verticalSepPlaneSupport = value;
}

void StrategyParameterSet::setUncertaintyDetection(bool value)
{
    uncertaintyDetection = value;
}

std::ostream& operator<<(std::ostream& os, const StrategyParameterSet& ps)
{
    std::string boolString[2] = { "false", "true" };
    return os <<   "+-- Strategy parameters '" << ps.getName() << "' --"
              << "\n| angleMax = " << ps.angleMax
              << "\n| verticalSepPlaneSupport = " << boolString[ps.verticalSepPlaneSupport]
              << "\n| uncertaintyDetection = " << boolString[ps.uncertaintyDetection];
}

std::ostream& operator<<(std::ostream& os, const std::vector<StrategyParameterSet>& pss)
{
    os << "+- Strategy parameter sets (" << pss.size() << ") -\n";
    for (const auto& ps : pss)
    {
        os << ps << "\n";
    }
    return os;
}


