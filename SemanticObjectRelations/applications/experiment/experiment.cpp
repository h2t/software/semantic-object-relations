#include <SemanticObjectRelations/Hooks/Log.h>
#include <SemanticObjectRelations/ShapeExtraction/ShapeExtraction.h>
#include <SemanticObjectRelations/SupportAnalysis/SupportAnalysis.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include "CommonParameterSet.h"
#include "StrategyParameterSet.h"


#define ABORT() do { std::cerr << "Abort." << std::endl; return -1; } while(false)


void writeGraph(const std::string& filename,
                const std::string& strategyName,
                const semrel::SupportGraph& graph)
{
    std::ofstream outfile(filename);
    if (!outfile.is_open())
    {
        std::cerr << "Cannot write to file '" << filename << "'" << std::endl;
        return;
    }

    outfile << "# Result support graph of strategy '" << strategyName << "'\n"
            << "# Numbers correspond to labels of the input point cloud\n"
            << "\n";

    for (semrel::SupportGraph::ConstVertex vertex : graph.vertices())
    {
        outfile << vertex.objectID() << " -> ";

        for (semrel::SupportGraph::ConstEdge outedge : vertex.outEdges())
        {
            outfile << outedge.targetObjectID() << " ";
        }
        outfile << "\n";
    }
}


int main(int argc, const char* argv[])
{
    semrel::LogInterface::setMinimumLogLevel(semrel::LogLevel::DEBUG);

    if (argc < 5)
    {
        std::cout << "Usage: " << argv[0]
                  << " labeled-pointcloud.pcd output_graph_name params-common.txt params-strategy.txt" << std::endl;
        return 1;
    }

    // pcd file input point cloud
    std::string pcdFilename = argv[1];
    // output graph name
    std::string outputGraphName = argv[2];
    // common parameters file
    std::string commonParamsFilename = argv[3];
    // strategy parameter sets file
    std::string strategyParamSetsFilename = argv[4];


    // load parameters

    CommonParameterSet commonParams;
    std::vector<StrategyParameterSet> strategyParamSets;
    try
    {
        std::cout << "Loading common parameters from '" << commonParamsFilename << "' ..." << std::endl;
        commonParams = CommonParameterSet::loadParameterSet(commonParamsFilename);

        std::cout << "Loading strategy parameters from '" << strategyParamSetsFilename << "' ..." << std::endl;
        strategyParamSets = StrategyParameterSet::loadParameterSets(strategyParamSetsFilename);
    }
    catch(ParameterParsingError& e)
    {
        std::cerr << "Error while loading parameters:\n" << e.what() << std::endl;
        ABORT();
    }

    std::cout << commonParams << std::endl;
    std::cout << strategyParamSets << std::endl;



    // load point cloud
    std::cout << "Loading point cloud from '" << pcdFilename << "' ..." << std::endl;

    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr pointcloud(new pcl::PointCloud<pcl::PointXYZRGBL>());
    if (pcl::io::loadPCDFile<pcl::PointXYZRGBL>(pcdFilename, *pointcloud))
    {
        std::cerr << "Could not load PCD file '" << pcdFilename << "'." << std::endl;
        ABORT();
    }

    std::cout << "Loaded point cloud with " << pointcloud->size() << " points.\n" << std::endl;


    // run shape extraction

    // extract objects
    semrel::ShapeExtraction shapeExtraction;
    shapeExtraction.setMaxIterations(commonParams.getMaxIterations());
    shapeExtraction.setDistanceThresholdBox(commonParams.getDistanceThresholdBox());
    shapeExtraction.setDistanceThresholdPCL(commonParams.getDistanceThresholdPCL());
    shapeExtraction.setMinInlierRate(commonParams.getMinInlierRate());
    shapeExtraction.setOutlierRate(commonParams.getOutlierRate());
    shapeExtraction.setConcurrencyEnabled(commonParams.getConcurrencyEnabled());

    std::cout << "Running shape extraction ..." << std::endl;
    semrel::ShapeExtractionResult shapexResult = shapeExtraction.extract(*pointcloud);
    std::cout << "Extracted shapes: " << shapexResult.objects << "\n" << std::endl;


    // RUN SUPPORT ANALYSIS FOR DIFFERENT PARAMETERS

    std::cout << "Running support analysis with " << strategyParamSets.size()
              << " strategies ... " << std::endl;

    semrel::SupportAnalysis supportAnalysis;
    supportAnalysis.setGravityVector(commonParams.getGravity());
    supportAnalysis.setContactMargin(commonParams.getContactMargin());
    supportAnalysis.setSupportAreaRatioMin(commonParams.getSupportAreaRatioMin());

    for (StrategyParameterSet& sp : strategyParamSets)
    {
        std::cout << "=== Strategy '" << sp.getName() << "' ===" << std::endl;

        // set strategy params
        supportAnalysis.setVertSepPlaneAngleMax(sp.getAngleMax());
        supportAnalysis.setVertSepPlaneAssumeSupport(sp.getVerticalSepplaneSupport());
        supportAnalysis.setUncertaintyDetectionEnabled(sp.getUncertaintyDetection());


        std::cout << "Performing support analysis ..." << std::endl;
        semrel::SupportGraph supportGraph = supportAnalysis.performSupportAnalysis(shapexResult.objects);

        std::cout << "Support graph: " << supportGraph << std::endl;

        // write .txt, .dot and .png files of support graph

        std::stringstream graphName;
        graphName << outputGraphName << "_" << sp.getName();

        semrel::SupportGraph::getGraphvizWriter().write(supportGraph, graphName.str());

        graphName << ".txt";
        writeGraph(graphName.str(), sp.getName(), supportGraph);
    }

    return 0;
}


