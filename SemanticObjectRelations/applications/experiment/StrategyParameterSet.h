#pragma once

#include "ParameterSet.h"


class StrategyParameterSet : public ParameterSet
{
public:
    
    static std::vector<StrategyParameterSet> loadParameterSets(const std::string& filename);
    
    StrategyParameterSet();
    StrategyParameterSet(const std::string& name = "",
                       float angleMax = 0, 
                       bool verticalSepPlaneSupport = false, 
                       bool uncertaintyDetection = false);

    virtual void setParameter(const std::string& name, const std::string& value) override;
    
    float getAngleMax() const;
    void setAngleMax(float value);
    
    bool getVerticalSepplaneSupport() const;
    void setVerticalSepplaneSupport(bool value);
    
    bool getUncertaintyDetection() const;
    void setUncertaintyDetection(bool value);

    friend std::ostream& operator<<(std::ostream& os, const StrategyParameterSet& ps);
    
    
private:
    
    
    float angleMax = 0;
    bool verticalSepPlaneSupport = false;
    bool uncertaintyDetection = false;
    
};

    std::ostream& operator<<(std::ostream& os, const std::vector<StrategyParameterSet>& pss);
