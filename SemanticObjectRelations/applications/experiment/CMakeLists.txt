set(TARGET_NAME "${PROJECT_NAME}Experiment")

find_package(SemanticObjectRelations REQUIRED)
set(LIBRARIES ${SemanticObjectRelations_LIBRARIES})

set(SOURCES
    experiment.cpp
    CommonParameterSet.cpp
    ParameterSet.cpp
    StrategyParameterSet.cpp
)

set(HEADERS
    CommonParameterSet.h
    ParameterSet.h
    StrategyParameterSet.h
)

file (COPY params-strategy.txt DESTINATION ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
file (COPY params-common.txt DESTINATION ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})


add_executable(${TARGET_NAME} ${SOURCES} ${HEADERS})
target_link_libraries(${TARGET_NAME} PRIVATE ${LIBRARIES})
