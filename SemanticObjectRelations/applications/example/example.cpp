#include <SemanticObjectRelations/Hooks/Log.h>
#include <SemanticObjectRelations/ShapeExtraction/ShapeExtraction.h>
#include <SemanticObjectRelations/SupportAnalysis/SupportAnalysis.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>


int main(int argc, const char* argv[])
{
    semrel::LogInterface::setMinimumLogLevel(semrel::LogLevel::INFO);
    
    // pcd file with example point cloud
    std::string pcdFilename = "segmented-point-cloud.pcd";
    if (argc > 1)
    {
        pcdFilename = argv[1];
    }
    // output filename
    std::string graphOutputFilename = "support-graph";
    if (argc > 2)
    {
        graphOutputFilename = argv[2];
    }
      
    // load point cloud
    std::cout << "Loading point cloud from '" << pcdFilename << "' ..." << std::endl;
    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr pointcloud(new pcl::PointCloud<pcl::PointXYZRGBL>());
    if (pcl::io::loadPCDFile<pcl::PointXYZRGBL>(pcdFilename, *pointcloud))
    {
        std::cout << "Could not load PCD file '" << pcdFilename << "'." << std::endl;;
        return -1;
    }
    std::cout << "Loaded point cloud with " << pointcloud->size() << " points." << std::endl;;
    
    // extract objects
    semrel::ShapeExtraction shapeExtraction;
    shapeExtraction.setMaxIterations(100);
    shapeExtraction.setDistanceThreshold(5.0);
    shapeExtraction.setMinInlierRate(0.7f);
    shapeExtraction.setOutlierRate(0.00125f);
    shapeExtraction.setConcurrencyEnabled(true);
    
    std::cout << "Running shape extraction ..." << std::endl;;
    semrel::ShapeExtractionResult shapexResult = shapeExtraction.extract(*pointcloud);
    std::cout << "Extracted shapes: " << shapexResult.objects << std::endl;;
    
    // analyze support relations
    semrel::SupportAnalysis supportAnalysis;
    supportAnalysis.setGravityVector(- Eigen::Vector3f::UnitZ());
    supportAnalysis.setContactMargin(10);
    
    supportAnalysis.setVertSepPlaneAngleMax(10);
    supportAnalysis.setVertSepPlaneAssumeSupport(true);
    
    supportAnalysis.setUncertaintyDetectionEnabled(true);
    supportAnalysis.setSupportAreaRatioMin(0.7f);
    
    std::cout << "Performing support analysis ..." << std::endl;
    semrel::SupportGraph supportGraph = supportAnalysis.performSupportAnalysis(shapexResult.objects);
    
    std::cout << "Support graph: " << supportGraph << std::endl;
    
    // write a png file of the support graph
    semrel::SupportGraph::getGraphvizWriter().write(supportGraph, graphOutputFilename);

    return 0;
}


