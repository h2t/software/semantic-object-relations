#include "SpatialRelations.h"

#include <ostream>

#include <boost/algorithm/string.hpp>

#include <SemanticObjectRelations/Serialization/type_name.h>


using namespace semrel;



std::vector<SpatialRelations::Type> SpatialRelations::makeRelationTypes()
{
    return {
        Type::contact,

        Type::static_above,
        Type::static_below,
        Type::static_left_of,
        Type::static_right_of,
        Type::static_behind,
        Type::static_in_front_of,
        Type::static_around,
        Type::static_inside,
        Type::static_surrounding,

        Type::static_bottom,
        Type::static_top,
        Type::static_contact_around,

        Type::dynamic_moving_together,
        Type::dynamic_halting_together,
        Type::dynamic_fixed_moving_together,
        Type::dynamic_getting_close,
        Type::dynamic_moving_apart,
        Type::dynamic_stable,
    };
}

std::map<SpatialRelations::Type, std::string> SpatialRelations::makeRelationNames()
{
    std::map<Type, std::string> names;

    names[Type::contact] = "contact";

    names[Type::static_above] = "(static) above";
    names[Type::static_below] = "(static) below";
    names[Type::static_left_of] = "(static) left of";
    names[Type::static_right_of] = "(static) right of";
    names[Type::static_behind] = "(static) behind of";
    names[Type::static_in_front_of] = "(static) in front of";
    names[Type::static_around] = "(static) around";
    names[Type::static_inside] = "(static) inside";
    names[Type::static_surrounding] = "(static) surround";

    names[Type::static_bottom] = "(static) bottom";
    names[Type::static_top] = "(static) top";
    names[Type::static_contact_around] = "(static) contact around";

    names[Type::dynamic_moving_together] = "(dynamic) moving together";
    names[Type::dynamic_halting_together] = "(dynamic) halting together";
    names[Type::dynamic_fixed_moving_together] = "(dynamic) fixed moving together";
    names[Type::dynamic_getting_close] = "(dynamic) getting close";
    names[Type::dynamic_moving_apart] = "(dynamic) moving apart";
    names[Type::dynamic_stable] = "(dynamic) stable";

    return names;
}


const std::vector<SpatialRelations::Type> SpatialRelations::types = makeRelationTypes();
const std::map<SpatialRelations::Type, std::string> SpatialRelations::names = makeRelationNames();
const SpatialRelations SpatialRelations::staticTypes = {
    {
        Type::static_above,
        Type::static_below,
        Type::static_left_of,
        Type::static_right_of,
        Type::static_behind,
        Type::static_in_front_of,
        Type::static_around,
        Type::static_inside,
        Type::static_surrounding,

        Type::static_bottom,
        Type::static_top,
        Type::static_contact_around,
    }
};

const SpatialRelations SpatialRelations::dynamicTypes = {
    {
        Type::dynamic_moving_together,
        Type::dynamic_halting_together,
        Type::dynamic_fixed_moving_together,
        Type::dynamic_getting_close,
        Type::dynamic_moving_apart,
        Type::dynamic_stable,
    }
};


SpatialRelations::SpatialRelations() :
    _bitset(0)
{
    // pass
}

SpatialRelations::SpatialRelations(const std::vector<Type>& activeRelations)
{
    for (Type type : activeRelations)
    {
        set(type, true);
    }
}


bool SpatialRelations::get(SpatialRelations::Type type) const
{
    return _bitset.test(static_cast<std::size_t>(type));
}

void SpatialRelations::set(SpatialRelations::Type type, bool value)
{
    _bitset.set(static_cast<std::size_t>(type), value);
}


SpatialRelations::SpatialRelations(const relations_bitset& relations_bitset) :
    _bitset(relations_bitset)
{
    // pass
}


SpatialRelations
SpatialRelations::filter(const SpatialRelations& filterMask) const
{
    return _bitset bitand filterMask._bitset;
}

bool SpatialRelations::any() const
{
    return _bitset.any();
}


std::vector<std::string>
SpatialRelations::getStringList() const
{
    std::vector<std::string> v;
    for (Type t : types)
    {
        if (get(t))
        {
            v.push_back(names.at(t));
        }
    }
    return v;
}


std::ostream& semrel::operator<<(std::ostream& os, const SpatialRelations::Type type)
{
    if (auto find = SpatialRelations::names.find(type); find != SpatialRelations::names.end())
    {
        return os << SpatialRelations::names.at(type);
    }
    else
    {
        return os << "<unknown>";
    }
}

std::ostream& semrel::operator<<(std::ostream& os, const SpatialRelations& rels)
{
    return os << boost::join(rels.getStringList(), " | ");
}
