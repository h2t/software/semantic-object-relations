#pragma once

#include <SemanticObjectRelations/Serialization/json.h>

#include "SpatialGraph.h"


namespace semrel
{

    void to_json(nlohmann::json& j, const SpatialRelations& rels);
    void from_json(const nlohmann::json& j, SpatialRelations& rels);

    void to_json(nlohmann::json& j, const SpatialVertex& v);
    void from_json(const nlohmann::json& j, SpatialVertex& v);

    void to_json(nlohmann::json& j, const SpatialEdge& e);
    void from_json(const nlohmann::json& j, SpatialEdge& e);

    // SpatialGraphAttribs == NoAttrib
    // void to_json(nlohmann::json& j, const SpatialGraphAttribs& g);
    // void from_json(const nlohmann::json& j, SpatialGraphAttribs& g);

}

