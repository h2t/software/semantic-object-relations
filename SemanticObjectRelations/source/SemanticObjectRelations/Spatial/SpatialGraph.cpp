#include "SpatialGraph.h"


using namespace semrel;



SpatialGraph SpatialGraph::filtered(const SpatialRelations& relationMask) const
{
    // Copy this to transfer all attributes.
    SpatialGraph result = *this;

    // List edges for removal.
    std::vector<Edge> edgesToRemove;
    for (auto edge : result.edges())
    {
        if (!edge.attrib().relations.filter(relationMask).any())
        {
            edgesToRemove.push_back(edge);
        }
    }

    // Remove edges.
    for (auto edge : edgesToRemove)
    {
        result.removeEdge(edge);
    }

    return result;
}
