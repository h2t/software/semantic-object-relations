#pragma once

#include <stdexcept>
#include <string>


namespace semrel::error
{

    /// Base class for all exceptions thrown by this library.
    class SemanticObjectRelationsError : public std::runtime_error
    {
    public:
        SemanticObjectRelationsError(const std::string& msg);
    };

}
