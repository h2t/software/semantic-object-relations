#pragma once

#include <string>

#include <SemanticObjectRelations/Shapes.h>


namespace semrel
{

/**
 * @brief Visualization levels with decreasing detail (similar to log levels).
 */
enum class VisuLevel
{
    LIVE_VISU,  ///< Live visualization ("animated", i.e. with sleeps).
    VERBOSE,    ///< Verbose visualization (some internals)
    RESULT,     ///< Result visualization (extracted shapes and support graph).
    USER,       ///< User visualization (not used by library).
    DISABLED,   ///< No visualization.
};

/**
 * @brief Visualization meta information.
 * Layers serve like namespaces. In each layer, a name serves as a unique
 * identifier. Subsequent draw calls to the same name in the same layer
 * shall overwrite each other.
 */
struct VisuMetaInfo
{
    VisuMetaInfo();
    VisuMetaInfo(const std::string layer, const std::string& name);

    std::string layer; ///< Name of the containing layer (aka namespace).
    std::string name;  ///< Unique name (in the layer).
};


/**
 * @brief RGBA color (with color values in [0, 1]).
 */
struct DrawColor
{
    /// Initialize to black (full alpha).
    DrawColor();
    /// Construct with given values.
    DrawColor(float r, float g, float b, float a = 1);

    float r = 0;
    float g = 0;
    float b = 0;
    float a = 1;
};

/**
 * @brief The visualizer interface (hook for your own implementation).
 *
 * In order to use your own visualization facilities, derive from and implement
 * `VisualizerInterface` and call `VisualizerInterface::setImplementation()`
 * with an instance of your implementation. All draw calls will be directed
 * to this instance.
 */
class VisualizerInterface
{

public:

    static VisualizerInterface& get();
    static void setImplementation(std::shared_ptr<VisualizerInterface> implementation);

    static VisuLevel getMinimumVisuLevel();
    static void setMinimumVisuLevel(VisuLevel level);


public:

    /// Constructor.
    VisualizerInterface();
    /// Virtual destructor.
    virtual ~VisualizerInterface();

    /// Clear all visualizations on all layers.
    virtual void clearAll();
    /// Clear all visualizations on the given layer.
    virtual void clearLayer(const std::string& layer);


    // GEOMETRIC PRIMITIVES

    /// Draw a line.
    virtual void drawLine(VisuMetaInfo id, const Eigen::Vector3f& start, const Eigen::Vector3f& end, float lineWidth, DrawColor color);
    /// Draw an arrow.
    virtual void drawArrow(VisuMetaInfo id, const Eigen::Vector3f& origin, const Eigen::Vector3f& direction, float length, float width, DrawColor color);

    /// Draw a box (cuboid).
    virtual void drawBox(VisuMetaInfo id, const Box& box, DrawColor color);
    /// Draw a cylinder.
    virtual void drawCylinder(VisuMetaInfo id, const Cylinder& cylinder, DrawColor color);
    /// Draw a sphere.
    virtual void drawSphere(VisuMetaInfo id, const Sphere& sphere, DrawColor color);

    /// Draw a polygon (color filled, if possible).
    virtual void drawPolygon(VisuMetaInfo id, const std::vector<Eigen::Vector3f>& polygonPoints,
                             float lineWidth, DrawColor colorInner, DrawColor colorBorder);
    /// Draw a triangle mesh.
    virtual void drawTriMesh(VisuMetaInfo id, const TriMesh& mesh, DrawColor color);


    // TEXT

    /// Draw text.
    virtual void drawText(VisuMetaInfo id, const std::string& text, const Eigen::Vector3f& position, float size, DrawColor color);


    // POINT CLOUD

    /// Draw a point cloud.
    virtual void drawPointCloud(semrel::VisuMetaInfo id, const std::vector<Eigen::Vector3f>& cloud, float pointSize, semrel::DrawColor color);


    float metricScaling = 1.f;
    float textScaling = 1.f;


private:

    /// The implementation.
    static std::shared_ptr<VisualizerInterface> implementation;

    /// The minimum visualization level.
    static VisuLevel minimumLevel;

};

}
