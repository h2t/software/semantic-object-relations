#pragma once

#include "Shape.h"

namespace semrel
{

/**
     * @brief A shape represented by a triangle mesh.
     *
     * A mesh shape has a position and orientation w.r.t. its parent coordinate frame,
     * as well a triangle mesh (TriMesh) located in the local coordinate frame.
     */
class MeshShape : public Shape
{
public:
    
    /// Initialize with zero position and orientation and empty mesh, with optional ID.
    explicit MeshShape(ShapeID id = ShapeID(0));
    /// Initialize with given position, orientation, optional ID, and empty mesh.
    MeshShape(const Eigen::Vector3f& position, const Eigen::Quaternionf& orientation,
              ShapeID id = ShapeID(0));
    /// Initialize with zero position and orientation and given mesh, and optional ID.
    explicit MeshShape(const TriMesh& mesh, ShapeID id = ShapeID(0));
    /// Initialize with given position, orientation, mesh, and optional ID.
    MeshShape(const Eigen::Vector3f& position, const Eigen::Quaternionf& orientation,
              const TriMesh& mesh, ShapeID id = ShapeID(0));
    
    /// Copy constructor. Copies the internal TriMesh.
    MeshShape(const MeshShape& other);
    /// Move constructor. Moves the internal TriMesh.
    MeshShape(MeshShape&& other);
    
    
    // Shape interface
    virtual Eigen::Vector3f getPosition() const override;
    virtual void setPosition(const Eigen::Vector3f& position) override;
    
    virtual Eigen::Quaternionf getOrientation() const override;
    virtual void setOrientation(const Eigen::Quaternionf& orientation) override;
    
    virtual TriMesh getTriMeshLocal() const override;
    
    float getBoundingSphereRadius() const override;
    
    
    virtual std::string name() const override;
    virtual std::string tagPrefix() const override;  ///< Returns "Msh".
    virtual std::string str() const override;
    
    virtual std::shared_ptr<btCollisionShape> getBulletCollisionShape(float margin = 0) const override;
    virtual void addMargin(float margin) override;
    
    
    // MeshShape interface
    
    /// Get a reference to the triangle mesh.
    TriMesh& mesh();
    const TriMesh& mesh() const;
    
    /// Copy assignment operator. Copies the internal TriMesh.
    MeshShape& operator=(const MeshShape& other);
    /// Move assignment operator. Moves the internal TriMesh.
    MeshShape& operator=(MeshShape&& other);
    
    
private:
    
    /// The position.
    Eigen::Vector3f position = Eigen::Vector3f::Zero();
    /// The orientation.
    Eigen::Quaternionf orientation = Eigen::Quaternionf::Identity();
    /// The triangle mesh (in local frame).
    std::unique_ptr<TriMesh> _mesh { new TriMesh{} };
    
};

}
