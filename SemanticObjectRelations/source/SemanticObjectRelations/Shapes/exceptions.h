#pragma once

#include <sstream>
#include <stdexcept>

#include <SemanticObjectRelations/exceptions.h>
#include <SemanticObjectRelations/Shapes/Shape.h>


namespace semrel::error
{

    /// Indicates that an object pointer was null.
    class ShapePointerIsNull : public SemanticObjectRelationsError
    {
    public:
        ShapePointerIsNull(const std::string& msg);

        static ShapePointerIsNull InShapeList(std::size_t index);
        static ShapePointerIsNull InShapeMap(ShapeID id);

    private:
        static std::string makeMsg(const std::string& msg);
    };

}
