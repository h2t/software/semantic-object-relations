#pragma once

#include <iosfwd>
#include <vector>

#include <Eigen/Core>

#include "AxisAlignedBoundingBox.h"


namespace semrel
{

/**
 * @brief A triangle mesh.
 *
 * A triangle mesh consists of a number of triangles, each of which is comprised of
 * three vertices and corresponding normals.
 * A TriMesh instance holds separate containers (std::vector) for all occuring vertices
 * and normals. Each triangle stores the indices of its vertices and normals w.r.t. the
 * corresponding containers.
 */
class TriMesh
{
public:

    /// A single triangle with 3 vertex indices and 3 normal indices.
    class Triangle
    {
    public:
        /// Constructor.
        explicit Triangle(int v0 = 0, int v1 = 0, int v2 = 0, int n0 = 0, int n1 = 0, int n2 = 0);
        /// Construct from indices in Eigen::Vector3i.
        explicit Triangle(const Eigen::Vector3i vs, const Eigen::Vector3i& ns = Eigen::Vector3i::Zero());

        static Triangle singleNormal(int v0, int v1, int v2, int n)
        {
            return Triangle(v0, v1, v2, n, n, n);
        }

        /// Vertex indices (indices into the vertex container)
        int v0, v1, v2;
        /// Normal indices (indices into the normal container)
        int n0, n1, n2;

        // Operators
        bool operator==(const Triangle& rhs);
        bool operator!=(const Triangle& rhs);
        friend std::ostream& operator<<(std::ostream& os, const Triangle& t);
    };


public:

    /// Constructor.
    TriMesh();


    /// Add a vertex to the vertex container.
    void addVertex(const Eigen::Vector3f& vertex);
    /// Add a normal to the normal container.
    void addNormal(const Eigen::Vector3f& normal);
    /// Add a triangle.
    void addTriangle(const Triangle& triangle);

    /// Get a reference to the vertex at given index.
    Eigen::Vector3f& getVertex(int index);
    /// Get a the vertex at given index.
    const Eigen::Vector3f& getVertex(int index) const;

    /// Get a reference to the normal at given index.
    Eigen::Vector3f& getNormal(int index);
    /// Get a the normal at given index.
    const Eigen::Vector3f& getNormal(int index) const;

    /// Get the vertex vector.
    std::vector<Eigen::Vector3f>& getVertices();
    /// Get the vertex vector (const).
    const std::vector<Eigen::Vector3f>& getVertices() const;

    /// Get the normal vector.
    std::vector<Eigen::Vector3f>& getNormals();
    /// Get the normal vector (const).
    const std::vector<Eigen::Vector3f>& getNormals() const;

    /// Get the triangles.
    std::vector<Triangle>& getTriangles();
    /// Get the triangles (const).
    const std::vector<Triangle>& getTriangles() const;

    /// Get the number of vertices.
    int numVertices() const;
    /// Get the number of normals.
    int numNormals() const;
    /// Get the number of triangles.
    int numTriangles() const;

    /// Get the axis aligned bounding box (AABB) of this mesh.
    AxisAlignedBoundingBox getAxisAlignedBoundingBox() const;
    /// Alias for `getAxisAlignedBoundingBox()`.
    AxisAlignedBoundingBox getAABB() const;


    /// Get a human readable string representation of this TriMesh.
    std::string str() const;

    /// Writes a human readable summary of this TriMesh to os.
    friend std::ostream& operator<<(std::ostream& os, const TriMesh& m);


private:

    /// The vertex container.
    std::vector<Eigen::Vector3f> vertices;
    /// The normal container.
    std::vector<Eigen::Vector3f> normals;
    /// The triangles.
    std::vector<Triangle> triangles;

};

}

