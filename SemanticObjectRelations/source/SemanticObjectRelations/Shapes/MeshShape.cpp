#include "MeshShape.h"

#include <btBulletDynamicsCommon.h>


namespace semrel
{

MeshShape::MeshShape(ShapeID id) : 
    Shape(id)
{}

MeshShape::MeshShape(const Eigen::Vector3f& position, const Eigen::Quaternionf& orientation, 
                     ShapeID id) :
    Shape(id), position(position), orientation(orientation)
{}

MeshShape::MeshShape(const TriMesh& mesh, ShapeID id) :
    Shape(id), _mesh(new TriMesh(mesh))
{}

MeshShape::MeshShape(const Eigen::Vector3f& position, const Eigen::Quaternionf& orientation,
                     const TriMesh& mesh, ShapeID id) : 
    Shape(id), position(position), orientation(orientation), _mesh(new TriMesh(mesh))
{}

MeshShape::MeshShape(const MeshShape& other) : 
    Shape(other.getID()),
    position(other.position), orientation(other.orientation), _mesh(new TriMesh(*other._mesh))
{}

MeshShape::MeshShape(MeshShape&& other) : 
    Shape(other.getID()),
    position(other.position), orientation(other.orientation), _mesh(std::move(other._mesh))
{}

Eigen::Vector3f MeshShape::getPosition() const
{
    return position;
}

void MeshShape::setPosition(const Eigen::Vector3f& position)
{
    this->position = position;
}

Eigen::Quaternionf MeshShape::getOrientation() const
{
    return orientation;
}

void MeshShape::setOrientation(const Eigen::Quaternionf& orientation)
{
    this->orientation = orientation;
}

TriMesh& MeshShape::mesh()
{
    return *_mesh;
}

const TriMesh& MeshShape::mesh() const
{
    return *_mesh;
}

MeshShape& MeshShape::operator=(const MeshShape& other)
{
    if (this != &other)
    {
        this->position = other.position;
        this->orientation = other.orientation;
        this->_mesh.reset(new TriMesh(*other._mesh));
    }
    return *this;
}

MeshShape& MeshShape::operator=(MeshShape&& other)
{
    if (this != &other)
    {
        this->position = other.position;
        this->orientation = other.orientation;
        this->_mesh = std::move(other._mesh);
    }
    return *this;
}

TriMesh MeshShape::getTriMeshLocal() const
{
    return *_mesh;
}

float MeshShape::getBoundingSphereRadius() const
{
    float radiusSquared = 0;
    
    for (const auto& v : _mesh->getVertices())
    {
        radiusSquared = std::max(radiusSquared, v.squaredNorm());
    }
    
    return std::sqrt(radiusSquared);
}

std::string MeshShape::name() const
{
    return "MeshShape";
}

std::string MeshShape::tagPrefix() const
{
    return "Msh";
}

std::string MeshShape::str() const
{
    std::stringstream ss;
    ss << name() << " ["
       << "pos: " << toString(position)
       << ", ori: " << toString(orientation)
       << ", " << *_mesh
       << ", " << Shape::str()
       << "]";
    return ss.str();
}

std::shared_ptr<btCollisionShape> MeshShape::getBulletCollisionShape(float margin) const
{
    TriMesh trimesh = getTriMesh();
    btTriangleMesh* btTrimesh = new btTriangleMesh();
    
    // add margin to all vertices
    for (Eigen::Vector3f& vertex : trimesh.getVertices())
    {
        vertex += margin * vertex.normalized();
    }
    
    auto eigenToBullet = [](const Eigen::Vector3f& v)
    {
        auto vc = v.cast<btScalar>();
        return btVector3(vc(0), vc(1), vc(2));
    };
    
    for (const TriMesh::Triangle& tri : trimesh.getTriangles())
    {
        btTrimesh->addTriangle(eigenToBullet(trimesh.getVertex(tri.v0)),
                               eigenToBullet(trimesh.getVertex(tri.v1)),
                               eigenToBullet(trimesh.getVertex(tri.v2)));
    }

    return std::shared_ptr<btCollisionShape>(new btBvhTriangleMeshShape(btTrimesh, true));
}

void MeshShape::addMargin(float margin)
{
    // add margin to all vertices
    for (Eigen::Vector3f& vertex : _mesh->getVertices())
    {
        vertex += margin * vertex.normalized();
    }
}

}
