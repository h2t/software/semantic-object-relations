#pragma once

#include "Shape.h"


namespace semrel
{

/**
 * @brief A box, or cuboid, in 3D space.
 *
 * Besides a position and orientation, a box is specified by its three extents along
 * each local axis. The three axes can be stored as column vectors in a 3x3 matrix R.
 * This matrix R is also the rotation matrix from local frame to parent frame.
 * That is, the matrix R can be used to
 * (1) get the coordinates of a local vector in parent frame (base change), as in:
 *     R * v_local = v_parent
 * (2) to apply the rotation to a vector in parent frame, as in R * v = v'.
 *
 * The position of the box is its center. The standard orientation corresponds to a
 * box whose three local axes are aligned with the X, Y, and Z axis, respectively
 * (i.e., the rotation matrix R is the identity).
 *
 * The extents are represented as a Eigen::Vector3f, where each coordinate corresponds
 * to the respective axis. An extent is the distance between two opposing faces.
 * That is, an extent of 2.0 indicates that the box is 2.0 wide, and the distance of
 * each face to the center is 1.0 (half the extent).
 */
class Box : public Shape
{
public:

    /// Initialize to zero with optional ID.
    explicit Box(ShapeID id = ShapeID(0));

    /// Initialize with position, orientation, extents and optional ID.
    Box(const Eigen::Vector3f& position, const Eigen::Quaternionf& orientation,
        const Eigen::Vector3f& extents, ShapeID id = ShapeID{0});

    /// Inizialize with position, orientation from box axes, extents and optional ID.
    Box(const Eigen::Vector3f& position, const Eigen::Matrix3f& axes,
        const Eigen::Vector3f& extents, ShapeID id = ShapeID{0});

    /// Construct a box from the given axis aligned bounding box.
    Box(const AxisAlignedBoundingBox& aabb, ShapeID id = ShapeID{0});

    /// Construct a box from the given (local) axis aligned bounding box and pose.
    Box(const AxisAlignedBoundingBox& aabb, const Eigen::Vector3f& position,
        const Eigen::Quaternionf& orientation, ShapeID id = ShapeID{0});

    /// Get a Box from the local AABB of the given Shape.
    static Box fromAABBLocal(const Shape& shape);
    /// Get a Box from the global AABB of the given Shape.
    static Box fromAABB(const Shape& shape);


    // Shape interface

    virtual Eigen::Vector3f getPosition() const override;
    virtual void setPosition(const Eigen::Vector3f& position) override;

    virtual Eigen::Quaternionf getOrientation() const override;
    virtual void setOrientation(const Eigen::Quaternionf& orientation) override;

    virtual TriMesh getTriMeshLocal() const override;

    // Provide optimized implementations of Shape::getAABB*() for boxes.
    AxisAlignedBoundingBox getAABBLocal() const override;

    // Optimized implementation for boxes.
    virtual float getBoundingSphereRadius() const override;

    virtual std::string name() const override;
    virtual std::string str() const override;

    virtual std::shared_ptr<btCollisionShape> getBulletCollisionShape(float margin = 0) const override;
    virtual void addMargin(float margin) override;


    // Box interface

    /// Get the local axes as column vectors in a 3x3 matrix (or rotation matrix).
    Eigen::Matrix3f getAxes() const;
    /// Set the orientation according to the given axes (or rotation matrix).
    void setAxes(const Eigen::Matrix3f& axes);

    /// Get the extents.
    Eigen::Vector3f getExtents() const;
    /// Set the extents.
    void setExtents(const Eigen::Vector3f& extents);


    /**
     * @brief Get the six faces of the box.
     * Every face's normal points towards the outside of the box.
     *
     * The order is as follows: [ n1+ n1- n2+ n2- n3+ n3- ]
     * where nx{+-} means: plane with normal x (in {0,1,2})
     * positioned at positive (+) / negative (-) side of the box
     */
    std::vector<Hyperplane3f> getFaces() const;

    /**
     * @brief Get the eight corners of the box.
     * The order of the vectors is as follows (w.r.t. local axes):
     *
     *        7_________________ 3
     *       /|                /|
     *      / |               / |    ^
     *    4/________________0/  |    |
     *     |  |              |  |    | y
     *     |  |              |  |    |
     *     |  |              |  |
     *     |  |6_____________|__|2
     *     |  /              |  /    /
     *     | /               | /    / z
     *     |/________________|/    v
     *    5                  1
     *         x ----->
     */
    std::vector<Eigen::Vector3f> getCorners() const;


private:

    /// Compute and return the orientation corresponding to the given axes.
    static Eigen::Quaternionf orientationFromAxes(const Eigen::Matrix3f& axes);


    /// The centroid.
    Eigen::Vector3f position = Eigen::Vector3f::Zero();

    /// The orientations.
    Eigen::Quaternionf orientation = Eigen::Quaternionf::Identity();

    /// The box extents along the normals, aka width, height, depth.
    Eigen::Vector3f extents = Eigen::Vector3f::Zero();

};

}
