#include "TriMesh.h"

using namespace semrel;

TriMesh::TriMesh()
{}

void TriMesh::addVertex(const Eigen::Vector3f& vertex)
{
    vertices.push_back(vertex);
}

void TriMesh::addNormal(const Eigen::Vector3f& normal)
{
    normals.push_back(normal);
}

void TriMesh::addTriangle(const TriMesh::Triangle& triangle)
{
    triangles.push_back(triangle);
}

Eigen::Vector3f& TriMesh::getVertex(int index)
{
    return vertices.at(static_cast<std::size_t>(index));
}

const Eigen::Vector3f& TriMesh::getVertex(int index) const
{
    return vertices.at(static_cast<std::size_t>(index));
}

Eigen::Vector3f& TriMesh::getNormal(int index)
{
    return normals.at(static_cast<std::size_t>(index));
}

const Eigen::Vector3f& TriMesh::getNormal(int index) const
{
    return normals.at(static_cast<std::size_t>(index));
}

std::vector<Eigen::Vector3f>& TriMesh::getVertices()
{
    return vertices;
}

const std::vector<Eigen::Vector3f>& TriMesh::getVertices() const
{
    return vertices;
}

std::vector<Eigen::Vector3f>& TriMesh::getNormals()
{
    return normals;
}

const std::vector<Eigen::Vector3f>& TriMesh::getNormals() const
{
    return normals;
}

std::vector<TriMesh::Triangle>& TriMesh::getTriangles()
{
    return triangles;
}

const std::vector<TriMesh::Triangle>& TriMesh::getTriangles() const
{
    return triangles;
}

std::string TriMesh::str() const
{
    std::stringstream ss;
    ss << "TriMesh ("
       << numVertices() << " vertices, "
       << numNormals() << " normals, "
       << numTriangles() << " triangles"
       << ")";
    return ss.str();
}

int TriMesh::numVertices() const
{
    return static_cast<int>(vertices.size());
}

int TriMesh::numNormals() const
{
    return static_cast<int>(normals.size());
}

int TriMesh::numTriangles() const
{
    return static_cast<int>(triangles.size());
}

AxisAlignedBoundingBox TriMesh::getAxisAlignedBoundingBox() const
{
    Eigen::Array3f min = vertices.front();
    Eigen::Array3f max = vertices.front();

    for (auto&& v : vertices)
    {
        min = min.min(v.array());
        max = max.max(v.array());
    }

    AxisAlignedBoundingBox aabb;
    aabb.min() = min.matrix();
    aabb.max() = max.matrix();
    return aabb;
}

AxisAlignedBoundingBox TriMesh::getAABB() const
{
    return getAxisAlignedBoundingBox();
}

TriMesh::Triangle::Triangle(int v0, int v1, int v2, int n0, int n1, int n2) :
    v0(v0), v1(v1), v2(v2), n0(n0), n1(n1), n2(n2)
{}

TriMesh::Triangle::Triangle(const Eigen::Vector3i vs, const Eigen::Vector3i& ns) :
    TriMesh::Triangle(vs(0), vs(1), vs(2), ns(0), ns(1), ns(2))
{}

bool TriMesh::Triangle::operator==(const TriMesh::Triangle& rhs)
{
    return v0 == rhs.v0 && v1 == rhs.v1 && v2 == rhs.v2
           && n0 == rhs.n0 && n1 == rhs.n1 && n2 == rhs.n2;
}

bool TriMesh::Triangle::operator!=(const TriMesh::Triangle& rhs)
{
    return !(*this == rhs);
}


namespace semrel
{
    std::ostream& operator<<(std::ostream& os, const TriMesh& m)
    {
        return os << m.str();
    }

    std::ostream& operator<<(std::ostream& os, const TriMesh::Triangle& t)
    {
        return os << "Triangle [v: " << t.v0 << " " << t.v1 << " " << t.v2
               << " |n: " << t.n0 << " " << t.n1 << " " << t.n2 << " ]";
    }
}


