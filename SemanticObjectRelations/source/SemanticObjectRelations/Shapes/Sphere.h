#pragma once

#include "Shape.h"


namespace semrel
{

/**
 * @brief A sphere in 3D space.
 *
 * A sphere has a position (its center) and radius, but no orientation.
 * (getOrientation() always returns Identity, and setOrientation() is ignored.)
 */
class Sphere : public Shape
{
public:

    /// Initialize everything to zero, with optional ID.
    explicit Sphere(ShapeID id = ShapeID(0));
    /// Initialize with position and radius, and optional ID.
    Sphere(const Eigen::Vector3f& position, float radius, ShapeID id = ShapeID(0));


    // Shape interface

    virtual Eigen::Vector3f getPosition() const override;
    virtual void setPosition(const Eigen::Vector3f& position) override;

    virtual Eigen::Quaternionf getOrientation() const override;
    virtual void setOrientation(const Eigen::Quaternionf& orientation) override;

    virtual TriMesh getTriMeshLocal() const override;

    // Provide optimized implementations of Shape::getAABB*() for spheres.
    virtual AxisAlignedBoundingBox getAABBLocal() const override;
    virtual AxisAlignedBoundingBox getAABB() const override;

    // Optimized implementation for spheres.
    virtual float getBoundingSphereRadius() const override;

    virtual std::string name() const override;
    virtual std::string str() const override;

    virtual std::shared_ptr<btCollisionShape> getBulletCollisionShape(float margin = 0) const override;
    virtual void addMargin(float margin) override;


    // Sphere interface

    /// Get the radius.
    float getRadius() const;
    /// Set the radius.
    void setRadius(float radius);


private:

    /// The position.
    Eigen::Vector3f position = Eigen::Vector3f::Zero();
    /// The radius.
    float radius = 0;

    // A sphere does not have a meaningful orientation.


};

}

