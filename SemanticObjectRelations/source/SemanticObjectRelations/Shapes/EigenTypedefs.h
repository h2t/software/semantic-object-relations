#pragma once

#include <Eigen/Geometry>

namespace semrel
{

    using Hyperplane3f = Eigen::Hyperplane<float, 3>;
    using ParametrizedLine3f = Eigen::ParametrizedLine<float, 3>;

}
