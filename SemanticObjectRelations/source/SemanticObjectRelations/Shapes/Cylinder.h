#pragma once

#include "Shape.h"


class BtCylinderShape;

namespace semrel
{

/**
 * @brief A cylinder in 3D space.
 *
 * A cylinder has a position (its center) and a direction of its central axis
 * (axis of rotational symmetry), along with a radius and height (distance from top
 * to bottom).
 * The standard orientation of a cylinder corresponds to a central axis aligned with the Y-axis.
 * The central axis can be represented by a parametrized line (with origin and direction).
 */
class Cylinder : public Shape
{
public:

    /// Inizialize everything with zero with optional ID.
    explicit Cylinder(ShapeID id = ShapeID(0));

    /// Construct a cylinder with axis from position and direction, radius, height, and optional ID.
    Cylinder(const Eigen::Vector3f& position, const Eigen::Vector3f& direction,
             float radius, float height = 0, ShapeID id = ShapeID {0});

    /// Construct a cylinder with axis from ParametrizedLine, radius, height, and optional ID.
    Cylinder(const ParametrizedLine3f& centralAxis,
             float radius, float height = 0, ShapeID id = ShapeID {0});


    // Shape interface

    virtual Eigen::Vector3f getPosition() const override;
    virtual void setPosition(const Eigen::Vector3f& position) override;

    /// Get the orientation (w.r.t. a central axis aligned with the Y-axis).
    virtual Eigen::Quaternionf getOrientation() const override;
    /// Set the orientation (w.r.t. a central axis aligned with the Y-axis).
    virtual void setOrientation(const Eigen::Quaternionf& orientation) override;

    virtual TriMesh getTriMeshLocal() const override;

    // Provides an optimized implementation of Shape::getAABBLocal() for cylinders.
    virtual AxisAlignedBoundingBox getAABBLocal() const override;

    // Optimized implementation for cylinders.
    virtual float getBoundingSphereRadius() const override;

    virtual std::string name() const override;
    virtual std::string str() const override;

    virtual std::shared_ptr<btCollisionShape> getBulletCollisionShape(float margin = 0) const override;
    virtual void addMargin(float margin) override;


    // Cylinder interface

    /// Return the axis direction (normalized).
    Eigen::Vector3f getAxisDirection() const;
    /// Set the axis direction. (Given direction is normalized.)
    void setAxisDirection(const Eigen::Vector3f& direction);

    /// Return the central axis as parametrized line.
    ParametrizedLine3f getAxis() const;
    /// Set the central axis as parametrized line.
    void setAxis(const ParametrizedLine3f& value);
    /// Set the central axis by position and direction.
    void setAxis(const Eigen::Vector3f& position, Eigen::Vector3f& direction);

    /// Get the radius.
    float getRadius() const;
    /// Set the radius.
    void setRadius(float value);

    /// Get the height.
    float getHeight() const;
    /// Set the height.
    void setHeight(float value);

    /// Indicates whether this cylinder's height is set (i.e. is not zero).
    bool hasHeight() const;

    /// Get the top point of the cylinder (center of top circle).
    Eigen::Vector3f getTop() const;
    /// Get the bottom point of the cylidner (center of bottom circle).
    Eigen::Vector3f getBottom() const;

    /// Get the plane of the top circle, with the normal pointing outwards.
    Hyperplane3f getTopPlane() const;
    /// Get the plane of the bottom circle, with the normal pointing outwards.
    Hyperplane3f getBottomPlane() const;


private:

    /// The central axis (origin = cylinder center = position).
    ParametrizedLine3f axis = ParametrizedLine3f(
                Eigen::Vector3f::Zero(), Eigen::Vector3f::UnitY());

    /// The radius.
    float radius = 0;
    /// The height (distance from top to bottom).
    float height = 0;
};

}
