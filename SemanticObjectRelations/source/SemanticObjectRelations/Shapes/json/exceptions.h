#pragma once

#include <SemanticObjectRelations/exceptions.h>
#include <SemanticObjectRelations/Serialization/json.h>


namespace semrel::error
{

    /// Indicates that there was no registered serializer for a type.
    class NoSerializerForType : public SemanticObjectRelationsError
    {
    public:
        NoSerializerForType(const std::string& typeName);
    };


    /**
     * @brief Indicates that there already was a serializer registered for
     * a type when trying to register a serializer.
     */
    class SerializerAlreadyRegisteredForType : public SemanticObjectRelationsError
    {
    public:
        SerializerAlreadyRegisteredForType(const std::string& typeName,
                                           const std::vector<std::string>& acceptedTypes = {});
    private:
        static std::string makeMsg(const std::string& typeName, const std::vector<std::string>& acceptedTypes);
    };

}
