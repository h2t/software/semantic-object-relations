#include "ShapeSerializers.h"

#include "basic.h"
#include "exceptions.h"


void semrel::to_json(nlohmann::json& j, const Shape& shape)
{
    json::ShapeSerializers::to_json(j, shape);
}

void semrel::from_json(const nlohmann::json& j, Shape& shape)
{
    json::ShapeSerializers::from_json(j, shape);
}

void semrel::to_json(nlohmann::json& j, const ShapePtr& shapePtr)
{
    json::ShapeSerializers::to_json(j, shapePtr.get());
}

void semrel::from_json(const nlohmann::json& j, ShapePtr& shapePtr)
{
    json::ShapeSerializers::from_json(j, shapePtr);
}

void semrel::to_json(nlohmann::json& j, const semrel::Shape* shapePtr)
{
    json::ShapeSerializers::to_json(j, shapePtr);
}

void semrel::to_json(nlohmann::json& j, const ShapeMap& shapeMap)
{
    // Make it look like a ShapeList.
    j.clear();
    for (const auto& [id, shapePtr] : shapeMap)
    {
        j.push_back(nlohmann::json(shapePtr));
    }
}


namespace semrel::json
{

    const std::string ShapeSerializers::JSON_TYPE_NAME_KEY = "__type__";

    ShapeSerializers ShapeSerializers::_instance = {};


    const int ShapeSerializers::BUILTIN_SERIALIZER_REGISTRATION = []()
    {
        registerSerializer<Box>(semrel::to_json, semrel::from_json);
        registerSerializer<Cylinder>(semrel::to_json, semrel::from_json);
        registerSerializer<Sphere>(semrel::to_json, semrel::from_json);
        // registerSerializer<MeshShape>(&semrel::to_json, &semrel::from_json);

        int i = 0;

        const bool lambda = false;
        if (lambda)
        {
            // This would also work.
            registerSerializer<Cylinder>(
                        [i](nlohmann::json& j, const Cylinder& cyl) { (void) i; to_json(j, cyl); },
                        [i](const nlohmann::json& j, Cylinder& cyl) { (void) i; from_json(j, cyl); }
            );
        }
        return 0;
    }();


    void ShapeSerializers::ShapeJsonSerializer::to_json(nlohmann::json& j, const Shape& shape)
    {
        _to_json(j, shape);
    }

    void ShapeSerializers::ShapeJsonSerializer::from_json(const nlohmann::json& j, Shape& shape)
    {
        _from_json(j, shape);
    }

    void ShapeSerializers::ShapeJsonSerializer::to_json(nlohmann::json& j, const ShapePtr& shapePtr)
    {
        _to_json(j, *shapePtr);
    }

    void ShapeSerializers::ShapeJsonSerializer::from_json(const nlohmann::json& j, ShapePtr& shapePtr)
    {
        _from_json_ptr(j, shapePtr);
    }

    ShapeSerializers::ShapeJsonSerializer& ShapeSerializers::getSerializer(const nlohmann::json& j)
    {
        return _instance._getSerializer(serial::getTypeName(j, JSON_TYPE_NAME_KEY));
    }

    ShapeSerializers::ShapeJsonSerializer& ShapeSerializers::getSerializer(const std::string& demangledTypeName)
    {
        return _instance._getSerializer(demangledTypeName);
    }

    std::vector<std::string> ShapeSerializers::getRegisteredTypes()
    {
        return _instance._getRegisteredTypes();
    }

    void ShapeSerializers::to_json(nlohmann::json& j, const Shape& shape)
    {
        return getSerializer(serial::getTypeName(shape)).to_json(j, shape);
    }

    void ShapeSerializers::from_json(const nlohmann::json& j, Shape& shape)
    {
        const std::string typeName = serial::getTypeName(j, JSON_TYPE_NAME_KEY);
        if (typeName != serial::getTypeName(shape))
        {
            throw error::TypeNameMismatch(typeName, serial::getTypeName(shape));
        }
        getSerializer(typeName).from_json(j, shape);
    }

    void ShapeSerializers::to_json(nlohmann::json& j, const Shape* shapePtr)
    {
        if (!shapePtr)
        {
            throw error::ShapePointerIsNull("In ShapeJsonSerializers::to_json().");
        }
        to_json(j, *shapePtr);
    }

    void ShapeSerializers::from_json(const nlohmann::json& j, ShapePtr& shapePtr)
    {
        getSerializer(j).from_json(j, shapePtr);
    }

    ShapeSerializers::ShapeSerializers()
    {
    }


    ShapeSerializers::ShapeJsonSerializer& ShapeSerializers::_getSerializer(
            const std::string& demangledTypeName)
    {
        if (auto find = _serializers.find(demangledTypeName); find != _serializers.end())
        {
            return find->second;
        }
        else
        {
            throw error::NoSerializerForType(demangledTypeName);
        }
    }

    std::vector<std::string> ShapeSerializers::_getRegisteredTypes() const
    {
        std::vector<std::string> types;
        for (const auto& [typeName, serializer] : _serializers)
        {
            types.push_back(typeName);
        }
        return types;
    }
}
