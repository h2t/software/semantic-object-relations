#pragma once

#include <functional>
#include <typeinfo>

#include <SemanticObjectRelations/Shapes.h>
#include <SemanticObjectRelations/Serialization/json.h>
#include <SemanticObjectRelations/Serialization/type_name.h>

#include "exceptions.h"


namespace semrel
{
    /// @see `ShapeSerializers::to_json()`
    void to_json(nlohmann::json& j, const Shape& shape);
    /// @see `ShapeSerializers::from_json()`
    void from_json(const nlohmann::json& j, Shape& shape);

    /// @see `ShapeSerializers::to_json()`
    void to_json(nlohmann::json& j, const ShapePtr& shapePtr);
    /// @see `ShapeSerializers::from_json()`
    void from_json(const nlohmann::json& j, ShapePtr& shapePtr);

    /// @see `ShapeSerializers::to_json()`
    void to_json(nlohmann::json& j, const Shape* shapePtr);

    /**
     * @brief Serializes `shapeMap` to JSON as if it were a `ShapeList`.
     * After this operation, `j` can be deserialized to a `ShapeList`.
     *
     * @see `ShapeSerializers::to_json()`
     */
    void to_json(nlohmann::json& j, const ShapeMap& shapeMap);
}


namespace semrel::json
{
    /**
     * @brief Handles serialization and deserialization of dynamic `Shape`
     * objects to and from JSON.
     *
     * This class allows serialization of newly defined types through the
     * standard `nlohmann::json` interface, which builds upon user-defined
     * `to_json()` and `from_json()` functions for a user-defined type.
     *
     * @section How to register your custom Shape type
     *
     * Suppose you have a custom class deriving from `semrel::Shape` in a
     * namespace `myshape`, along with serializing functions following the
     * standard pattern employed by `nlohmann::json`:
     * @code
     * namespace myshape
     * {
     *     class MyShape : public semrel::Shape { ... };
     *
     *     void to_json(nlohmann::json& j, const MyShape& shape);
     *     void from_json(const nlohmann::json& j, MyShape& shape);
     * }
     * @endcode
     *
     * To enable serialization through the general serialization functions
     * for `Shape`, register your functions to `ShapeSerializers`, specifying
     * your custom type as template argument:
     * @code
     * // in namespace myshape
     * json::ShapeSerializers::registerSerializer<MyShape>(to_json, from_json);
     * @endcode
     * You can also specify the namespace, like in `myshape::to_json`.
     *
     * To make sure the serializer is always registered, you can create a
     * static variable (extern free or a static class member) whose
     * initialization will register the serializers, e.g.:
     * @code
     * // In the header file:
     * namespace myshape
     * {
     *     // Extern free or static member variable.
     *     extern const int _MY_SHAPE_JSON_REGISTRATION;
     * }
     *
     * // In the sourcer file:
     * const int myshape::_MY_SHAPE_JSON_REGISTRATION = []()
     * {
     *     // Register serializer when initializing variable.
     *     json::ShapeSerializers::registerSerializer<MyShape>(to_json, from_json);
     *     return 0;
     * }();
     * @endcode
     *
     *
     * @section How it is done
     *
     * `ShapeSerializers` has a global map, which maps a (demangled) type name
     * to the respective serializer. When serializing an object of type
     * `Shape`, the map is searched for an entry for the instance's dynamic
     * type. If one is found, the registered `to_json()` is used to write the
     * JSON document. In addition, a type name entry specifying the (demangled)
     * derived type name is stored (under the key
     * `ShapeSerializers::JSON_TYPE_NAME_KEY`).
     *
     * When deserializing from a JSON document, the type name entry is used to
     * determine the correct serializer, which casts the to-be-deserialized
     * `Shape` instance to the derived type and passes it to the registered
     * `from_json` method. When deserializing into a `ShapePtr`, the pointer
     * is allocated a new instance of the deriving type first.
     */
    class ShapeSerializers
    {
    public:

        /// JSON key under which demangled type name is stored.
        static const std::string JSON_TYPE_NAME_KEY;


    public:

        // PUBLIC STATIC INTERFACE

        /**
         * @brief Register a JSON seralizer for `DerivedShape`.
         *
         * Can be called with raw function pointers with automatic type deduction e.g.:
         *
         * @code
         * namespace my_shape {
         *     // Standard serialization methods.
         *     void to_json(nlohmann::json& j, const MyShape& box);
         *     void from_json(const nlohmann::json& j, MyShape& box);
         * }
         *
         * void register() {
         *     // Register serializer:
         *     registerSerializer<my_shape::MyShape>(my_shape::to_json, my_shape::from_json);
         * }
         * @endcode
         *
         * @throw `error::SerializerAlreadyRegisteredForType`
         *      If there already is a registered serializer for that type.
         */
        template <class DerivedShape>
        static void registerSerializer(serial::RawToJsonFn<DerivedShape> to_json,
                                       serial::RawFromJsonFn<DerivedShape> from_json,
                                       bool overwrite = false)
        {
            registerSerializer<DerivedShape>(serial::ToJsonFn<DerivedShape>(to_json),
                                             serial::FromJsonFn<DerivedShape>(from_json), overwrite);
        }

        /**
         * @brief Register a JSON seralizer for `DerivedShape`.
         *
         * Can be called with `std::function` objects, e.g. lambdas:
         * @code
         * // Capture `serializer` by reference.
         * registerSerializer<Box>(
         *      [&](nlohmann::json& j, const Box& box) { myserializer.to_json(j, box); },
         *      [&](const nlohmann::json& j, Box& box) { myserializer.from_json(j, cyl); }
         * );
         * @endcode
         *
         * @throw `error::SerializerAlreadyRegisteredForType`
         *      If there already is a registered serializer for that type.
         */
        template <class DerivedShape>
        static void registerSerializer(serial::ToJsonFn<DerivedShape> to_json,
                                       serial::FromJsonFn<DerivedShape> from_json,
                                       bool overwrite = false)
        {
            _instance._registerSerializer<DerivedShape>(to_json, from_json, overwrite);
        }

        /**
         * Remove a registered serializer for `DerivedShape`.
         */
        template <class DerivedShape>
        static void removeSerializer()
        {
            _instance._removeSerializer<DerivedShape>();
        }


        /// Get the type names for which serializers are registered.
        static std::vector<std::string> getRegisteredTypes();


        /**
         * @brief Serialize `shape` to JSON according to its dynamic type.
         * @throw `error::NoSerializerForType` If there is no serializer for the given name.
         */
        static void to_json(nlohmann::json& j, const Shape& shape);
        /**
         * @brief Deserialize `shape` from JSON according to its dynamic type.
         * @throws `error::NoTypeNameEntryInJsonObject`
         *      If `j` does not contain the key `JSON_TYPE_NAME_KEY` (or is not a JSON object).
         * @throws `error::TypeNameMismatch`
         *      If the type name in `j` does not match `shape`'s dynamic type.
         * @throws `error::NoSerializerForType`
         *      If there is no serializer for the given name.
         */
        static void from_json(const nlohmann::json& j, Shape& shape);

        /**
         * @brief Serialize `shape` to JSON according to its dynamic type.
         * @throw `error::ShapePointerIsNull` If `shapePtr` is null.
         * @throw `error::NoSerializerForType`
         *      If there is no serializer for the shape type.
         * @see `to_json(nlohmann::json& j, const Shape& shape)`
         */
        static void to_json(nlohmann::json& j, const Shape* shapePtr);
        /**
         * @brief Deserialize `shapePtr` from JSON according the type name in `j`.
         * If there is a registered serializer for the type name in `j`,
         * assigns a new instance of the dynamic type to `shapePtr` and
         * deserializes the created instance from `j`.
         *        /// Get the accepted demangled type names.

         * @throws `error::NoTypeNameEntryInJsonObject`
         *      If `j` does not contain the key `JSON_TYPE_NAME_KEY` (or is not a JSON object).
         * @throw `error::NoSerializerForType`
         *      If there is no serializer for the type in `j`.
         */
        static void from_json(const nlohmann::json& j, ShapePtr& shapePtr);


    private:

        // PRIVATE STATIC INTERFACE

        /// A serializer for a specific derived `Shape` type.
        struct ShapeJsonSerializer
        {
            template <class DerivedShape>
            ShapeJsonSerializer(serial::ToJsonFn<DerivedShape> to_json,
                                serial::FromJsonFn<DerivedShape> from_json)
            {
                _to_json = [to_json](nlohmann::json& j, const Shape& shape)
                {
                    to_json(j, *dynamic_cast<const DerivedShape*>(&shape));
                    serial::setTypeName(j, JSON_TYPE_NAME_KEY, serial::getTypeName<DerivedShape>());
                };
                _from_json = [this, from_json](const nlohmann::json& j, Shape& shape)
                {
                    from_json(j, *dynamic_cast<DerivedShape*>(&shape));
                };
                _from_json_ptr = [this, from_json](const nlohmann::json& j, ShapePtr& shapePtr)
                {
                    // Referencing this->from_json() here causes a memory access violation.
                    // Therefore we use the from_json argument.
                    shapePtr.reset(new DerivedShape());
                    from_json(j, *dynamic_cast<DerivedShape*>(shapePtr.get()));
                    // this->_from_json(j, *shapePtr));  // Causes memory access violation.
                };
            }

            void to_json(nlohmann::json& j, const Shape& shape);
            void from_json(const nlohmann::json& j, Shape& shape);

            void to_json(nlohmann::json& j, const ShapePtr& shapePtr);
            void from_json(const nlohmann::json& j, ShapePtr& shapePtr);


        public:

            serial::ToJsonFn<Shape> _to_json;
            serial::FromJsonFn<Shape> _from_json;
            serial::FromJsonFn<ShapePtr> _from_json_ptr;

        };


        static ShapeJsonSerializer& getSerializer(const nlohmann::json& j);
        static ShapeJsonSerializer& getSerializer(const std::string& demangledTypeName);


        /// The instance, holding the registered serializers.
        static ShapeSerializers _instance;

        /**
         * @brief When initialized, serializers for builtin types are registered.
         *
         * Serializers for the following built-in types are registered:
         * - semrel::Box
         * - semrel::Cylinder
         * - semrel::Sphere
         *
         * Currently not supported:
         * - semrel::MeshShape
         */
        static const int BUILTIN_SERIALIZER_REGISTRATION;


    private:

        // PRIVATE NON-STATIC INTERFACE

        /// Construct the instance.
        ShapeSerializers();


        /**
         * @brief Register a serializer for `DerivedShape`.
         * @throw `error::SerializerAlreadyRegisteredForType`
         *      If there already is a registered serializer for that type.
         */
        template <class DerivedShape>
        void _registerSerializer(serial::ToJsonFn<DerivedShape> to_json,
                                 serial::FromJsonFn<DerivedShape> from_json,
                                 bool overwrite)
        {
            const std::string typeName = serial::getTypeName<DerivedShape>();
            if (!overwrite && _serializers.count(typeName))
            {
                throw error::SerializerAlreadyRegisteredForType(typeName, getRegisteredTypes());
            }
            _serializers.emplace(typeName, ShapeJsonSerializer(to_json, from_json));
        }

        /**
         * @brief Remove the serializer for `DerivedShape`.
         */
        template <class DerivedShape>
        void _removeSerializer()
        {
            _serializers.erase(serial::getTypeName<DerivedShape>());
        }


        /**
         * @brief Get the serializer for the given demangled type name.
         * @throw `error::NoSerializerForType` If there is no serializer for the given name.
         */
        ShapeJsonSerializer& _getSerializer(const std::string& demangledTypeName);

        /// Get the type names for which serializers are registered.
        std::vector<std::string> _getRegisteredTypes() const;


        /// The serializers. Map of demangled type name to serializer.
        std::map<std::string, ShapeJsonSerializer> _serializers;

    };


}


