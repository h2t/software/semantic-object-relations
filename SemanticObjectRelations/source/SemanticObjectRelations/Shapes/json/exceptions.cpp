#include "exceptions.h"

#include <sstream>

#include <boost/algorithm/string/join.hpp>


namespace semrel::error
{
    NoSerializerForType::NoSerializerForType(const std::string& typeName) :
        SemanticObjectRelationsError("No registered serializer for type '" + typeName + "'.")
    {}

    SerializerAlreadyRegisteredForType::SerializerAlreadyRegisteredForType(
            const std::string& typeName, const std::vector<std::string>& acceptedTypes) :
        SemanticObjectRelationsError(makeMsg(typeName, acceptedTypes))
    {}

    std::string SerializerAlreadyRegisteredForType::makeMsg(
            const std::string& typeName, const std::vector<std::string>& acceptedTypes)
    {
        std::stringstream ss;
        ss << "There is already a registered serializer for type '" + typeName + "'.";
        if (!acceptedTypes.empty())
        {
            ss << "\nAccepted types:\n" + boost::join(acceptedTypes, "\n- ");
        }
        return ss.str();
    }

}

