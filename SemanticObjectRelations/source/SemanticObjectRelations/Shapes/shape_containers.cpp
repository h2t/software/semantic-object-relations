#include "shape_containers.h"

namespace
{
    semrel::Shape* getRawPointer(const semrel::ShapePtr& p)
    {
        return p.get();
    }

    semrel::Shape* getRawPointer(semrel::Shape* p)
    {
        return p;
    }

    template <class ShapePointerList>
    semrel::ShapeMap toShapeMap(const ShapePointerList& shapes)
    {
        using namespace semrel;
        ShapeMap map;

        for (std::size_t i = 0; i < shapes.size(); ++i)
        {
            const auto& shape = shapes[i];
            if (!shape)
            {
                throw error::ShapePointerIsNull::InShapeList(i);
            }
            map[shape->getID()] = getRawPointer(shape);
        }
        return map;
    }
}


semrel::ShapeMap semrel::toShapeMap(const ShapeList& shapes)
{
    return ::toShapeMap(shapes);
}


semrel::ShapeMap semrel::toShapeMap(const std::vector<semrel::Shape*>& shapes)
{
    return ::toShapeMap(shapes);
}


std::ostream& semrel::operator <<(std::ostream& os, const ShapeList& shapeList)
{
    os << "ShapeList [ ";
    for (auto && shape : shapeList)
    {
        os << shape->tag() << " ";
    }
    os << "]";
    return os;
}

std::ostream& semrel::operator <<(std::ostream& os, const ShapeMap& shapeMap)
{
    os << "ShapeMap {";
    for (const auto& [id, shape] : shapeMap)
    {
        os << id << ": " << shape->tag() << ", ";
    }
    os << "}";
    return os;
}
