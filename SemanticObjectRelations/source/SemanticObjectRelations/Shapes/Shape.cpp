#include "Shape.h"

#include "Sphere.h"


using namespace semrel;


Shape::Shape(ShapeID id) : id(id)
{
}

ShapeID Shape::getID() const
{
    return id;
}

void Shape::setID(ShapeID id)
{
    this->id = id;
}


std::string Shape::name() const
{
    return "Shape";
}

std::string Shape::tagPrefix() const
{
    return name().substr(0, 3);
}

std::string Shape::tag() const
{
    return tagPrefix() + "_" + std::to_string(id);
}

std::string Shape::str() const
{
    return "ID: " + std::to_string(id);
}

std::string Shape::toString(const Eigen::Vector3f& vector)
{
    static const Eigen::IOFormat iof(5, 0, " ", " ", "", "", "(", ")");
    std::stringstream ss;
    ss << vector.format(iof);
    return ss.str();
}

std::string Shape::toString(const Eigen::Quaternionf& quat)
{
    std::stringstream ss;
    ss << "(" << quat.w() << " | " << " " << quat.x() << " " << quat.y() << " " << quat.z() << ")";
    return ss.str();
}

TriMesh Shape::getTriMesh() const
{
    TriMesh mesh = getTriMeshLocal();
    Eigen::Affine3f transform = Eigen::Translation3f(getPosition()) * getOrientation();
    Eigen::Matrix3f normalTransform = transform.linear().inverse().transpose();

    for (int i = 0; i < mesh.numVertices(); ++i)
    {
        mesh.getVertex(i) = transform * mesh.getVertex(i);
    }

    for (int i = 0; i < mesh.numNormals(); ++i)
    {
        mesh.getNormal(i) = (normalTransform * mesh.getNormal(i)).normalized();
    }

    return mesh;
}

AxisAlignedBoundingBox Shape::getAABBLocal() const
{
    return getTriMeshLocal().getAABB();
}

AxisAlignedBoundingBox Shape::getAABB() const
{
    return getTriMesh().getAABB();
}

float Shape::getBoundingSphereRadius() const
{
    const TriMesh mesh = getTriMeshLocal();

    float maxSquaredDistance = 0;
    for (const auto& v : mesh.getVertices())
    {
        const float squaredDistance = v.squaredNorm();
        if (squaredDistance > maxSquaredDistance)
        {
            maxSquaredDistance = squaredDistance;
        }
    }
    return std::sqrt(maxSquaredDistance);
}

Sphere Shape::getBoundingSphereLocal() const
{
    return Sphere({0, 0, 0}, getBoundingSphereRadius(), id);
}

Sphere Shape::getBoundingSphere() const
{
    return Sphere(getPosition(), getBoundingSphereRadius(), id);
}


std::ostream& semrel::operator <<(std::ostream& os, const ShapeID& id)
{
    os << long(id);
    return os;
}

std::ostream& semrel::operator <<(std::ostream& os, const Shape& s)
{
    return os << s.str();
}
