#include "exceptions.h"

namespace semrel::error
{

template <typename T>
std::string str(const T& t)
{
    return std::to_string(t);
}



ShapePointerIsNull::ShapePointerIsNull(const std::string& msg) :
    SemanticObjectRelationsError(makeMsg(msg))
{}

ShapePointerIsNull ShapePointerIsNull::InShapeList(std::size_t index)
{
    return ShapePointerIsNull("In ShapeList at index " + std::to_string(index) + ".");
}

ShapePointerIsNull ShapePointerIsNull::InShapeMap(ShapeID id)
{
    return ShapePointerIsNull("In ShapeMap at key " + std::to_string(id) + ".");
}

std::string ShapePointerIsNull::makeMsg(const std::string& msg)
{
    std::stringstream ss;
    ss << "Object pointer is null. \n" << msg;
    return ss.str();
}


}
