#pragma once

#include <SimoxUtility/json.h>
#include <SemanticObjectRelations/exceptions.h>


namespace semrel::error
{

    /**
     * @brief Indicates that a JSON document did not contain the type name.
     */
    class NoTypeNameEntryInJsonObject : public SemanticObjectRelationsError
    {
    public:
        NoTypeNameEntryInJsonObject(const std::string& missingKey, const nlohmann::json& j);
    private:
        static std::string makeMsg(const std::string& missingKey, const nlohmann::json& j);
    };


    /**
     * @brief The TypeNameEntryAlreadyInJsonObject class
     */
    class TypeNameEntryAlreadyInJsonObject : public SemanticObjectRelationsError
    {
    public:
        TypeNameEntryAlreadyInJsonObject(const std::string& key, const std::string& typeName,
                                         const nlohmann::json& j);
    private:
        static std::string makeMsg(const std::string& key, const std::string& typeName,
                                   const nlohmann::json& j);
    };


    /**
     * @brief Indicates that the type name in a JSON object did not match
     * the type of the passed C++ object.
     */
    class TypeNameMismatch : public SemanticObjectRelationsError
    {
    public:
        TypeNameMismatch(const std::string& typeInJson, const std::string& typeOfObject);
    private:
        static std::string makeMsg(const std::string& typeInJson, const std::string& typeOfObject);
    };

}
