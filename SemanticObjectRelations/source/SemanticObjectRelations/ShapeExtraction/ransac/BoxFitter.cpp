#include "BoxFitter.h"

#include <SemanticObjectRelations/Hooks/Log.h>
#include <SemanticObjectRelations/Visualization/BoxRansacVisualizer.h>

#include <SemanticObjectRelations/ShapeExtraction/ShapeExtraction.h>
#include <SemanticObjectRelations/ShapeExtraction/util/Histogram.h>
#include <SemanticObjectRelations/ShapeExtraction/util/SoftMinMax.h>


namespace semrel
{

/// Compute the line parameter of vector when projected onto line.
static float lineProjectionParameter(const ParametrizedLine3f& line, const Eigen::Vector3f& vector)
{
    Eigen::Vector3f pOnLine = line.projection(vector);

    // line(t) = o + t*d = p => t*d = p - o
    // => t = "(p-o)/d"  (ture in each dimension, including 0)

    return (pOnLine(0) - line.origin()(0)) / line.direction()(0);
}

static BoxRansacVisualizer lifeVisu = BoxRansacVisualizer::get();

BoxFitter::BoxFitter(float outlierRate) : outlierRate(outlierRate)
{
}

Eigen::Matrix3f BoxFitter::fitBoxAxesToMSS(const std::vector<Eigen::Vector3f>& mss)
{
    if (mss.size() < MSS_SIZE)
    {
        std::stringstream msg;
        msg << "MSS must contain at least " << MSS_SIZE << " points, but contained only"
            << mss.size() << ".";
        throw std::invalid_argument(msg.str());
    }
    
    Eigen::Vector3f p1 = mss[0], p2 = mss[1], p3 = mss[2], p4 = mss[3], p5 = mss[4];

    Hyperplane3f planeOne = Hyperplane3f::Through(p1, p2, p3);

    // bad MSS
    if (std::abs(planeOne.signedDistance(p4)) < 0.01f 
            || std::abs(planeOne.signedDistance(p5)) < 0.01f)
    {
        return Eigen::Matrix3f::Zero();
    }

    Eigen::Vector3f p4Proj = planeOne.projection(p4);
    Hyperplane3f planeTwo = Hyperplane3f::Through(p4, p4Proj, p5);

    
    Eigen::Matrix3f axes;
    axes.col(0) = planeOne.normal();
    axes.col(1) = planeTwo.normal();
    axes.col(2) = axes.col(0).cross(axes.col(1));

    return axes;
}


std::vector<Hyperplane3f> BoxFitter::computeVisiblePlanes(const Eigen::Matrix3f& axes,
                                                          const std::vector<Eigen::Vector3f>& points)
{
    // compute displacements for each normal
    std::vector<float> displacements[3];
    for (int i = 0; i < 3; ++i)
    {
        displacements[i].resize(points.size());
    }

    int pointsSize = (int)points.size();
    for (int pi = 0; pi < pointsSize; ++pi)
    {
        Eigen::Vector3f p = points[pi];
        Eigen::Vector3f displacement = - p.transpose() * axes;
        displacements[0][pi] = displacement[0];
        displacements[1][pi] = displacement[1];
        displacements[2][pi] = displacement[2];
    }

    std::vector<Hyperplane3f> result;
    result.reserve(3);
    Histogram histo;
    for (int i = 0; i < 3; ++i)
    {
        // find most frequent displacements
        histo.initialize(displacements[i]);
        // back side not visible => popPeak not necessary
        result.emplace_back(axes.col(i), histo.getMaxBinValue());
    }

    return result;
}



Box BoxFitter::computeBox(std::vector<Hyperplane3f>& visiblePlanes,
                          std::vector<Eigen::Vector3f> (& planeInliers)[3])
{
    /* Ideally, we now have the visible three planes.
     * We have to find their parallel (invisible) counterparts
     * aka, we have to find the dimensions of the box
     *
     * However, assume we only see two sides.
     * Knowing the dimension of two sides, the third can be extended
     * => take the two planes with most inliers
     * (assume the third one is garbage, besides its normal)
     */

    sortByNumberOfInliers(visiblePlanes, planeInliers);

    SR_LOG_DEBUG << "Got plane inliers of sizes: " << planeInliers[0].size()
                 << ", " << planeInliers[1].size() << ", " << planeInliers[2].size();

    Hyperplane3f planeOne = visiblePlanes[0];
    std::vector<Eigen::Vector3f> const& inliersOne = planeInliers[0];

    Hyperplane3f planeTwo = visiblePlanes[1];
    std::vector<Eigen::Vector3f> const& inliersTwo = planeInliers[1];


    SR_LOG_DEBUG << "Drawing selected planes and inliers";
    lifeVisu.clearLayer();
    lifeVisu.shortSleep();

    lifeVisu.drawFittingPlane(planeOne, inliersOne[0], 0);
    lifeVisu.drawFittingPlane(planeTwo, inliersTwo[0], 1);
    lifeVisu.drawInliers(inliersOne, 0);
    lifeVisu.drawInliers(inliersTwo, 1);
    lifeVisu.shortSleep();


    SR_LOG_DEBUG << "Constructing lines...";

    Eigen::Vector3f lineIntersection = planeOne.projection(planeTwo.projection(inliersOne[0]));

    // 1st: directed along normal of plane 1
    // 2nd: directed along normal plane 2
    // 3rd: along intersection line of planeOne and planeTwo

    ParametrizedLine3f line1(lineIntersection, planeOne.normal());
    ParametrizedLine3f line2(lineIntersection, planeTwo.normal());
    ParametrizedLine3f line12(lineIntersection, planeOne.normal().cross(planeTwo.normal()));

    lifeVisu.drawFittingLine(line1, line1.origin(), 0);
    lifeVisu.drawFittingLine(line2, line2.origin(), 1);
    lifeVisu.drawFittingLine(line12, line12.origin(), 2);

    lifeVisu.shortSleep();


    // project inliers onto lines and get min/max line parameters

    SR_LOG_DEBUG << "Projecting inliers onto lines";

    // on line12 from planeOne
    SoftMinMax minMaxT12One(outlierRate, inliersOne.size());
    // on line12 frmo planeTwo
    SoftMinMax minMaxT12Two(outlierRate, inliersTwo.size());
    // on line1 (in planeTwo, orthotogonal to planeOne)
    SoftMinMax minMaxT1(outlierRate, inliersTwo.size());
    // on line2 (in planeOne, orthotogonal to planeTwo)
    SoftMinMax minMaxT2(outlierRate, inliersOne.size());

    /* Visualization: for planeOne (P1)
     *
     *       _____________________/ line parallel to line2
     *      /             maxT2  /
     *     /        P1          /
     *    /                    /
     *___/______________minT2 /_____ line12
     *   |                   /|
     *   |        P2        / |
     *   |                    |
     *   |____________________|
     *  minT12              maxT12
     *
     */

    // planeOne => line 2
    for (Eigen::Vector3f const& inlier : inliersOne)
    {
        float t12 = lineProjectionParameter(line12, inlier);
        float t2 = lineProjectionParameter(line2, inlier);
        minMaxT12One.add(t12);
        minMaxT2.add(t2);
    }

    // planeTwo => line 1
    for (Eigen::Vector3f const& inlier : inliersTwo)
    {
        float t12 = lineProjectionParameter(line12, inlier);
        float t1 = lineProjectionParameter(line1, inlier);
        minMaxT12Two.add(t12);
        minMaxT1.add(t1);
    }

    float minT1 = minMaxT1.getSoftMin();
    float maxT1 = minMaxT1.getSoftMax();

    float minT2 = minMaxT2.getSoftMin();
    float maxT2 = minMaxT2.getSoftMax();

    // dimensions on 12 are shared between planeOne and planeTwo
    // => take min/max (same line => same parameter space)
    float minT12 = std::min(minMaxT12One.getSoftMin(), minMaxT12Two.getSoftMin());
    float maxT12 = std::max(minMaxT12One.getSoftMax(), minMaxT12Two.getSoftMax());

    SR_LOG_DEBUG << "Got min/maxTs";
    lifeVisu.drawFittingLine(line1, minT1, maxT1, 0);
    lifeVisu.drawFittingLine(line2, minT2, maxT2, 1);
    lifeVisu.drawFittingLine(line12, minT12, maxT12, 2);
    lifeVisu.shortSleep();


    // compute dimensions and directed normals
    float dim12 = maxT12 - minT12;

    float dim1;
    float dim2;
    Eigen::Vector3f normalOne;
    Eigen::Vector3f normalTwo;

    std::tie(dim1, normalOne) = computeDimAndNormal(minT1, maxT1, line1, line12, planeOne);
    std::tie(dim2, normalTwo) = computeDimAndNormal(minT2, maxT2, line2, line12, planeTwo);
    
    // normals shall point towards line12 (towards projections)

    SR_LOG_DEBUG << "Dimensions: " << dim1 << ", " << dim2 << ", " << dim12;


    // now the position remains to be computed

    // start from center point on line12, move along normals to center
    // (dim1/2 along normalOne, dim2/2 along normalTwo)

    Eigen::Vector3f line12Mid = 0.5 * (line12.pointAt(minT12) + line12.pointAt(maxT12));

    Eigen::Vector3f position = line12Mid - (dim1 / 2) * normalOne - (dim2 / 2) * normalTwo;


    /* Visualization:
     *
     *                   ^ n1
     *        M__________|___________ P1   <---> line2
     *        |                         ^      ^
     *    n2  |                         |      |
     *  <-----|          P             dim1  line1
     *        |                         |      |
     *        |                         v      v
     *       P2
     *         <------- dim2 ------->
     */


    Eigen::Vector3f dimensions; // along n1, n2, n3
    dimensions << dim1, dim2, dim12;

    Eigen::Matrix3f axes;
    axes.col(0) = normalOne;
    axes.col(1) = normalTwo;
    axes.col(2) = normalOne.cross(normalTwo);
    
    return Box(position, axes, dimensions);
    
}

int BoxFitter::getMssSize() const
{
    return MSS_SIZE;
}


std::pair<float, Eigen::Vector3f> BoxFitter::computeDimAndNormal(float minT, float maxT,
                                                                 const ParametrizedLine3f& line,
                                                                 const ParametrizedLine3f& line12,
                                                                 const Hyperplane3f& plane)
{
    // we compute the distance in steps and keep track of the results
    // to avoid unnecessary computations
    Eigen::Vector3f lineLimits[] = { line.pointAt(minT), line.pointAt(maxT) };

    Eigen::Vector3f lineProjs[] = { line12.projection(lineLimits[0]), line12.projection(lineLimits[1]) };

    float lineDists[] = { (lineLimits[0] - lineProjs[0]).squaredNorm(),
                          (lineLimits[1] - lineProjs[1]).squaredNorm()
                        };

    int max = lineDists[0] >= lineDists[1] ? 0 : 1;

    float dim = std::sqrt(lineDists[max]);

    Eigen::Vector3f normalDir = lineProjs[max] - lineLimits[max];
    // align with normalDir (assure normal * normalDir >= 0)
    Eigen::Vector3f normal = plane.normal().dot(normalDir) <= 0
            ? Eigen::Vector3f(- plane.normal())    // invert (needs cast)
            : plane.normal();               // correct

    return std::make_pair(dim, normal);
}

void BoxFitter::sortByNumberOfInliers(std::vector<Hyperplane3f>& visiblePlanes,
                                      std::vector<Eigen::Vector3f> (& planeInliers)[3])
{
    std::size_t sizes[] = {
        planeInliers[0].size(),
        planeInliers[1].size(),
        planeInliers[2].size()
    };

    std::size_t iMax = std::distance(sizes, std::max_element(sizes, sizes + 3));

    auto swap = [&](std::size_t i, std::size_t j)
    {
        std::swap(visiblePlanes[i], visiblePlanes[j]);
        std::swap(planeInliers[i], planeInliers[j]);
    };

    // swap iMax to 0
    if (iMax != 0)
    {
        swap(0, iMax);
    }

    // sort 1 and 2
    if (planeInliers[1].size() < planeInliers[2].size())
    {
        swap(1, 2);
    }

}

}

