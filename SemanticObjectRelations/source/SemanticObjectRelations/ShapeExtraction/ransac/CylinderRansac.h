#pragma once

#include <SemanticObjectRelations/Shapes/Cylinder.h>

#include "RansacPCL.h"


namespace semrel
{

/**
 * @brief RANSAC for cylinders based on PCL sample consensus.
 *
 * @param @see RansacBase
 */
class CylinderRansac : public RansacPCL
{
    
public:

    /// Constructor for default parameters.
    CylinderRansac();

    void setUseRandomSeed(bool useRandomSeed) override;
    
protected:

    /// @see RansacPCL::getSampleConsensusModel()
    virtual pcl::SampleConsensusModel<PointR>::Ptr getSampleConsensusModel() override;

    /// @see RansacPCL::makeShape()
    virtual std::unique_ptr<Shape> makeShape(const Eigen::VectorXf& coefficients,
                                             const std::vector<int>& inlierIndices) override;

    /// @see RansacBase::shapeName()
    virtual std::string shapeName() const override;
    /// Returns the cylinder's radius as size measure.
    virtual float getSizeMeasure(const Shape& shape) const override;

private:
    bool useRandomSeed = false;

};

}
