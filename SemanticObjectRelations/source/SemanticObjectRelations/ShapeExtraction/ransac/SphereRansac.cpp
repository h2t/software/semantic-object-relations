#include "SphereRansac.h"

#include <pcl/sample_consensus/sac_model_sphere.h>
#include <boost/make_shared.hpp>


namespace semrel
{

SphereRansac::SphereRansac()
{
    errorFunction = SphereErrorFunction;
}

void SphereRansac::setUseRandomSeed(bool useRandomSeed)
{
    this->useRandomSeed = useRandomSeed;
}

pcl::SampleConsensusModel<SphereRansac::PointR>::Ptr SphereRansac::getSampleConsensusModel()
{
    return
#if PCL_MINOR_VERSION < 11
    boost::make_shared<
#else
    std::make_shared<
#endif
            pcl::SampleConsensusModelSphere<PointR>>(pointCloud, useRandomSeed);
}

std::unique_ptr<Shape> SphereRansac::makeShape(const Eigen::VectorXf& coefficients,
                                 const std::vector<int>& inlierIndices)
{
    (void) inlierIndices; // unused
    /*
     * Sphere coefficients (4D):
     * [center.x center.y center.z radius]
     * Source: http://docs.pointclouds.org/trunk/group__sample__consensus.html
     */
    Eigen::Vector3f center = coefficients.head(3);
    float radius = coefficients(3);

    return std::unique_ptr<Shape>(new Sphere(center, radius));
}

std::string SphereRansac::shapeName() const
{
    return "Sphere";
}

float SphereRansac::getSizeMeasure(const Shape& shape) const
{
    return static_cast<const Sphere&>(shape).getRadius();
}

}
