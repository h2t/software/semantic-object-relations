#include "ShapeErrorFunction.h"

#include <SemanticObjectRelations/Shapes.h>


std::vector<float> semrel::BoxErrorFunction(
        const std::vector<Eigen::Vector3f>& points, const Shape& model)
{
    const Box& box = dynamic_cast<const Box&>(model);

    std::vector<float> errors;
    errors.reserve(points.size());

    std::vector<Hyperplane3f> faces = box.getFaces();

    for (const Eigen::Vector3f& point : points)
    {
        /* Cases:
         *
         * 0) behind all six faces => inside Box => error =
         * 1) outside exactly one face => distance to this face
         * 2) outside exactly two faces (orthogonal) => distance to intersection line
         * 3) outside exactly three faces => distance to corner
         *
         * In (1-3), the distance can be computed
         * by Pythagoras over the single distances as kathetes
         */

        float sumSquaredDistances = 0;
        float minDistance = std::numeric_limits<float>::max();
        for (Hyperplane3f& face : faces)
        {
            float signedDistance = face.signedDistance(point);

            // check whether at least one signedDistance is positive (=> point outside box)
            if (signedDistance > 0)
            {
                // sum up squared distances
                sumSquaredDistances += signedDistance * signedDistance;
            }

            minDistance = std::min(minDistance, std::abs(signedDistance));
        }

        float squaredDistance = 0;
        if (sumSquaredDistances > 0)
        {
            // at least one distance indicated outside
            squaredDistance = sumSquaredDistances;
        }
        else
        {
            // inside => return smallest distance
            squaredDistance = minDistance * minDistance;
        }

        errors.push_back(squaredDistance);
    }

    return errors;
}


std::vector<float> semrel::CylinderErrorFunction(
        const std::vector<Eigen::Vector3f>& points, const Shape& model)
{
    std::vector<float> errors;
    errors.reserve(points.size());

    const Cylinder& cylinder = dynamic_cast<const Cylinder&>(model);

    if (!cylinder.hasHeight())
    {
        // just compute distances to infinite mantle
        for (const Eigen::Vector3f& point : points)
        {
            float distance = cylinder.getAxis().distance(point) - cylinder.getRadius();
            errors.push_back(distance * distance);
        }
        return errors;
    }

    // cylinder has height
    for (const Eigen::Vector3f& point : points)
    {

        // source: http://liris.cnrs.fr/Documents/Liris-1297.pdf

        /*
         * Visualization:
         *
         *       Side (CASE 1) o p     Circle (CASE 3)
         *    __|______________:____|__
         *      |              :    |
         *    r |              : y  |  Disc (CASE 2)
         *      |           x  :    |
         * -----o---------o--->o----o-------- central axis
         *      a         c    h    b
         *
         * a = top, b = bottom
         * p = point, c = cylinder position
         * h = projected point
         * x = distance along axis, y = distance orthogonal to axis
         */

        // have to differentate 3 cases (shortest distance to mantle, disc, circle)

        // h in source
        Eigen::Vector3f pointOnAxis = cylinder.getAxis().projection(point);
        // x in source
        float distAlongAxis = (cylinder.getPosition() - pointOnAxis).norm();

        // y in source
        float distFromAxis = cylinder.getAxis().distance(point);

        float distToTube = distFromAxis - cylinder.getRadius();
        float distToDisc = distAlongAxis - cylinder.getHeight() / 2;

        float squaredDistance = 0;

        if (distAlongAxis <= cylinder.getHeight() / 2)
        {
            // SIDE or DISC: pointOnAxis inside cylinder
            float distance = std::abs(distToTube);
            if (distFromAxis <= cylinder.getRadius())
            {
                // inside cylinder => distance to disc could be shorter
                distance = std::min(distance, std::abs(distToDisc));
            }
            squaredDistance = distance * distance;
        }
        else if (distFromAxis <= cylinder.getRadius())
        {
            // DISC: projects onto disc
            squaredDistance = distToDisc * distToDisc;
        }
        else
        {
            // CIRCLE: projects onto circle => pythagoras
            squaredDistance = distToTube * distToTube + distToDisc * distToDisc;
        }

        errors.push_back(squaredDistance);
    }

    return errors;
}


std::vector<float> semrel::SphereErrorFunction(
        const std::vector<Eigen::Vector3f>& points, const Shape& model)
{
    const Sphere& sphere = dynamic_cast<const Sphere&>(model);

    std::vector<float> errors;
    errors.reserve(points.size());

    Eigen::Vector3f pos = sphere.getPosition();
    float radius = sphere.getRadius();

    for (const Eigen::Vector3f& point : points)
    {
        // distance center <-> point minus radius
        float distance = (point - pos).norm() - radius;
        errors.push_back(distance * distance);
    }

    return errors;
}

