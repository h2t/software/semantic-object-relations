#pragma once

#include "ShapeErrorFunction.h"

#include <SemanticObjectRelations/Shapes/Shape.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>


namespace semrel
{

/// Main point type (colored and labeled).
using PointL = pcl::PointXYZRGBL;

struct RansacResultInfo
{
    /// The error of the computed model.
    float error;
    /// The coverage of the computed model.
    float coverage;
};

/// Result of a RANSAC procedure.
struct RansacResult
{
    /// Construct with nullptr and zero error and coverage.
    RansacResult();
    /// Construct with given values.
    RansacResult(std::unique_ptr<Shape>&& shape, float error, float coverage);
    
    /// The computed model. May be nullptr if no good model was found.
    std::unique_ptr<Shape> shape;
    
    /// Computed properties of the result.
    RansacResultInfo info;
};


/**
 * @brief Base class for RANSAC shape fitting for different Shape types.
 *
 *
 * @param Maximal number of iterations: (default: 100)
 *      How many model candidates shall be fitted to the point cloud.
 * @param Distance threshold (default: 1.0)
 *      The maximal distance distance of a point to the model to be
 *      considered an inlier.
 * @param Size penalty factor and exponent
 *      Size penalty attempts to avoid very large models trivially fiting
 *      the point cloud (e.g. a huge sphere fitting a plane). Given a size
 *      measure size (e.g. the radius of a sphere), the size penalty p is
 *      penalty = factor * size ^ exponent.
 * @param Outlier rate (must be in [0, 0.5], default: 0.0)
 *      Allows a proportion of points to be considered as "outliers" and
 *      neglected for fitting and error calculation. This helps to cope
 *      with unclean segmentations. (@see SoftMinMax)
 */
class RansacBase
{
public:
    
    /// Constructor with all parameters.
    RansacBase(int maxIterations = 100, float distanceThreshold = 1.0, float outlierRate = 0,
               float sizePenaltyFactor = 0.000125, float sizePenaltyExponent = 2.0);
    
    virtual ~RansacBase() = default;
    
    
    /**
     * @brief Fit a shape to the given point cloud.
     * The result may contain a nullptr if no good model was found
     * or the point cloud was too small.
     */
    virtual RansacResult fitShape(const pcl::PointCloud<PointL>& pointCloud) = 0;

    virtual void setUseRandomSeed(bool useRandomSeed) = 0;
    
    
    // PARAMETERS
    
    /// Set max iterations.
    void setMaxIterations(int maxIterations);
    /// Set distance threshold.
    void setDistanceThreshold(float distanceThreshold);
    
    /// Set size penalty factor and exponent.
    void setSizePenalty(float factor, float exponent);
    void setSizePenaltyFactor(float factor);
    void setSizePenaltyExponent(float exponent);
    /// Set the soft error margin (must be in [0..0.5]).
    void setOutlierRate(float outlierRate);
    
    
protected:
    
    struct Evaluation
    {
        float error;
        float coverage;
    };
    
    /// Return the extracted shape's name for logging.
    virtual std::string shapeName() const = 0;
    /// Return a measure of the shape's size for size penalty.
    virtual float getSizeMeasure(const Shape& shape) const = 0;
    
    /// Fill `points` with the given point cloud.
    virtual void setPointCloud(const pcl::PointCloud<PointL>& pointCloud);
    
    /**
     * @brief Evaluates the given model.
     *
     * Computes the inliers to the given model, the error with respect to
     * the inliers and the coverage as proportion of inliers to total
     * points.
     * The inlier error threshold is defaulted to distanceThreshold^2.
     * @return error and coverage
     */
    Evaluation evaluate(const Shape& model) const;
    
    /**
     * @brief Evaluates the given model.
     *
     * Computes the inliers to the given model, the error with respect to
     * the inliers and the coverage as proportion of inliers to total
     * points.
     *
     * @param inlierErrorThreshold the error threshold for inliers
     * @return error and coverage
     */
    Evaluation evaluate(const Shape& model, float inlierErrorThreshold) const;
    
    
    // INPUT
    
    /// The point cloud (as Eigen points).
    std::vector<Eigen::Vector3f> points;
    
    
    // PARAMETERS
    
    /// Maximal number of iterations.
    int maxIterations;
    /// Maximal distance for inliers.
    float distanceThreshold;
    
    /// The outlier rate.
    float outlierRate;
    
    ShapeErrorFunction errorFunction;
    
    
private:
    
    /// Returns the size penalty of the given model.
    float sizePenalty(const Shape& model) const;
    
    
    // PARAMETERS
    
    /// Proportional factor for size penalty.
    float sizePenaltyFactor;
    /// Exponent of size penalty.
    float sizePenaltyExponent;
    
};

}
