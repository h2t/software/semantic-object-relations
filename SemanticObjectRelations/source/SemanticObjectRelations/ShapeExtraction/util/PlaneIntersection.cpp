#include "PlaneIntersection.h"


namespace semrel
{

ParametrizedLine3f intersectPlanes(
        const Hyperplane3f& plane1,
        const Hyperplane3f& plane2)
{
    Eigen::Vector3f n1 = plane1.normal(), n2 = plane2.normal();

    Eigen::Vector3f direction = n1.cross(n2).normalized();

    /*
     * Direction is easy. Now we have to find a point on the line (in both planes).
     *
     * There must be one coordinate of direction that is non-zero.
     * Without loss of generality, let's assume this is x.
     *
     * Because the x component of direction is non-zero,
     * there is a point X0=(x,y,z) on the line where x is zero: X0=(0,y0,z0)
     * This is our wanted point.
     *
     * We have two plane equations:
     * P1: a1*x0 + b1*y0 + c1*z0 + d1 = 0, with n1=(a1,b1,c1)
     * P2: a2*x0 + b2*y0 + c2*z0 + d1 = 0, with n2=(a2,b2,c2)
     *
     * Set x0 (iNonZero) to 0 => 2 equation with 2 variables
     *   b1*y0 + c1*z0 = -d1
     *   b2*y0 + c2*z0 = -d2
     * Or in Matrix notation:
     * (b1 c1) * (y0) = (-d1)
     * (c2 c2)   (z0)   (-d2)
     *    A    *  x   =   b
     *
     * => Solve this eqation and get y0,z0
     * => We have a point X0=(0, y0, z0) on the line
     * => Line = X0 + direction
     *
     * Source: https://www.gamefaqs.com/boards/576216-maths-marathon/58370519
     */

    // find non-zero component
    int iNonZero = 0; // "x"
    while (std::abs(direction(iNonZero)) < 1e-7 && iNonZero < 3)
    {
        iNonZero++;
    }

    int iOther1 = (iNonZero + 1) % 3; // "y"
    int iOther2 = (iNonZero + 2) % 3; // "z"

    // build linear equation system
    /*
     *    A    *   x  =   b
     * (b1 c1) * (y0) = (-d1)
     * (c2 c2)   (z0)   (-d2)
     */

    Eigen::Matrix2f A;
    A << n1(iOther1), n1(iOther2),
            n2(iOther1), n2(iOther2);

    Eigen::Vector2f b;
    b << -plane1.offset(), -plane2.offset();

    Eigen::Vector2f x = A.colPivHouseholderQr().solve(b);

    Eigen::Vector3f point;
    point(iNonZero) = 0;
    point(iOther1) = x(0);
    point(iOther2) = x(1);

    return ParametrizedLine3f(point, direction);
    
}

}
