#pragma once

#include <SemanticObjectRelations/Shapes/EigenTypedefs.h>


namespace semrel
{

// @Cleanup: Is this still used? Where?
/// Compute the intersection line between the two given planes.
ParametrizedLine3f intersectPlanes(const Hyperplane3f& plane1, const Hyperplane3f& plane2);

}
