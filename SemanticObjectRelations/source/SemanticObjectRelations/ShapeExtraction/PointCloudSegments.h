#pragma once

#include <map>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>


namespace semrel
{

/**
 * @brief Represents a segmented point cloud.
 *
 * Can be constructed from a labeled point cloud. The point cloud is
 * split into distinct segments, i.e. parts of the point cloud with a
 * common label.
 *
 * Labels do not need to be continuous: they may be distributed
 * arbitrarily.
 *
 * The class exposes its segments as mutable point cloud pointers. You may
 * modify a segment freely, but keep in mind that all points of a segment
 * should have a common label. This condition is not monitored by this
 * class, but it might be expected to be true at any point.
 */
class PointCloudSegments
{

public:

    /// The used point type.
    using PointT = pcl::PointXYZRGBL;
    /// Map from labels to segments (point cloud with a common label).
    using SegmentMap = std::map<uint32_t, pcl::PointCloud<PointT>::Ptr>;


    /// Construct empty point cloud segments.
    PointCloudSegments();

    /// Construct form a labeled point cloud.
    PointCloudSegments(const pcl::PointCloud<PointT>& pointCloud);


    /// Make *this empty.
    void clear();
    /// Insert the given point cloud.
    /// If *this already contains a label, the segments are merged.
    void addPointCloud(const pcl::PointCloud<PointT>& pointCloud);
    /// Add an empty segment for the given label.
    pcl::PointCloud<PointT>::Ptr addEmptySegment(uint32_t label);
    /// Removes the segment with the given label.
    void removeSegment(uint32_t label);

    /// Return the number of segments (the number of distinct labels).
    std::size_t numSegments() const;
    /// Return the total number of points in all segments.
    std::size_t numPoints() const;

    /// Return the distinct labels.
    std::vector<uint32_t> getLabels() const;

    /// Returns the segment with the given label.
    /// @throws std::out_of_range if the label is not contained
    pcl::PointCloud<PointT>::Ptr const& getSegment(uint32_t label) const;

    /// Returns all segments in a std::vector.
    std::vector<pcl::PointCloud<PointT>::Ptr> getSegments() const;


    /// Return the map from labels to point clouds (the segments) (const).
    const SegmentMap& getSegmentMap() const;

    /// Return all segments in one point cloud.
    pcl::PointCloud<PointT>::Ptr merged() const;

    /// Map-like iterators over segments (pairs of <label, point cloud>).
    /// Identical to iteration over getSegmentMap().
    SegmentMap::const_iterator begin() const;
    SegmentMap::const_iterator end() const;


private:

    /// Map from label to point cloud.
    /// All points in `segmentMap[label]` have the label `label`.
    SegmentMap segmentMap;

};

}
