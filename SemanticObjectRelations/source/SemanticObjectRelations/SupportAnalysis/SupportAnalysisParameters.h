#pragma once

#include "SupportAnalysis.h"


namespace semrel
{

    /// Paramters of `SupportAnalysis`.
    struct SupportAnalysisParameters
    {

        /// Default constructor.
        SupportAnalysisParameters();
        /// Construct with values from `supportAnalysis`.
        SupportAnalysisParameters(const SupportAnalysis& supportAnalysis);


        /// Write the parameter values to `supportAnalysis`.
        void writeTo(SupportAnalysis& supportAnalysis) const;

        /// Set the values to the values of `supportAnalysis`.
        void setFrom(const SupportAnalysis& supportAnalysis);


        float contactMargin = 10;

        float vertSepPlaneAngleMax = 10;
        bool vertSepPlaneAssumeSupport = false;

        bool uncertaintyDetectionEnabled = true;
        float supportAreaRatioMin = 0.7f;

    };

}
