#include "UncertaintyDetection.h"

#include <boost/geometry/algorithms/area.hpp>

#include <SemanticObjectRelations/Visualization/SupportAnalysisVisualizer.h>

#include "SupportAnalysis.h"
#include "SupportPolygon.h"


namespace semrel
{

namespace bg = boost::geometry;

UncertaintyDetection::UncertaintyDetection(const Eigen::Vector3f& gravity, float supportAreaRatioMin)
    : gravityDirection(gravity), supportAreaRatioMin(supportAreaRatioMin)
{
}

SupportGraph UncertaintyDetection::performUncertaintyDetection(
        const SupportGraph& supportGraph, const ShapeMap& objectMap,
        const std::set<ShapeID>& safeObjectIDs) const
{
    SupportGraph resultGraph = supportGraph;

    for (auto vertex : resultGraph.vertices())
    {
        computeSupportAreaRatio(vertex, objectMap, safeObjectIDs);
    }

    // for each unsafe object, add edges from all objects the unsafe object is supporting
    for (SupportGraph::Vertex vertex : resultGraph.vertices())
    {
        if (!vertex.attrib().ud.safe)
        {
            // add edges from all supported objects, i.e. targets of outgoing edges
            for (SupportGraph::Edge e : vertex.outEdges())
            {
                if (!resultGraph.hasEdge(e.target(), e.source()))
                {
                    SupportGraph::Vertex supported = e.target();
                    SupportGraph::Edge newEdge = resultGraph.addEdge(supported, vertex);
                    newEdge.attrib().fromUncertaintyDetection = true;
                }
            }
        }
    }
    return resultGraph;
}


void UncertaintyDetection::computeSupportAreaRatio(
        SupportGraph::Vertex vertex, const ShapeMap& objectMap,
        const std::set<ShapeID>& safeObjectIDs) const
{
    SupportPolygon supportPolygon(gravityDirection);

    supportPolygon.setObjectsFromSupportGraph(vertex, objectMap);
    supportPolygon.compute();

    Polygon2D objectProjection = supportPolygon.getSupportedObjectProjection();
    Polygon2D objectSupportPolygon = supportPolygon.getSupportPolygon();
    MultiPolygon2D supportingPolygons = supportPolygon.getSupportingPolygons();

    SupportAnalysisVisualizer::get().drawSupportPolygon(supportPolygon, *objectMap.at(vertex.objectID()));
    //SupportAnalysisVisualizer::get().drawProjection(vertex.object(), gravityDirection);

    // Compute supported area ratio
    float supportedArea = float(bg::area(objectSupportPolygon));
    float objectArea = float(bg::area(objectProjection));

    float ratio = supportedArea / objectArea;
    bool safe = (ratio >= supportAreaRatioMin);

    // Check whether object is "explicitly safe"
    ShapeID objectID = vertex.objectID();
    if (safeObjectIDs.count(objectID) > 0)
    {
        safe = true;
    }

    // write into graph
    vertex.attrib().ud.enabled = true;
    vertex.attrib().ud.supportAreaRatio = ratio;
    vertex.attrib().ud.safe = safe;
}


void UncertaintyDetection::setGravityVector(const Eigen::Vector3f& gravity)
{
    this->gravityDirection = gravity;
}

Eigen::Vector3f UncertaintyDetection::getGravityVector() const
{
    return gravityDirection;
}

void UncertaintyDetection::setSupportAreaRatioMin(float supportAreaRatioMin)
{
    this->supportAreaRatioMin = supportAreaRatioMin;
}

float UncertaintyDetection::getSupportAreaRatioMin() const
{
    return supportAreaRatioMin;
}

}

