#include "SupportGraph.h"

#include <SemanticObjectRelations/RelationGraph/graphviz/WriterImpl.h>

#include <boost/algorithm/string/join.hpp>


namespace semrel
{

template class graphviz::Writer<SupportGraph>;

std::string SupportGraph::strVertex(ConstVertex v) const
{
    const SupportVertex& sv = v.attrib();
    std::stringstream ss;
    ss << RelationGraph::strVertex(v) << " [UD: ";
    if (sv.ud.enabled)
    {
        static const std::string safe_string[2] = { "unsafe", "safe" };
        ss << "suppAreaRatio=" << sv.ud.supportAreaRatio
           << " (" << safe_string[int(sv.ud.safe)] << ")";
    }
    else
    {
        ss << "disabled";
    }
    ss << " ]";
    return ss.str();
}

std::string SupportGraph::strEdge(ConstEdge e) const
{
    const SupportEdge& se = e.attrib();
    std::stringstream ss;
    ss << RelationGraph::strEdge(e) << ": ";
    if (se.verticalSeparatingPlane)
        ss << "VSP ";
    if (se.fromUncertaintyDetection)
        ss << "UD ";

    return ss.str();
}

graphviz::Writer<SupportGraph> SupportGraph::getGraphvizWriter()
{
    return { graphvizVertexFormat, graphvizEdgeFormat, graphvizGraphFormat };
}

graphviz::Attributes SupportGraph::graphvizEdgeFormat(ConstEdge edge)
{
    const SupportEdge& se = edge.attrib();
    graphviz::Attributes attribs;

    {
        std::vector<std::string> labels;
        if (se.verticalSeparatingPlane)
        {
            labels.push_back("V");
        }
        if (se.fromUncertaintyDetection)
        {
            labels.push_back("UD");
        }
        attribs["label"] = boost::algorithm::join(labels, "\n");
    }

    if (se.verticalSeparatingPlane)
    {
        attribs["color"] = attribs["fontcolor"] = "blue";
    }
    if (se.fromUncertaintyDetection)
    {
        attribs["color"] = attribs["fontcolor"] = "red";
    }
    return attribs;
}

}
