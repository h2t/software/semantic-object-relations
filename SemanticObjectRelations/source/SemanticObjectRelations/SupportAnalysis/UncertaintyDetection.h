#pragma once

#include <SemanticObjectRelations/Shapes/Shape.h>

#include "SupportGraph.h"


namespace semrel
{

/**
 * @brief Uncertainty detection (UD) aims at finding possble top-down support.
 *
 * UD takes a base support graph as input and tries to find unsafe objects.
 * An object is said to be unsafe if it is likely to fall if an object on
 * top of it is removed (because it is poorly supported by its supporting
 * objects). An object is considered unsafe if its support area ratio is
 * below a threshold.
 *
 * For each found unsafe object A, edges are added from each object B
 * supported by A to object A (i.e. supp(B,A)).
 *
 *
 * ## Parameters
 * - Gravity vector: (default: -Z)
 *   The vector of gravity (down).
 * - Support area ratio min: (default: 0.7)
 *   Minimal support area ratio of an object to consider it safe.
 *   If an object's support area ratio is below this value, an edge
 *   will be added from all objects the unsafe object is supporting.
 */
class UncertaintyDetection
{

public:

    /// Constructor.
    UncertaintyDetection(const Eigen::Vector3f& gravity = - Eigen::Vector3f::UnitZ(),
                         float supportAreaRatioMin = 0.7f);


    /**
     * @brief Perform uncertainty detection on the given support graph.
     * @param supportGraph the input support graph
     * @param safeObjectIDs IDs of objects which are "explicitly safe", i.e.
     *      specified objects will always be labeled safe irrespectively of
     *      their support are ratio. This is helpful for floors, tables, or
     *      other forms of prior knowledge.
     * @return the updated support graph (contains UD vertex attributes and UD edges)
     */
    SupportGraph performUncertaintyDetection(
            const SupportGraph& supportGraph,
            const ShapeMap& objectMap,
            const std::set<ShapeID>& safeObjectIDs = {}) const;


    // PARAMS

    /// Set the gravity vector.
    void setGravityVector(const Eigen::Vector3f& gravity);
    Eigen::Vector3f getGravityVector() const;

    /// Set the support area ratio minimum for safe objects.
    void setSupportAreaRatioMin(float supportAreaRatioMin);
    float getSupportAreaRatioMin() const;


private:

    /// Computes the support area ratio of.
    void computeSupportAreaRatio(SupportGraph::Vertex vertex, const ShapeMap& objectMap,
                                 const std::set<ShapeID>& safeObjectIDs) const;


    /// Unit vector pointing into the gravity direction.
    Eigen::Vector3f gravityDirection;

    /// Minimal support area ratio ratio of an object to consider it safe.
    float supportAreaRatioMin;

};

}
