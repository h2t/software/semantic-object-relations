#pragma once

#include <SemanticObjectRelations/Shapes/Shape.h>
#include <SemanticObjectRelations/ContactDetection/ContactPoint.h>


namespace semrel
{

/**
 * @brief ActReasoning determines the act relation between two objects,
 * given their geometry and contact points.
 *
 * @param Gravity vector: (default: -Z)
 *      The vector of gravity (down).
 * @param Vertical separating plane - Angle max: (default: 10.0) [degree]
 *      Maximal angle between gravity and separating plane for separating
 *      plane to be considered vertical.
 */
class ActReasoning
{
public:

    /// Possible results of act reasoning:
    enum Result {
        A_ACTS, ///< A acts on B.
        B_ACTS, ///< B acts on A.
        VERT_SEP_PLANE ///< Vertical separating plane.
    };

    /// Construct the separating plane. (public for visualization or other usage).
    static Hyperplane3f constructSeparatingPlane(const ContactPointList& contactPoints);


    /// Constructor.
    ActReasoning(const Eigen::Vector3f& gravity = - Eigen::Vector3f::UnitZ(),
                 float vertSepPlaneAngleMax = 10.0,
                 bool useNormalsForVerticalPlane = true);

    /// Determine act relation between the given objects.
    Result decideAct(const Shape& objectA, const Shape& objectB,
                     const ContactPointList& contactPoints) const;


    /// Set the vector of gravity (down).
    void setGravityVector(const Eigen::Vector3f& gravity);
    Eigen::Vector3f getGravityVector() const;

    /// Set the maximum angle for vertical separating planes.
    void setVertSepPlaneAngleMax(float angleMax);
    float getVertSepPlaneAngleMax() const;


private:

    /// Computes the centroid of the contact points.
    static Eigen::Vector3f getCentroid(const ContactPointList& contactPoints);


    /// The up vector, i.e. `- gravity`.
    Eigen::Vector3f up;
    /// Angle max for vertical separating plane.
    float vertSepPlaneAngleMax;
    /// true: Use normals for vertical plane detection; false: only use signed distance
    bool useNormalsForVerticalPlane = false;


};

}
