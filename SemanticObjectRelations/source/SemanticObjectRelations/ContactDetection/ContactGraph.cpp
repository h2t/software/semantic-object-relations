#include "ContactGraph.h"

namespace semrel
{

std::string ContactGraph::strEdge(RelationGraph::ConstEdge e) const
{
    std::stringstream ss;
    ss << RelationGraph::strEdge(e) << ": " << e.attrib().contactPoints.size()
       << " contact points";
    return ss.str();
}

}
