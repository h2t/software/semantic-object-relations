#pragma once

#include "ContactGraph.h"


namespace semrel
{

/**
 * @brief Contact detection allows to compute the contact points and
 * normals between a number of Shape objects. It makes use of the Bullet
 * physics library.
 *
 * An optional contact margin can be used to increase the size of the
 * objects for contact detection. This is good to cope with pose and size
 * inaccuracies in the objects.
 *
 * @param Contact margin:
 *      The distance by which objects are enlarged for contact computation.
 */
class ContactDetection
{

public:

    /// Initialization constructor.
    ContactDetection(float margin = 0.0);


    /// Compute the contacts between the given objects.
    ContactGraph computeContacts(const ShapeMap& objects) const;
    /// Compute the contacts between the given objects.
    ContactGraph computeContacts(const ShapeList& objects) const;


    /// Set the contact margin.
    void setMargin(float value);
    float getMargin() const;


private:

    /// Distance by which objects are enlarged for contact computation.
    float margin = 0.0f;

};

ContactGraph buildContactGraph(ContactPointList& contactPoints, const ShapeList& objects);
ContactGraph buildContactGraph(ContactPointList& contactPoints, const ShapeMap& objects);

}


