#include "ContactPoint.h"

#include <SemanticObjectRelations/Shapes/Shape.h>

namespace semrel
{

ContactPoint::ContactPoint(const Shape *shapeA, const Shape* shapeB,
                           const Eigen::Vector3f& position, const Eigen::Vector3f& normal,
                           bool normalOnB)
    : shapeA(shapeA), shapeB(shapeB), position(position)
{
    normalOnB ? setNormalOnB(normal) : setNormalOnA(normal);
}

std::ostream& operator<<(std::ostream& os, const ContactPoint& cp)
{
    Eigen::IOFormat iofPos(6, 0, "", " ", "", "", "[", "]");
    Eigen::IOFormat iofNorm = iofPos;
    iofNorm.precision = 3;

    os << "ContactPoint (" << cp.getShapeA()->getID() << "--" << cp.getShapeB()->getID()
       << " at " << cp.getPosition().format(iofPos)
       << " with normal " << cp.getNormalOnA().format(iofNorm) << " on A)";
    return os;
}

}
