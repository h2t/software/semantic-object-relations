#pragma once

#include <btBulletDynamicsCommon.h>
#include <memory>

namespace semrel
{

class Shape;

/**
 * @brief Wrapper for bullet's btRigidBody. Takes care of allocating and
 * keeping the necessary arguments.
 *
 * On construction from a Shape object, the object size is increased by
 * the given margin.
 */
class BulletRigidBody
{
public:
    /// Construct from a Shape.
    BulletRigidBody(const Shape& shape, float margin = 0.0);


    const btCollisionShape& getBulletShape() const { return *bulletShape; }
    const btMotionState& getMotionState() const { return *bulletMotionState; }
    const btRigidBody& getRigidBody() const { return *bulletRigidBody; }

    const Shape* getShape() const { return shape; }

    /// Get the btRigidBody raw pointer.
    btRigidBody* get() { return bulletRigidBody.get(); }
    /// Get the btRigidBody raw pointer.
    const btRigidBody* get() const { return bulletRigidBody.get(); }

    /// Access the rigid body's members.
    btRigidBody* operator->() { return get(); }
    /// Access the rigid body's members (const).
    const btRigidBody* operator->() const { return get(); }


private:

    std::shared_ptr<btCollisionShape> bulletShape;
    std::unique_ptr<btMotionState> bulletMotionState;
    std::unique_ptr<btRigidBody> bulletRigidBody;
    const Shape* shape;
};

}

