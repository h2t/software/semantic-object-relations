#include "BulletWorld.h"

namespace semrel
{

BulletWorld::BulletWorld()
    : pairCache(new btDbvtBroadphase())
    , collisionConfiguration(new btDefaultCollisionConfiguration())
    , dispatcher(new btCollisionDispatcher(collisionConfiguration.get()))
    , solver(new btSequentialImpulseConstraintSolver())
    , world(new btDiscreteDynamicsWorld(dispatcher.get(), pairCache.get(),
                                        solver.get(), collisionConfiguration.get()))
{ }

}
