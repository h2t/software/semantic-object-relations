#pragma once

#include <SemanticObjectRelations/ContactDetection/ContactPoint.h>

#include <SemanticObjectRelations/RelationGraph/RelationGraph.h>

#include <string>

namespace semrel
{

struct ContactEdge
{
    /// The contact points between the two objects.
    ContactPointList contactPoints;
};


/**
 * @brief A graph storing the contact points between each pair of objects.
 */
class ContactGraph : public RelationGraph<ShapeVertex, ContactEdge>
{
public:

    using RelationGraph<ShapeVertex, ContactEdge>::RelationGraph;

    std::string strEdge(ConstEdge e) const override;

};

}



