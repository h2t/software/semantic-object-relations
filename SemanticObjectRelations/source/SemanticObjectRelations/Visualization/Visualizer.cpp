#include "Visualizer.h"
#include "SegmentColors.h"

using namespace semrel;

Visualizer::Visualizer(const std::string& layer) : layer(layer)
{
}

void Visualizer::clearAll()
{
    impl().clearAll();
}

void Visualizer::clearLayer()
{
    impl().clearLayer(layer);
}

void Visualizer::drawPlane(
        VisuLevel level, const std::string& name,
        const Hyperplane3f& plane, const Eigen::Vector3f& at, 
        float planeSize, float normalLength, 
        DrawColor colorNormalAndEdge, DrawColor colorFill)
{
    draw(level, [&]
    {
        float lineWidth = 1;
        float normalWidth = 2.f;
    
        Eigen::Vector3f atPlane = plane.projection(at);
        Eigen::Vector3f otherAtPlane = plane.projection(at + Eigen::Vector3f(1., 1., 1.));
    
        Eigen::Vector3f dir1 = (otherAtPlane - atPlane).normalized() * planeSize;
        Eigen::Vector3f dir2 = plane.normal().cross(dir1).normalized() * planeSize;
    
        std::vector<Eigen::Vector3f> points = 
        {
            atPlane + dir1,
            atPlane + dir2,
            atPlane - dir1,
            atPlane - dir2
        };
    
        // plane
        if (planeSize > 0)
        {
            impl().drawPolygon(visuID(name + "_polygon"), points, lineWidth, colorFill, colorNormalAndEdge);
        }
    
        // normal
        if (normalLength > 0)
        {
            impl().drawArrow(visuID(name + "_normal"), atPlane, plane.normal(), normalLength, normalWidth, colorNormalAndEdge);
        }
    });
}

void Visualizer::drawTag(VisuLevel level, const Shape& shape)
{
    draw(level, [&]
    {
        impl().drawText(visuID(shape, "", "_ID"), shape.tag(), shape.getPosition(), 10, DrawColor());
    });
}

void Visualizer::drawText(VisuLevel level, const std::string& name, const std::string& text, const Eigen::Vector3f& at, int size)
{
    draw(level, [&]
    {
        impl().drawText(visuID( name), text, at, size, DrawColor());
    });
}

void Visualizer::drawShape(VisuLevel level, const Shape& shape, DrawColor color)
{
    if (auto* box = dynamic_cast<const Box*>(&shape))
    {
        drawBox(level, *box, color);
    }
    else if (auto* cylinder = dynamic_cast<const Cylinder*>(&shape))
    {
        drawCylinder(level, *cylinder, color);
    }
    else if (auto* sphere = dynamic_cast<const Sphere*>(&shape))
    {
        drawSphere(level, *sphere, color);
    }
    else if (auto* mesh = dynamic_cast<const MeshShape*>(&shape))
    {
        drawMeshShape(level, *mesh, color);
    }
}

void Visualizer::drawBox(VisuLevel level, const Box& box, DrawColor color)
{
    draw(level, [&]
    {
        impl().drawBox(visuID(box), box, color);
    });
}

void Visualizer::drawCylinder(VisuLevel level, const Cylinder& cylinder, DrawColor color)
{
    draw(level, [&]
    {
        impl().drawCylinder(visuID(cylinder), cylinder, color);
    });
}

void Visualizer::drawSphere(VisuLevel level, const Sphere& sphere, DrawColor color)
{
    draw(level, [&]
    {
        impl().drawSphere(visuID(sphere), sphere, color);
    });
}

void Visualizer::drawMeshShape(VisuLevel level, const MeshShape& meshShape, DrawColor color)
{
    draw(level, [&]
    {
        impl().drawTriMesh(visuID(meshShape), meshShape.getTriMesh(), color);
    });
}

void Visualizer::drawShapeList(VisuLevel level, const ShapeList& shapeList, float alpha, bool tags)
{
    for (auto&& shape : shapeList)
    {
        drawShape(level, *shape, SegmentColors::getColor(shape->getID(), alpha));
        if (tags)
        {
            drawTag(level, *shape);
        }
    }
}

void Visualizer::drawPointCloud(
        VisuLevel level, const std::string& name, 
        const std::vector<Eigen::Vector3f>& cloud, float pointSize, DrawColor color)
{
    draw(level, [&]
    {
        impl().drawPointCloud(visuID(name), cloud, pointSize, color);
    });
}

const std::string&Visualizer::getLayer() const
{
    return layer;
}

void Visualizer::setLayer(const std::string& layer)
{
    this->layer = layer;
}

VisualizerInterface& Visualizer::impl()
{
    return VisualizerInterface::get();
}

void Visualizer::draw(VisuLevel level, std::function<void ()> drawCall)
{
    if (level >= VisualizerInterface::getMinimumVisuLevel())
    {
        drawCall();
    }
}

DrawColor Visualizer::redGreenBlue(int rgb, float alpha)
{
    DrawColor color(0, 0, 0, alpha);
    switch (rgb % 3)
    {
        case 0:
            color.r = 1.;
            break;
        case 1:
            color.g = 1.;
            break;
        case 2:
            color.b = 1.;
            break;
        default: // i < 0
            color.r = color.g = color.b = 0.5; // grey
            break;
    }
    return color;
}

VisuMetaInfo Visualizer::visuID(const std::string& name) const
{
    return VisuMetaInfo(layer, name);
}

VisuMetaInfo Visualizer::visuID(int id, const std::string& prefix, const std::string& suffix) const
{
    std::stringstream ss;
    ss << prefix << id << suffix;
    return visuID(ss.str());
}

VisuMetaInfo Visualizer::visuID(ShapeID id, const std::string& prefix, const std::string& suffix) const
{
    std::stringstream ss;
    ss << prefix << id << suffix;
    return visuID(ss.str());
}

VisuMetaInfo Visualizer::visuID(const Shape& shape, const std::string& prefix, const std::string& suffix) const
{
    std::stringstream id;
    id << prefix << shape.tag() << suffix;
    return visuID(id.str());
}

void Visualizer::drawImportantPoint(const std::string& name, const Eigen::Vector3f& pos, float size, DrawColor color)
{
    impl().drawSphere(visuID(name), Sphere(pos, size), color);
}
