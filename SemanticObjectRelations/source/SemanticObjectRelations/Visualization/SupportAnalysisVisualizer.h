#pragma once

#include <boost/geometry.hpp>
#include <boost/geometry/multi/geometries/multi_polygon.hpp>

#include <SemanticObjectRelations/ContactDetection/ContactPoint.h>
#include <SemanticObjectRelations/SupportAnalysis/SupportGraph.h>
#include <SemanticObjectRelations/SupportAnalysis/SupportPolygon.h>

#include "Visualizer.h"


namespace semrel
{

class SupportAnalysisVisualizer : public Visualizer
{
public:

    static SupportAnalysisVisualizer& get();


    SupportAnalysisVisualizer() : Visualizer("SupportAnalysis") {}


    // Geometric Reasoning
    void drawGravity(const Eigen::Vector3f& gravity, const Eigen::Vector3f& at);

    void drawSeparatingPlane(const Hyperplane3f& sepPlane, const Eigen::Vector3f& at,
                             ShapeID idA, ShapeID idB);

    void drawActGraph(const SupportGraph& act, const ShapeMap& objects);
    void drawSupportGraph(const SupportGraph& graph, const ShapeMap& objects);

    // Uncertainty Detection
    using Point2D = boost::geometry::model::point<float, 2, boost::geometry::cs::cartesian>;
    using Polygon2D = boost::geometry::model::polygon<Point2D>;
    using MultiPolygon2D = boost::geometry::model::multi_polygon<Polygon2D>;

    void drawSupportPolygon(const SupportPolygon& supportPolygon, const Shape& object);


    // Customizable graph drawing.
    void drawSupportGraph(VisuLevel level, const std::string& namePrefix,
                          const SupportGraph& graph, const ShapeMap& objects,
                          DrawColor edgeColorBase, DrawColor edgeColorVert, DrawColor edgeColorUD);

private:

    void drawProjectionPolygon(VisuMetaInfo id, const Polygon2D& polygon,
                               const Eigen::Vector3f& basisA, const Eigen::Vector3f& basisB,
                               const DrawColor& color, float lineWidth);

};

}
