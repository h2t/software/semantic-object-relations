#pragma once

#include <SemanticObjectRelations/Hooks/VisualizerInterface.h>


namespace semrel
{


    class SegmentColors
    {
        
    public:
        
        static DrawColor getColor(ShapeID id, float alpha = 1.0);
        static std::string getColorCode(ShapeID id);
        
        
    private:
        
        static DrawColor fromHex(const std::string& hexColor, float alpha = 1.0);
        
        SegmentColors();
        
    };

}
