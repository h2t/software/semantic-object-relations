#pragma once

#include <functional>

#include <SemanticObjectRelations/Hooks/VisualizerInterface.h>


namespace semrel 
{


class Visualizer
{

public:
    
    Visualizer(const std::string& layer);

    void clearAll();
    void clearLayer();

    
    // BASIC GEOMETRY

    void drawPlane(VisuLevel level, const std::string& name,
                   const Hyperplane3f& plane, const Eigen::Vector3f& at,
                   float planeSize = 750., float normalLength = 400.,
                   DrawColor colorNormalAndEdge = { 0., 0.4f, 0.4f, 1.f }, 
                   DrawColor colorFill = { 0., 0.8f, 0.8f, 0.3f });
    
    
    // SHAPES AND TEXT
    
    void drawTag(VisuLevel level, const Shape& shape);
    void drawText(VisuLevel level, const std::string& name, const std::string& text, const Eigen::Vector3f& at, int size = 20);

    void drawShape(VisuLevel level, const Shape& shape, DrawColor color = {0., 0., 0.8f, 0.5});
    void drawBox(VisuLevel level, const Box& box, DrawColor color = {0., 0., 0.8f, 0.5});
    void drawCylinder(VisuLevel level, const Cylinder& cylinder, DrawColor color = {0., 0.8f, 0., 0.5});
    void drawSphere(VisuLevel level, const Sphere& sphere, DrawColor color = {0.8f, 0., 0., 0.5});
    void drawMeshShape(VisuLevel level, const MeshShape& meshShape, DrawColor color = {0.75, 0.75, 0.75, 0.5});

    void drawShapeList(VisuLevel level, const ShapeList& shapeList, float alpha = 1.0, bool tags = false);
    
    
    // POINT CLOUD

    void drawPointCloud(VisuLevel level, const std::string& name, const std::vector<Eigen::Vector3f>& cloud, float pointSize = 2.0, DrawColor color = {0.5, 0.5, 0.5});

    
    const std::string& getLayer() const;
    void setLayer(const std::string& layer);
    
    
protected:

    static VisualizerInterface& impl();
    static void draw(VisuLevel level, std::function<void(void)> drawCall);
    
    static DrawColor redGreenBlue(int rgb, float alpha = 1.0);
    
    
    VisuMetaInfo visuID(const std::string& name) const;
    VisuMetaInfo visuID(int id, const std::string& prefix = "", const std::string& suffix = "") const;
    VisuMetaInfo visuID(semrel::ShapeID id, const std::string& prefix = "", const std::string& suffix = "") const;
    VisuMetaInfo visuID(const Shape& shape, const std::string& prefix = "", const std::string& suffix = "") const;
    
    void drawImportantPoint(const std::string& name, const Eigen::Vector3f& pos, float size, DrawColor color);

    
    /// The layer to draw on.
    std::string layer;
    
};

}
