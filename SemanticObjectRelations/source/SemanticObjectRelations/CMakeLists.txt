
set(TARGET_NAME ${PROJECT_NAME})

find_package(Eigen3 REQUIRED)
find_package(PCL 1.8 QUIET REQUIRED COMPONENTS common features io sample_consensus)

find_package(Bullet REQUIRED)
if(BULLET_FOUND)
    BulletCheckPrecision()
endif()

find_package(Simox REQUIRED)

find_package(Boost ${Simox_BOOST_VERSION} EXACT COMPONENTS
    filesystem
    system
    program_options
    thread
    unit_test_framework
    regex
    REQUIRED)

set(PRIVATE_LIBRARIES
    stdc++fs
    pthread

    ${BULLET_LIBRARIES}

    ${PCL_FEATURES_LIBRARIES}
    ${PCL_SAMPLE_CONSENSUS_LIBRARIES}

)

set(PUBLIC_LIBRARIES
    ${PCL_COMMON_LIBRARIES}
    ${PCL_IO_LIBRARIES}
    SimoxUtility
    )

# These headers make up the public interface of the library
# They are installed via "make install" and should not include
# private headers which are only used for implementation.
set(PUBLIC_HEADERS

    exceptions.h

    # ContactDetection
    ContactDetection.h
    ContactDetection/ContactDetection.h
    ContactDetection/ContactGraph.h
    ContactDetection/ContactPoint.h

    # Hooks
    Hooks.h
    Hooks/VisualizerInterface.h
    Hooks/Log.h

    # RelationGraph
    RelationGraph.h
    RelationGraph/exceptions.h
    RelationGraph/json.h
    RelationGraph/RelationGraph.h
    RelationGraph/RelationGraphImpl.h
    RelationGraph/VertexEdge.h
    RelationGraph/graphviz.h
    RelationGraph/graphviz/attributes.h
    RelationGraph/graphviz/formats.h
    RelationGraph/graphviz/Writer.h
    RelationGraph/graphviz/WriterImpl.h
    RelationGraph/json.h
    RelationGraph/json/AttributedGraph.h
    RelationGraph/json/attributes.h

    # Serialization
    Serialization/exceptions.h
    Serialization/json.h
    Serialization/type_name.h

    # ShapeExtraction
    ShapeExtraction.h
    ShapeExtraction/ShapeExtraction.h
    ShapeExtraction/PointCloudSegments.h

    # TODO: Can we move the RANSAC stuff into the private headers?
    ShapeExtraction.h
    ShapeExtraction/ransac/BoxFitter.h
    ShapeExtraction/ransac/BoxRansac.h
    ShapeExtraction/ransac/CylinderRansac.h
    ShapeExtraction/ransac/SphereRansac.h
    ShapeExtraction/ransac/RansacPCL.h
    ShapeExtraction/ransac/RansacBase.h
    ShapeExtraction/ransac/ShapeErrorFunction.h
    ShapeExtraction/util/PlaneIntersection.h

    # Shapes
    Shapes.h
    Shapes/AxisAlignedBoundingBox.h
    Shapes/Box.h
    Shapes/Cylinder.h
    Shapes/EigenTypedefs.h
    Shapes/exceptions.h
    Shapes/MeshShape.h
    Shapes/Shape.h
    Shapes/shape_containers.h
    Shapes/Sphere.h
    Shapes/TriMesh.h
    Shapes/json.h
    Shapes/json/basic.h
    Shapes/json/exceptions.h
    Shapes/json/ShapeSerializers.h

    # Spatial
    Spatial.h
    Spatial/SpatialGraph.h
    Spatial/SpatialRelations.h
    Spatial/evaluate_relations.h
    Spatial/json.h

    # SupportAnalysis
    Support.h
    SupportAnalysis/ActReasoning.h
    SupportAnalysis/GeometricReasoning.h
    SupportAnalysis/SupportAnalysis.h
    SupportAnalysis/SupportAnalysisParameters.h
    SupportAnalysis/SupportGraph.h
    SupportAnalysis/UncertaintyDetection.h
    SupportAnalysis/json.h

    # Visualization
    Visualization.h
    Visualization/Visualizer.h
    Visualization/ContactDetectionVisualizer.h
    Visualization/BoxRansacVisualizer.h
    Visualization/SegmentColors.h
    Visualization/SupportAnalysisVisualizer.h
)

set(HEADERS
    # ContactDetection
    ContactDetection/BulletRigidBody.h
    ContactDetection/BulletWorld.h

    # ShapeExtraction
    ShapeExtraction/util/Histogram.h
    ShapeExtraction/util/SoftMinMax.h

    # Shapes
    Shapes/utils/LineIntersection.h

    # SupportAnalysis
    SupportAnalysis/SupportPolygon.h

    ${PUBLIC_HEADERS}
)

set(SOURCES

    exceptions.cpp

    # ContactDetection
    ContactDetection/BulletRigidBody.cpp
    ContactDetection/BulletWorld.cpp
    ContactDetection/ContactDetection.cpp
    ContactDetection/ContactGraph.cpp
    ContactDetection/ContactPoint.cpp

    # Hooks
    Hooks/Log.cpp
    Hooks/VisualizerInterface.cpp

    # RelationGraph
    RelationGraph/exceptions.cpp
    RelationGraph/RelationGraph.cpp
    RelationGraph/VertexEdge.cpp
    RelationGraph/graphviz/attributes.cpp
    RelationGraph/graphviz/formats.cpp
    RelationGraph/graphviz/Writer.cpp
    RelationGraph/json/AttributedGraph.cpp
    RelationGraph/json/attributes.cpp

    # Serialization
    Serialization/exceptions.cpp
    Serialization/json.cpp
    Serialization/type_name.cpp

    # ShapeExtraction
    ShapeExtraction/ShapeExtraction.cpp
    ShapeExtraction/PointCloudSegments.cpp
    ShapeExtraction/ransac/BoxFitter.cpp
    ShapeExtraction/ransac/BoxRansac.cpp
    ShapeExtraction/ransac/CylinderRansac.cpp
    ShapeExtraction/ransac/SphereRansac.cpp
    ShapeExtraction/ransac/RansacPCL.cpp
    ShapeExtraction/ransac/RansacBase.cpp
    ShapeExtraction/ransac/ShapeErrorFunction.cpp
    ShapeExtraction/util/Histogram.cpp
    ShapeExtraction/util/PlaneIntersection.cpp

    # Shapes
    Shapes/AxisAlignedBoundingBox.cpp
    Shapes/Box.cpp
    Shapes/Cylinder.cpp
    Shapes/exceptions.cpp
    Shapes/MeshShape.cpp
    Shapes/Shape.cpp
    Shapes/shape_containers.cpp
    Shapes/Sphere.cpp
    Shapes/TriMesh.cpp
    Shapes/utils/LineIntersection.cpp
    Shapes/json/basic.cpp
    Shapes/json/exceptions.cpp
    Shapes/json/ShapeSerializers.cpp

    # Spatial
    Spatial/SpatialGraph.cpp
    Spatial/SpatialRelations.cpp
    Spatial/json.cpp
    Spatial/evaluate_relations.cpp

    # SupportAnalysis
    SupportAnalysis/ActReasoning.cpp
    SupportAnalysis/GeometricReasoning.cpp
    SupportAnalysis/SupportAnalysis.cpp
    SupportAnalysis/SupportAnalysisParameters.cpp
    SupportAnalysis/SupportGraph.cpp
    SupportAnalysis/SupportPolygon.cpp
    SupportAnalysis/UncertaintyDetection.cpp
    SupportAnalysis/json.cpp

    # Visualization
    Visualization/Visualizer.cpp
    Visualization/ContactDetectionVisualizer.cpp
    Visualization/BoxRansacVisualizer.cpp
    Visualization/SegmentColors.cpp
    Visualization/SupportAnalysisVisualizer.cpp

)

add_library(${TARGET_NAME} SHARED ${SOURCES} ${HEADERS})
target_link_libraries(${TARGET_NAME} PRIVATE ${PRIVATE_LIBRARIES})
target_link_libraries(${TARGET_NAME} PUBLIC ${PUBLIC_LIBRARIES})
set_target_properties(${TARGET_NAME} PROPERTIES PUBLIC_HEADER "${PUBLIC_HEADERS}")

target_compile_definitions(${TARGET_NAME} PUBLIC -D_REENTRANT)
target_include_directories(${TARGET_NAME} PUBLIC $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>)
target_include_directories(${TARGET_NAME} PUBLIC $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>)


target_include_directories(${TARGET_NAME} SYSTEM PUBLIC ${Eigen3_INCLUDE_DIR})
if (Boost_FOUND)
    target_include_directories(${TARGET_NAME} SYSTEM PUBLIC ${Boost_INCLUDE_DIR})
endif()
if(PCL_FOUND)
    target_include_directories(${TARGET_NAME} SYSTEM PUBLIC ${PCL_INCLUDE_DIRS})
endif()
if(BULLET_FOUND)
    target_include_directories(${TARGET_NAME} SYSTEM PUBLIC ${BULLET_INCLUDE_DIR})
endif()

install(TARGETS ${TARGET_NAME}
    EXPORT ${TARGET_NAME}Targets
    RUNTIME DESTINATION "${INSTALL_BIN_DIR}" COMPONENT bin
    LIBRARY DESTINATION "${INSTALL_LIB_DIR}" COMPONENT shlib
    PUBLIC_HEADER DESTINATION "${INSTALL_INCLUDE_DIR}/${TARGET_NAME}"
    COMPONENT dev
)

