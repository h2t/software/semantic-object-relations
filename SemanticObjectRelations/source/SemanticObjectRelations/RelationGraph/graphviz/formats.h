#pragma once

#include <functional>

#include <SemanticObjectRelations/Shapes/Shape.h>

#include "attributes.h"


namespace semrel::graphviz
{

    /// Object-agnostic vertex format. Returns vertex attributes from a vertex.
    template <class RelationGraphT>
    using VertexFormat = typename std::function<Attributes(typename RelationGraphT::ConstVertex vertex)>;

    /// Object-aware vertex format. Returns vertex attributes from a vertex and its object.
    template <class RelationGraphT>
    using VertexShapeFormat = typename std::function<
        Attributes(typename RelationGraphT::ConstVertex vertex, const Shape* object)>;

    /// Object-agnostic edge format. Returns vertex attributes from an edge.
    template <class RelationGraphT>
    using EdgeFormat = typename std::function<Attributes(typename RelationGraphT::ConstEdge)>;

    /// Object-aware edge format. Returns vertex attributes from an edge and its adjacent objects.
    template <class RelationGraphT>
    using EdgeShapeFormat = typename std::function<
        Attributes(typename RelationGraphT::ConstEdge, const Shape* sourceObject, const Shape* targetObject)>;

    /// Graph format. Returns graph and global vertex/edge attributes from a graph.
    template <class RelationGraphT>
    using GraphFormat = typename std::function<GraphAttributes(const RelationGraphT& graph)>;

}
