#include "attributes.h"

#include <ostream>


namespace semrel { namespace graphviz
{

std::ostream& operator<<(std::ostream& os, const Attribute& rhs)
{
    os << rhs.first << "=\"" << rhs.second << "\"";
    return os;
}

std::ostream& operator<<(std::ostream& os, const semrel::graphviz::Attributes& rhs)
{
    os << "[ ";
    for (const auto& attrib : rhs)
    {
        os << attrib << " ";
    }
    os << "]";
    return os;
}

std::ostream& operator<<(std::ostream& os, const GraphAttributes& rhs)
{
    os << "graph " << rhs.graph << "\n";
    os << "node " << rhs.vertex << "\n";
    os << "edge " << rhs.edge << "\n";
    return os;
}


}}
