#include "attributes.h"

#include <SemanticObjectRelations/Shapes/json.h>


namespace semrel
{

    void json::to_json_base(nlohmann::json& j, const ShapeVertex& attrib)
    {
        j["id"] = attrib.objectID;
    }

    void json::from_json_base(const nlohmann::json& j, ShapeVertex& attrib)
    {
        attrib.objectID = j.at("id").get<ShapeID>();
    }

}


void semrel::to_json(nlohmann::json& j, const ShapeVertex& attrib)
{
    json::to_json_base(j, attrib);
}

void semrel::from_json(const nlohmann::json& j, ShapeVertex& attrib)
{
    json::from_json_base(j, attrib);
}


void semrel::to_json(nlohmann::json& j, const NoAttrib&)
{
    // Just make sure j is a valid json object.
    j = nlohmann::json::object();
}

void semrel::from_json(const nlohmann::json&, NoAttrib&)
{
    // Nothing to do.
}

