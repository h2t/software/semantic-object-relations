#include "AttributedGraph.h"

#include <SemanticObjectRelations/Shapes/json.h>


namespace semrel
{
    const std::string AttributedVertex::KEY_OBJECT = "object";
    const std::string AttributedGraphAttributes::KEY_TYPE_NAME = "type";


    void AttributedVertex::setObject(const Shape& object)
    {
        json[KEY_OBJECT] = object;
    }

    ShapePtr AttributedVertex::getObject() const
    {
        return json.count(KEY_OBJECT) > 0 ? json.at(KEY_OBJECT).get<ShapePtr>() : nullptr;
    }

    void AttributedGraph::serializeObjects(const semrel::ShapeMap& objects)
    {
        for (auto v : vertices())
        {
            if (auto find = objects.find(v.objectID()); find != objects.end())
            {
                v.attrib().setObject(*find->second);
            }
        }
    }

    ShapeList AttributedGraph::deserializeObjects() const
    {
        ShapeList objects;
        for (auto v : vertices())
        {
            if (ShapePtr object = v.attrib().getObject())
            {
                objects.emplace_back(std::move(object));
            }
        }
        return objects;
    }

    std::string AttributedGraph::getTypeName() const
    {
        return this->attrib().getTypeName();
    }

    void AttributedGraph::setTypeName(const std::string& typeName)
    {
        this->attrib().setTypeName(typeName);
    }

    void AttributedGraphAttributes::setTypeName(const std::string& typeName)
    {
        serial::setTypeName(json, KEY_TYPE_NAME, typeName);
    }

    std::string AttributedGraphAttributes::getTypeName() const
    {
        return json.count(KEY_TYPE_NAME) > 0 ? serial::getTypeName(json, KEY_TYPE_NAME) : "";
    }

}

void semrel::to_json(nlohmann::json& j, const AttributedVertex& value)
{
    j = value.json;
}

void semrel::from_json(const nlohmann::json& j, AttributedVertex& value)
{
    value.json = j;
}

void semrel::to_json(nlohmann::json& j, const AttributedEdge& value)
{
    j = value.json;
}

void semrel::from_json(const nlohmann::json& j, AttributedEdge& value)
{
    value.json = j;
}

void semrel::to_json(nlohmann::json& j, const AttributedGraphAttributes& value)
{
    j = value.json;
}

void semrel::from_json(const nlohmann::json& j, AttributedGraphAttributes& value)
{
    value.json = j;
}


void semrel::to_json(nlohmann::json& j, const AttributedGraph& graph)
{
    using nlohmann::json;

    // Graph attributes.
    j["attrib"] = graph.attrib();

    // Vertices and vertex attributes.
    j["vertices"] = json::array();
    {
        json& vertices = j.at("vertices");
        for (auto v : graph.vertices())
        {
            vertices.push_back({
                                   { "objectID", v.objectID() },
                                   { "attrib", v.attrib() },
                               });
        }
    }

    // Edges and edge attributes.
    j["edges"] = json::array();
    {
        json& vertices = j.at("edges");
        for (auto e : graph.edges())
        {
            vertices.push_back({
                                   { "sourceObjectID", e.sourceObjectID() },
                                   { "targetObjectID", e.targetObjectID() },
                                   { "attrib", e.attrib() },
                               });
        }
    }
}

void semrel::from_json(const nlohmann::json& j, AttributedGraph& graph)
{
    using nlohmann::json;

    // Reset graph.
    graph = AttributedGraph();

    graph.attrib() = j.at("attrib").get<AttributedGraph::GraphAttrib>();

    const json& vertices = j.at("vertices");
    for (const auto& jv : vertices)
    {
        graph.addVertex(jv.at("objectID").get<ShapeID>(),
                        jv.at("attrib").get<AttributedGraph::VertexAttrib>());
    }

    const json& edges = j.at("edges");
    for (const auto& je : edges)
    {
        graph.addEdge(je.at("sourceObjectID").get<ShapeID>(),
                      je.at("targetObjectID").get<ShapeID>(),
                      je.at("attrib").get<AttributedGraph::EdgeAttrib>());
    }
}

